/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include <projects/synthetic_ibr/renderer/Config.hpp>
#include <projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp>

#include <core/renderer/PoissonRenderer.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/scene/ProxyMesh.hpp>
#include <core/scene/InputImages.hpp>
#include <core/graphics/MaterialMesh.hpp>

namespace sibr {


	/** \class ProbesULRScene
	 *\brief Basic scene with probes instead of 2D cameras.
	 * Designed to use the same probes as Glossy Probe Reprojection for Interactive Global illumination,
	 * including their adaptive parameterization.
	 */
	class ProbesULRScene {

		SIBR_DISALLOW_COPY(ProbesULRScene);
		SIBR_CLASS_PTR(ProbesULRScene);

	public:
		
		ProbesULRScene(SyntheticAppArgs & myArgs);

		InputImages::Ptr images() const { return _imgs; }

		ProxyMesh::Ptr proxies() const  { return _proxies; }

		ProbeLocations::Ptr probes() const { return _probes; }

		std::vector<bool> activeProbes() const { return _activeProbes; }

		inline void activeProbes(std::vector<uint> probes_update) { 
			for (int i = 0; i < _activeProbes.size(); i++)
				_activeProbes[i] = false;

			for (int i = 0; i < probes_update.size(); i++)
				_activeProbes[probes_update[i]] = true;
		}

		Texture2DRGBA::Ptr lightMap() const { return _lightMap;  }

		RGBInputTextureArray::Ptr renderTargets() const { return _renderTargets;  }

		const Raycaster::Ptr & raycaster();

		const std::string & basePath() const { return _basePath;  }

		const std::string & name() const { return _name; }

		const Texture2DArrayRGB32F::Ptr & reflectorMaps() const {
			return _reflectorMaps;
		}

		const Texture2DArrayUV32F::Ptr & warpMaps() const {
			return _warpMaps;
		}

		float scale() const {
			return _scale;
		}


	private:

		void setupFromPath(const std::string& basePath, const std::string& name);

		void loadPanoramas(const std::string& basePath, const std::string& name, std::vector<sibr::ImageRGB::Ptr>& images);
		
		InputImages::Ptr _imgs;
		ProxyMesh::Ptr _proxies;
		ProbeLocations::Ptr _probes;
		std::vector<bool> _activeProbes;

		Texture2DArrayUV::Ptr _idMaps;
		Texture2DArrayRGB32F::Ptr _reflectorMaps;
		Texture2DArrayUV32F::Ptr  _warpMaps;
		
		Texture2DRGBA::Ptr _lightMap;
		RGBInputTextureArray::Ptr _renderTargets;
		Raycaster::Ptr _raycaster;
		MaterialMesh::Ptr _matMesh;
		std::string _basePath;
		std::string _name;
		float _scale = 1.0f;
	};

}
