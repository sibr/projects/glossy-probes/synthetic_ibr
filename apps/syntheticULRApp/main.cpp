/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include "ProbesULRScene.hpp"
#include "ProbesULRView.hpp"

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>

#include <projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp>


#define PROGRAM_NAME "syntheticULRApp"

using namespace sibr;

int main( int ac, char** av )
{
	{

		// Parse Commad-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		SyntheticAppArgs myArgs;
		
		// window size
		uint win_width = myArgs.win_width;
		uint win_height = myArgs.win_height;
		myArgs.rendering_size = { 1920, 1080 };
		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);
		// Check rendering size
		const Vector2u usedResolution(myArgs.rendering_size.get()[0], myArgs.rendering_size.get()[1]);

		// Setup IBR
		ProbesULRScene::Ptr		scene(new ProbesULRScene(myArgs));

		// Create the Probes ULR view.
		ProbesULRView::Ptr	probesULRView(new ProbesULRView(scene, usedResolution[0], usedResolution[1]));


		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
		generalCamera->setup(scene->proxies()->proxyPtr(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()));
		generalCamera->setFPSCameraSpeed(3.0f);
		generalCamera->setClippingPlanes(0.1f, 5000.0f);


		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		multiViewManager.addIBRSubView("Probes ULR render view", probesULRView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Probes ULR render view", generalCamera);
		const std::string screenshotsPath = scene->basePath() + "/save/" + scene->name() + "/";
		multiViewManager.setExportPath(screenshotsPath);

		// Top view
		std::shared_ptr<sibr::MultiMeshManager>   debugView = nullptr;
		if (!myArgs.skipTopView) {
			debugView.reset(new MultiMeshManager("Debug view"));
			debugView->addMesh("Probes ULR TopView", scene->proxies()->proxyPtr());
			multiViewManager.addSubView("Top view", debugView, usedResolution / 4);
		}
		probesULRView->registerDebugObjects(debugView, generalCamera);

		CHECK_GL_ERROR;

		// Main looooooop.

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();
			if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
				window.close();
			}
			window.bind();
			window.viewport().bind();

			glClear(GL_COLOR_BUFFER_BIT);

			multiViewManager.onUpdate(sibr::Input::global());
			multiViewManager.onRender(window);
			window.swapBuffer();
			CHECK_GL_ERROR
		}

	}

	return EXIT_SUCCESS;
}
