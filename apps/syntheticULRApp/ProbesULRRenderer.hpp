/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include "ProbesULRScene.hpp"
# include <core/system/Config.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/renderer/RenderMaskHolder.hpp>


namespace sibr { 
	
	/**
	 * \class ProbesULRRenderer
	 * \brief Perform per-pixel Unstructured Lumigraph Rendering (Buehler et al., 2001) on a scene with probes.
	 * Based on ULRV3Renderer in its organization. Designed to use the same probes as Glossy Probe Reprojection for Interactive Global illumination,
	 * including their adaptive parameterization.
	 */
	class ProbesULRRenderer : public RenderMaskHolderArray
	{
		SIBR_CLASS_PTR(ProbesULRRenderer);
	
	public:

		/**
		 * Constructor.
		 * \param scene The ProbesULR scene composed of the specular infos (images), reflector positions (depth), and warp maps.
		 * \param w The width of the internal rendertargets.
		 * \param h The height of the internal rendertargets.
		 * \param fShader An optional name of the fragment shader to use (default to ulr_probes).
		 * \param vShader An optional name of the vertex shader to use (default to ulr_probes).
		 * \param facecull Should the mesh be rendered with backface culling.
		 */
		ProbesULRRenderer(const ProbesULRScene::Ptr & scene,
			const uint w, const uint h, 
			const std::string & fShader = "synthetic_ibr/ulr_probes", 
			const std::string & vShader = "synthetic_ibr/ulr_probes", 
			const bool facecull = true
		);

		/**
		 * Change the shaders used by the ULR renderer.
		 * \param fShader The name of the fragment shader to use.
		 * \param vShader The name of the vertex shader to use.
		 */
		virtual void setupShaders(
			const std::string & fShader = "synthetic_ibr/ulr_probes",
			const std::string & vShader = "synthetic_ibr/ulr_probes"
		);

		/**
		 * Performs ULR rendering to a given destination rendertarget.
		 * \param eye The novel viewpoint.
		 * \param dst The destination rendertarget.
		 * \param passthroughDepth If true, depth from the position map will be output to the depth buffer for ulterior passes.
		 */
		virtual void process(
			const sibr::Camera& eye,
			IRenderTarget& dst,
			bool passthroughDepth = false
			);


		/** 
		 *  Update which probes should be used for rendering, based on the indices passed.
		 *  \param probeIds The indices to enable.
		 **/
		void updateProbes(const std::vector<uint> & probeIds);

		/// Set the epsilon occlusion threshold.
		float & epsilonOcclusion() { return _epsilonOcclusion.get(); }
		
		/// Change discard black pixel
		bool & blackPixel() { return _discardBlackPixels.get(); }

		/// Change occlusion testing
		bool & occlusionTest() { return _occTest.get(); }

		/// Flip the RGB images before using them.
		bool & flipRGBs() { return _flipRGBs.get(); }

		/// Show debug weights.
		bool & showWeights() { return _showWeights.get(); }

		/// Set winner takes all weights strategy
		bool & winnerTakesAll() { return _winnerTakesAll.get(); }

		/// Apply gamma correction to the output.
		bool & gammaCorrection() { return _gammaCorrection.get(); }

		/// Apply backface culling to the mesh.
		bool & backfaceCull() { return _backFaceCulling; }

		/** Resize the internal rendertargets.
		 *\param w the new width
		 *\param h the new height
		 **/
		void resize(const unsigned int w, const unsigned int h);

		/// Should the final RT be cleared or not.
		bool & clearDst() { return _clearDst; }

		/// \return The ID of the first pass position map texture.
		uint depthHandle() { return _depthRT->texture(); }

		/**
		 * Render the world positions of the proxy points in an intermediate rendertarget.
		 * \param mesh the proxy mesh.
		 * \param eye The novel viewpoint.
		 */
		void renderProxyDepth(const sibr::Mesh & mesh, const sibr::Camera& eye);

		/**
		* Perform ULR blending.
		* \param eye The novel viewpoint.
		* \param dst The destination rendertarget.
		* \param inputRGBs A texture array containing the input RGB images.
		* \param passthroughDepth If true, depth from the position map will be output to the depth buffer for ulterior passes.
		*/
		void renderBlending(
			const sibr::Camera& eye,
			IRenderTarget& dst,
			const sibr::Texture2DArrayRGB::Ptr & inputRGBs,
			bool passthroughDepth
		);



	protected:

		ProbesULRScene::Ptr					_scene;
		/// Shader names.
		std::string fragString, vertexString;

		sibr::GLShader _ulrShader;
		sibr::GLShader _depthShader;

		sibr::RenderTargetRGBA32F::Ptr		_depthRT;
		GLuniform<Matrix4f>					_nCamProj;
		GLuniform<Vector3f>					_nCamPos;

		GLuniform<bool>
			_occTest = true,
			_discardBlackPixels = true,
			_flipRGBs = false,
			_showWeights = false,
			_winnerTakesAll = false,
			_gammaCorrection = false;

		size_t								_maxNumProbes = 0;
		GLuniform<int>						_probesCount = 0;

		GLuniform<float>					_epsilonOcclusion = 0.05f;
		bool								_backFaceCulling = true;
		bool								_clearDst = true;

		/// Camera infos data structure shared between the CPU and GPU.
		/// We have to be careful about alignment if we want to send those struct directly into the UBO.
		struct ProbesUBOInfos {	 
			Vector3f pos;
			int selected = 0;
		};

		std::vector<ProbesUBOInfos> _probesInfos;
		GLuint _uboIndex;
		
	};


} /*namespace sibr*/ 

