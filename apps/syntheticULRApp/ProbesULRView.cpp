/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "ProbesULRView.hpp"
#include <core/graphics/GUI.hpp>


sibr::ProbesULRView::ProbesULRView(const sibr::ProbesULRScene::Ptr& scene, uint w, uint h) :
	sibr::ViewBase(w,h)
{
	_scene = scene;

	_specRenderer = std::make_shared<ProbesULRRenderer>(_scene, w, h);

	_lightmapRenderer = std::make_shared<TexturedMeshRenderer>();
	_poissonFill = std::make_shared<PoissonRenderer>(w, h);
	_specularRT = std::make_shared<RenderTargetRGB>(w, h, SIBR_GPU_LINEAR_SAMPLING);
	_diffuseRT = std::make_shared<RenderTargetRGB>(w, h, SIBR_GPU_LINEAR_SAMPLING);
	_poissonRT = std::make_shared<RenderTargetRGBA>(w, h, SIBR_GPU_LINEAR_SAMPLING);
	
	_compositingShader.init("Compositing", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_blendcommon.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/basic_composite.frag"));
	_compositingMode.init(_compositingShader, "mode");
	
}

void sibr::ProbesULRView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
{
	if (_layerMode & SyntheticLayer::DIFFUSE) {
		// Resize if needed.
		if (dst.w() != _diffuseRT->w() || dst.h() != _diffuseRT->h()) {
			_diffuseRT.reset(new sibr::RenderTargetRGB(dst.w(), dst.h()));
		}

		glViewport(0, 0, _diffuseRT->w(), _diffuseRT->h());
		_diffuseRT->clear();
		_lightmapRenderer->process(_scene->proxies()->proxy(), eye, _scene->lightMap()->handle(), *_diffuseRT);
	}

	if ((_layerMode & SyntheticLayer::SPECULAR)) {
		
		// Resize if needed.
		if (dst.w() != _specularRT->w() || dst.h() != _specularRT->h()) {
			_specularRT.reset(new sibr::RenderTargetRGB(dst.w(), dst.h()));
			_specRenderer->resize(dst.w(), dst.h());
		}
		glViewport(0, 0, _specularRT->w(), _specularRT->h());

		_specRenderer->process(eye, *_specularRT);
	}

	// Display result:
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	glDisable(GL_DEPTH_TEST);

	// Composite optional diffuse and specular layers.
	_compositingShader.begin();
	_compositingMode.set(int(_layerMode));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _diffuseRT->handle());
	if (_specularRT) {
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _specularRT->handle());
	}
	
	RenderUtility::renderScreenQuad();
	_compositingShader.end();
	
	dst.unbind();

	// Optional poisson filling.
	if (_filling) {
		if (dst.w() != _poissonRT->w() || dst.h() != _poissonRT->h()) {
			_poissonRT.reset(new sibr::RenderTargetRGBA(dst.w(), dst.h()));
			_poissonFill.reset(new sibr::PoissonRenderer(dst.w(), dst.h()));
		}
		_poissonFill->process(dst.handle(), _poissonRT);
		sibr::blit(*_poissonRT, dst);
	}
	glDisable(GL_BLEND);


	// Visualisation.
	renderVisualisation();

	
}

void sibr::ProbesULRView::onGUI()
{
	
	if (ImGui::Begin("Synthetic ULR")) {
		
		ImGui::RadioButton("Diffuse", (int*)&_layerMode, 1); ImGui::SameLine();
		ImGui::RadioButton("Specular", (int*)&_layerMode, 2); ImGui::SameLine();
		ImGui::RadioButton("Both", (int*)&_layerMode, 3);
		
		ImGui::Separator();
		
		ImGui::Checkbox("Poisson filling", &_filling);

		if (ImGui::Button("Snap to closest")) {
			snapTo(-1);
		}
		ImGui::SameLine();
		if (ImGui::InputInt("Snap to", &_snapId, 1, 10)) {
			snapTo(_snapId);
		}

		ImGui::Separator();
		
		// Other settings.
		ImGui::Checkbox("Flip RGB ", &_specRenderer->flipRGBs());
		ImGui::PushScaledItemWidth(150);
		ImGui::InputFloat("Epsilon occlusion", &_specRenderer->epsilonOcclusion(), 0.001f, 0.01f);
		ImGui::Checkbox("Occlusion Test ", &_specRenderer->occlusionTest());
		ImGui::Checkbox("Black Pixel Discard ", &_specRenderer->blackPixel());
		ImGui::Checkbox("Winner takes all ", &_specRenderer->winnerTakesAll());

		ImGui::Separator();
		// Rendering mode selection.
		if (ImGui::Combo("Rendering mode", (int*)(&_renderMode), "Standard\0One probe\0Leave one out\0Every N\0N closest\0")) {
			updateProbes(true);
		}

		// Get the desired index, make sure it falls in the cameras range.
		if (_renderMode == ONE_PROBE || _renderMode == LEAVE_ONE_OUT) {
			const bool changedIndex = ImGui::InputInt("Selected image", &_singleProbeId, 1, 10);
			_singleProbeId = sibr::clamp(_singleProbeId, 0, (int)_scene->activeProbes().size() - 1);
			if (changedIndex) {
				// If we are in "leave one out" or "one camera only" mode, we have to update the list of enabled cameras.
				updateProbes(false);
			}
		}

		if (_renderMode == EVERY_N_CAM) {
			if (ImGui::InputInt("Selection step", &_everyNProbeStep, 1, 10)) {
				_everyNProbeStep = std::max(1, _everyNProbeStep);
				updateProbes(false);
			}
		}
		if(_renderMode == N_CLOSEST) {
			if (ImGui::InputInt("Probes count", &_numNeighs, 1, 10)) {
				_numNeighs = sibr::clamp( _numNeighs, 1, int(_scene->probes()->locations().size()));
				updateProbes(false);
			}
		}
		ImGui::Separator();
		
		ImGui::Checkbox("Debug weights", &_specRenderer->showWeights());
		ImGui::Checkbox("Gamma correction", &_specRenderer->gammaCorrection());
		ImGui::PopItemWidth();
	}
	ImGui::End();
}

void sibr::ProbesULRView::registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler)
{
	_debugView = debugView;
	_handler = handler;
	if (_debugView) {
		_debugView->addMesh("Proxy", _scene->proxies()->proxyPtr()).setColor({ 0.8f, 0.8f, 0.8f });
	}
	snapTo(-1);

	updateProbes(true);
}

void sibr::ProbesULRView::snapTo(int i) {
	const InputCamera & cam = _handler->getCamera();
	const sibr::Vector3f & camPos = cam.position();
	const auto & locs = _scene->probes()->locations();
	sibr::Vector3f dstPos = camPos;
	// Look for the closest probe position.
	if(i < 0) {
		float bestDist = std::numeric_limits<float>::max();
		for (size_t lid = 0; lid < locs.size(); ++lid) {
			const float newDist = (locs[lid] - camPos).norm();
			if(newDist < bestDist) {
				bestDist = newDist;
				dstPos = locs[lid];
				_snapId = int(lid);
			}
		}
	} else if(i < int(locs.size())) {
		dstPos = locs[i];
	}
	InputCamera newCam(cam);
	newCam.position(dstPos);
	_handler->fromCamera(newCam);
}

void sibr::ProbesULRView::onUpdate(Input& input)
{
	// Live shader reloading for debug.
	if (input.key().isReleased(Key::F8)) {

		const std::string shaderSrcPath = sibr::getBinDirectory() + "/../../src/projects/synthetic_ibr/apps/syntheticULRApp/shaders/";
		const std::string shaderDstPath = sibr::getShadersDirectory("synthetic_ibr") + "/";
		const auto files = sibr::listFiles(shaderSrcPath, false, false, { "vert", "frag", "geom" });
		for (const auto & file : files) {
			sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
		}

		_specRenderer->setupShaders();
	}
	if (input.key().isActivated(Key::LeftControl) && input.mouseScroll() != 0.0) {
		_cameraScale = std::max(0.001f, _cameraScale + (float)input.mouseScroll()*0.1f);
	}

	if(_renderMode == RenderMode::N_CLOSEST) {
		updateProbes(false);
	}
}



void sibr::ProbesULRView::renderVisualisation()
{
	const std::vector<sibr::Vector3f>& probes = _scene->probes()->locations();
	std::vector<sibr::Vector3f> probesHighlightPos;
	for (int i = 0; i < probes.size(); ++i) {

		if (!_scene->activeProbes()[i]) {
			continue;
		}
		probesHighlightPos.emplace_back(probes[i]);
	}
	if (_debugView) {
		_debugView->addMeshAsLines("Scene cam", generateCamFrustum(_handler->getCamera(), 0.0f, _cameraScale)).setColor({ 1,0,0 });
		_debugView->addPoints("Probes (all)", probes, { 0.0f, 0.5f, 1.0f });
		_debugView->addPoints("Probes (selected)", probesHighlightPos, { 1.0f, 0.5f, 0.0f });
	}
}

void sibr::ProbesULRView::updateProbes(bool allowResetToDefault)
{
	// If we are here, the rendering mode or the selected index have changed, we need to update the enabled probes.
	std::vector<uint> probes_ulr;
	const std::vector<bool> activeProbes = _scene->activeProbes();
	// Compute the probes indices based on the new mode.
	if (_renderMode == RenderMode::ONE_PROBE) {
		// We only use the given probes (if it is active).
		probes_ulr = { (uint)_singleProbeId };
	}
	else if (_renderMode == RenderMode::LEAVE_ONE_OUT) {
		// We use all active probes apart from the one given.
		for (size_t cid = 0; cid < activeProbes.size(); ++cid) {
			if (cid != (size_t)_singleProbeId) {
				probes_ulr.push_back(uint(cid));
			}
		}
	}
	else if (_renderMode == RenderMode::EVERY_N_CAM) {
		// We pick one probe every N
		for (size_t cid = 0; cid < activeProbes.size(); ++cid) {
			if ((cid % _everyNProbeStep == 0)) {
				probes_ulr.push_back(uint(cid));
			}
		}
	}
	else if (_renderMode == RenderMode::N_CLOSEST) {
		struct ProbeScore {
			float dist = 0.0f;
			int id = -1;

			ProbeScore(float distA, int idA) : dist((distA)), id(int(idA)) {}
		};
		std::vector<ProbeScore> scores;
		const sibr::Vector3f & camPos = _handler->getCamera().position();
		for (size_t cid = 0; cid < activeProbes.size(); ++cid) {
			const float dist = (camPos - _scene->probes()->locations()[cid]).norm();
			scores.emplace_back(dist, int(cid));
		}

		std::sort(scores.begin(), scores.end(), [](const ProbeScore & a, const ProbeScore & b) {
			return a.dist < b.dist;
		});

		for(int sid = 0; sid < std::min(_numNeighs, int(scores.size())); ++sid) {
			probes_ulr.push_back(uint(scores[sid].id));
		}
		
	}
	else if (allowResetToDefault) {
		// We use all active probes.
		for (size_t cid = 0; cid < activeProbes.size(); ++cid) {
			//if (activeProbes[cid]) {
			probes_ulr.push_back(uint(cid));
			//}
		}
	}

	// Only update if there is at least one probe enabled.
	if (!probes_ulr.empty()) {
		// Update the shader informations in the renderer.
		_specRenderer->updateProbes(probes_ulr);
		// Tell the scene which probes we are using for debug visualization.
		_scene->activeProbes(probes_ulr);
	}
}
