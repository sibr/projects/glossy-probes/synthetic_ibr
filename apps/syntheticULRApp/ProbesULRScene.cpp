/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ProbesULRScene.hpp"
#include <projects/synthetic_ibr/renderer/MeshOBJExporter.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/graphics/MaterialMesh.hpp>

#include <projects/synthetic_ibr/renderer/ProbesGatherScene.hpp>

namespace sibr {


	ProbesULRScene::ProbesULRScene(SyntheticAppArgs & myArgs)
	{
		const std::string basePath = myArgs.dataset_path.get();
		_name = myArgs.sceneSuffix.get();
		_basePath = basePath;
		setupFromPath(basePath, _name);
	}

	void ProbesULRScene::setupFromPath(const std::string & basePath, const std::string & name)  {
		// For the flow and camera regions, we need the output_suffix directory to exist.
		const std::string flowPath = basePath + "/flows/" + name + "/";
		_imgs.reset(new InputImages());
		_proxies.reset(new ProxyMesh());
		_probes.reset(new ProbeLocations());
		_renderTargets.reset(new RGBInputTextureArray());

		// Load scene scale.
		_scale = syntheticIBR::loadScale(basePath);
		// Load geometry.
		SIBR_LOG << "[SyntheticScene] Loading geometry..." << std::flush;
		Mesh::Ptr mesh(new Mesh(true));

		MeshOBJHandler::loadOBJ(basePath + "/lightmap/scene_light_map.obj", *mesh);
		_proxies->replaceProxyPtr(mesh);
		std::cout << "Done." << std::endl;

		// Load lightmap.
		ImageRGBA lightImg;
		_lightMap = ProbesGatherScene::loadLightmap(basePath, lightImg);

		// Load probes.
		_probes->setupFromPath(basePath + "/probes/" + name + ".positions");
		const size_t pCount = _probes->locations().size();
		// Set all probes to active
		_activeProbes.resize(pCount);
		for (int i = 0; i < pCount; i++) {
			_activeProbes[i] = true;
		}
		
		// Load panoramas.
		SIBR_LOG << "[SyntheticScene] Loading specular probes..." << std::endl;
		std::vector<sibr::ImageRGB::Ptr> images;
		loadPanoramas(basePath, name, images);
		_imgs->loadFromExisting(images);
		_renderTargets->initRGBTextureArrays(_imgs, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING);
		std::cout << " Done." << std::endl;

		
		// Load reflector images.
		std::vector<ImageRGB32F::Ptr> reflectorImgs(pCount);
		const std::vector<std::string> mapFiles = sibr::listFiles(flowPath + "/reflector/", false, false, { "bin"  });
		SIBR_LOG << "[SyntheticScene] Loading position maps..." << std::endl;
#pragma omp parallel for
		for (int i = 0; i < mapFiles.size(); ++i) {
			const auto & file = mapFiles[i];
			// Get the camera name.
			std::string srcName = file.substr(3);
			srcName = srcName.substr(0, srcName.find_last_of('.'));
			const size_t srcId = std::stol(srcName);
			reflectorImgs[srcId].reset(new ImageRGB32F());
			reflectorImgs[srcId]->loadByteStream(flowPath + "/reflector/" + file, false);
		}
		std::cout << " Done." << std::endl;
		{
			SIBR_LOG << "[SyntheticScene] Uploading textures..." << std::flush;
			// Mark samples to skip in reflector map.
#pragma omp parallel for
			for (int fid = 0; fid < reflectorImgs.size(); ++fid) {
				// Mark all black samples as not to be processed.
				const auto & img = *images[fid];
#pragma omp parallel for
				for (int y = 0; y < int(img.h()); ++y) {
					for (int x = 0; x < int(img.w()); ++x) {
						const sibr::Vector3ub col = img(x, y);
						if (col[0] == 0 && col[1] == 0 && col[2] == 0) {
							reflectorImgs[fid](x, y)[0] = 100000.0f;
						}
						/*const sibr::Vector2ub id = idImgs[fid](x, y);
						if (id[0] == 0 && id[1] == 0) {
							reflectorImgs[fid](x, y)[0] = 100000.0f;
						}*/
					}
				}
			}

			// We have to upload the textures from the main thread.
			_reflectorMaps = std::make_shared<Texture2DArrayRGB32F>(reflectorImgs, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING);
			std::cout << "Done." << std::endl;
		}
	
		SIBR_LOG << "[SyntheticScene] Loading forward maps..." << std::endl;
		const std::string paramPath = basePath + "/warps/" + name + "/forward_flow/";
		std::vector<sibr::ImageFloat2> warpMaps;
		warpMaps.resize(pCount);
		const auto warpFiles = sibr::listFiles(paramPath, false, false, { "exr" });
#pragma omp parallel for
		for (int i = 0; i < warpFiles.size(); ++i) {
			const auto & warpFile = warpFiles[i];
			// Extract the cam number.
			const std::string::size_type beg = warpFile.find_first_of("0123456789");
			const std::string::size_type end = warpFile.find_last_of("0123456789");
			const std::string idStr = warpFile.substr(beg, end - beg + 1);
			const int id = std::stoi(idStr);
			assert(id < warpMaps.size());
			warpMaps[id].load(paramPath + "/" + warpFile, false);
		}
		_warpMaps = std::make_shared<Texture2DArrayUV32F>(warpMaps, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING);
		std::cout << " Done." << std::endl;
		
	}

	void ProbesULRScene::loadPanoramas(const std::string& basePath, const std::string& name, std::vector<sibr::ImageRGB::Ptr>& images) {
		const std::string imagesDir = basePath + "/images/" + name + "/";
		auto imgFiles = sibr::listFiles(imagesDir, false, false, { "bin" });
		images.resize(imgFiles.size());
#pragma omp parallel for
		for (int fid = 0; fid < int(imgFiles.size()); ++fid) {
			const auto& imgFile = imgFiles[fid];
			sibr::ImageRGB32F exrImage;
			exrImage.loadByteStream(imagesDir + "/" + imgFile, false);
			images[fid] = sibr::ImageRGB::Ptr(new sibr::ImageRGB());
			syntheticIBR::convert(exrImage, *images[fid]);
		}
	}

	const Raycaster::Ptr & ProbesULRScene::raycaster() {
		if(!_raycaster) {
			_raycaster.reset(new Raycaster());
			_raycaster->addMesh(_proxies->proxy());
		}
		return _raycaster;
	}


}
