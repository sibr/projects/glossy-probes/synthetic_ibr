/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "ProbesULRRenderer.hpp"
#include <core/system/Config.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
#include <core/view/SceneDebugView.hpp>


namespace sibr { 

	/**
	 * \class ProbesULRView
	 * \brief ULR view for a scene containing panoramic probes.
	 * Derived from ULRV3View in its organization.
	 */
	class  ProbesULRView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(ProbesULRView);


	public:

		/**
		 * Constructor
		 */
		ProbesULRView(const ProbesULRScene::Ptr& scene, uint w, uint h);

		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		void onUpdate(Input& input) override;
		
		void onGUI() override;

		void registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler);

		void snapTo(int i);


	protected:

		void renderVisualisation();

		enum SyntheticLayer : int {
			DIFFUSE = 1, 
			SPECULAR = 2,
			BOTH = DIFFUSE | SPECULAR
		};
		/// Rendering mode: default, use only one camera, use all cameras but one.
		enum RenderMode { ALL_PROBES, ONE_PROBE, LEAVE_ONE_OUT, EVERY_N_CAM, N_CLOSEST };

		/**
		 * Update the probes informations in the ProbesULR renderer based on the current rendering mode and selected index.
		 * \param allowResetToDefault If true, when the rendering mode is ALL_PROBES, the probes information will be updated.
		 */
		void updateProbes(bool allowResetToDefault);

		int						_singleProbeId = 0;
		int						_everyNProbeStep = 1;

		RenderTargetRGB::Ptr _specularRT, _diffuseRT;
		RenderTargetRGBA::Ptr _poissonRT;
		ProbesULRRenderer::Ptr _specRenderer;
		PoissonRenderer::Ptr _poissonFill;
		TexturedMeshRenderer::Ptr _lightmapRenderer;
		
		ProbesULRScene::Ptr _scene;
		
		MultiMeshManager::Ptr _debugView;
		InteractiveCameraHandler::Ptr _handler;
		// Diffuse rendering.
		
		GLShader _compositingShader;
		GLuniform<int> _compositingMode;

		SyntheticLayer _layerMode  = SyntheticLayer::BOTH;
		RenderMode	   _renderMode = RenderMode::N_CLOSEST;

		int _snapId = 0;
		float _cameraScale = 10.0f;
		bool _inRegion = true;
		int _forcedId = 0;
		bool _filling = false;
		int _numNeighs = 128;
	};

} /*namespace sibr*/ 
