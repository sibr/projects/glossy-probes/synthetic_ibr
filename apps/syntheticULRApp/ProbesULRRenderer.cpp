/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ProbesULRRenderer.hpp"



sibr::ProbesULRRenderer::ProbesULRRenderer(const ProbesULRScene::Ptr & scene, const uint w, const uint h, const std::string & fShader, const std::string & vShader, const bool facecull)
{
	_scene = scene;
	_backFaceCulling = facecull;
	fragString = fShader;
	vertexString = vShader;
	_maxNumProbes = _scene->probes()->locations().size();
	_probesCount = int(_maxNumProbes);

	// Populate the cameraInfos array (will be uploaded to the GPU).
	_probesInfos.clear();
	_probesInfos.resize(_maxNumProbes);
	for (size_t i = 0; i < _maxNumProbes; ++i) {
		_probesInfos[i].pos = _scene->probes()->locations()[i];
		_probesInfos[i].selected = _scene->activeProbes()[i];
	}

	// Compute the max number of probes allowed.
	//GLint maxBlockSize = 0, maxSlicesSize = 0;
	//glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &maxBlockSize);
	//glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxSlicesSize);
	// For each probe we store 1 vecs3, 2 floats (including padding).
	//const unsigned int bytesPerCamera = 4 * (1 * 3 + 2);
	//const unsigned int maxProbesAllowed = std::min((unsigned int)maxSlicesSize, (unsigned int)(maxBlockSize / bytesPerCamera));
	//std::cout << "[ProbesULRRenderer] " << "MAX_UNIFORM_BLOCK_SIZE: " << maxBlockSize << ", MAX_ARRAY_TEXTURE_LAYERS: " << maxSlicesSize << ", meaning at most " << maxProbesAllowed << " probes." << std::endl;

	// Create UBO.
	_uboIndex = 0;
	glGenBuffers(1, &_uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(ProbesUBOInfos)*_maxNumProbes, &_probesInfos[0], GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// Setup shaders and uniforms.
	setupShaders(fragString, vertexString);

	// Create the intermediate rendertarget.
	_depthRT.reset(new sibr::RenderTargetRGBA32F(w, h));

	CHECK_GL_ERROR;
}


void sibr::ProbesULRRenderer::setupShaders(const std::string & fShader, const std::string & vShader)
{


	// Create shaders.
	std::cout << "[ProbesULRRenderer] Setting up shaders for " << _maxNumProbes << " probes." << std::endl;
	GLShader::Define::List defines;
	defines.emplace_back("NUM_PROBES", _maxNumProbes);
	defines.emplace_back("ULR_STREAMING", 0);

	_ulrShader.init("ProbesULR",
		sibr::loadFile(sibr::getShadersDirectory("") +vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("") +fShader + ".frag", defines));
	_depthShader.init("ProbesULRDepth",
		sibr::loadFile(sibr::getShadersDirectory("core") + "/ulr_intersect.vert"),
		sibr::loadFile(sibr::getShadersDirectory("core") + "/ulr_intersect.frag", defines));

	// Setup uniforms.
	_nCamProj.init(_depthShader, "proj");
	_nCamPos.init(_ulrShader, "ncam_pos");
	_occTest.init(_ulrShader, "occ_test");
	_discardBlackPixels.init(_ulrShader, "discard_black_pixels");
	_epsilonOcclusion.init(_ulrShader, "epsilonOcclusion");
	_flipRGBs.init(_ulrShader, "flipRGBs");
	_showWeights.init(_ulrShader, "showWeights");
	_winnerTakesAll.init(_ulrShader, "winner_takes_all");
	_probesCount.init(_ulrShader, "probesCount");
	_gammaCorrection.init(_ulrShader, "gammaCorrection");

	CHECK_GL_ERROR;
}

void sibr::ProbesULRRenderer::process(
	const sibr::Camera & eye,
	IRenderTarget & dst,
	bool passthroughDepth
) {
	// Render the proxy positions in world space.
	renderProxyDepth(_scene->proxies()->proxy(), eye);
	// Perform ULR blending.
	renderBlending(eye, dst, _scene->renderTargets()->getInputRGBTextureArrayPtr(), passthroughDepth);
}

void sibr::ProbesULRRenderer::updateProbes(const std::vector<uint> & probeIds) {
	// Reset all cameras.
	for(auto & probesInfos : _probesInfos) {
		probesInfos.selected = 0;
	}
	// Enabled the ones passed as indices.
	for (const auto & probeId : probeIds) {
		_probesInfos[probeId].selected = 1;
	}

	// Update the content of the UBO.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ProbesUBOInfos)*_maxNumProbes, &_probesInfos[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void sibr::ProbesULRRenderer::renderProxyDepth(const sibr::Mesh & mesh, const sibr::Camera & eye)
{
	// Bind and clear RT.
	_depthRT->bind();
	glViewport(0, 0, _depthRT->w(), _depthRT->h());
	glClearColor(0, 0, 0, 1);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render the mesh from the current viewpoint, output positions.
	_depthShader.begin();
	_nCamProj.set(eye.viewproj());

	mesh.render(true, _backFaceCulling);
	
	_depthShader.end();
	_depthRT->unbind();
}

void sibr::ProbesULRRenderer::renderBlending(
	const sibr::Camera & eye,
	IRenderTarget & dst,
	const sibr::Texture2DArrayRGB::Ptr & inputRGBs,
	bool passthroughDepth
){
	// Bind and clear destination rendertarget.
	glViewport(0, 0, dst.w(), dst.h());
	if (_clearDst) {
		dst.clear();
	}
	dst.bind();

	_ulrShader.begin();

	// Uniform values.
	_nCamPos.set(eye.position());
	_occTest.send();
	_discardBlackPixels.send();
	_epsilonOcclusion.send();
	_flipRGBs.send();
	_showWeights.send();
	_probesCount.send();
	_winnerTakesAll.send();
	_gammaCorrection.send();

	// Textures.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _depthRT->handle());

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputRGBs->handle());

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->reflectorMaps()->handle());

	// Pass the warp maps 
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->warpMaps()->handle());
	
	// Bind UBO to shader, after all possible textures.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBindBufferBase(GL_UNIFORM_BUFFER, 4, _uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	if (passthroughDepth) {
		glEnable(GL_DEPTH_TEST);
	}
	else {
		glDisable(GL_DEPTH_TEST);
	}

	// Perform ULR rendering.
	RenderUtility::renderScreenQuad();
	glDisable(GL_DEPTH_TEST);

	_ulrShader.end();
	dst.unbind();
}

void sibr::ProbesULRRenderer::resize(const unsigned w, const unsigned h) {
	_depthRT.reset(new sibr::RenderTargetRGBA32F(w, h));
}
