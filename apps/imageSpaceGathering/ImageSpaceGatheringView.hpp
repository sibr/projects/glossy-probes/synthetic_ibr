/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include <core/system/Config.hpp>
//#include <projects/synthetic_ibr/renderer/BilateralFilterRenderer.hpp>
#include <projects/optix/renderer/OptixRaycaster.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
#include <core/view/SceneDebugView.hpp>


namespace sibr { 

	/**
	 * \class ImageSpaceGatheringView
	 * \brief Provides a real-time implementation of Image space gathering, Robison & Shirley, 2007.
	 * This relies on Optix to generate a perfect mirror image of the scene that is then blurred in screenspace using an adaptive radius based on the BRDF footprint.
	 */
	class  ImageSpaceGatheringView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(ImageSpaceGatheringView);


	public:

		/**
		 * Constructor
		 * \param mesh the scene merged geoemtry
		 * \param lightmap the corresponding diffuse lightmap
		 * \param fresnelmap image containing fresnel coefficient info in the same parameterization as the lightmap
		 * \param w rendering width
		 * \param h rendering height
		 * \param scale scale of the scene
		 */
		ImageSpaceGatheringView(
			const Mesh::Ptr & mesh, 
			const Texture2DRGBA32F::Ptr & lightmap,
			const Texture2DRGBA::Ptr& fresnelmap, 
			uint w, uint h, float scale);

		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		void onUpdate(Input& input) override;
		
		void onGUI() override;

		void registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler);

	protected:

		enum SyntheticLayer : int {
			DIFFUSE = 1,
			SPECULAR = 2,
			BOTH = DIFFUSE | SPECULAR
		};
		
		void resize(uint w, uint h);

		RenderTargetRGBA32F::Ptr _prepassRT, _specularRT, _isgRT;
		RenderTargetRGBA32F::Ptr _reflectedPos;

		OptixRaycaster::Ptr _raycaster;

		GLShader _prepassShader;
		GLuniform<sibr::Matrix4f> _prepassMvp;
		GLuniform<sibr::Vector3f> _prepassEye;

		GLShader _uvShader;

		SyntheticLayer _layerMode = SyntheticLayer::BOTH;
		
		GLShader _compositingShader;
		GLuniform<int> _compositingMode;
		GLuniform<float> _exposure;
		GLuniform<bool> _clampToLDR;

		GLShader _isgSearchGatherShader;
		GLuniform<float> _fovy;
		GLuniform<sibr::Vector3f> _isgEye;
		GLuniform<float> _isgA;
		GLuniform<float> _isgB;
		GLuniform<float> _isgSigma;
		GLuniform<int> _isgSamplesSearch;
		GLuniform<int> _isgSamplesGather;

		GLuniform<float> _scale;

		Mesh::Ptr _mesh;
		Texture2DRGBA32F::Ptr _lightmap;
		Texture2DRGBA::Ptr _fresnelmap;
		
		MultiMeshManager::Ptr _debugView;
		InteractiveCameraHandler::Ptr _handler;
		bool _applyBlur = true;
	};

} /*namespace sibr*/ 
