/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include"ImageSpaceGatheringView.hpp"

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>

#include <projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp>
#include <projects/synthetic_ibr/renderer/GeometryInfos.hpp>
#include <projects/synthetic_ibr/renderer/ProbesGatherScene.hpp>


#define PROGRAM_NAME "imageSpaceGathering"

using namespace sibr;



/** Implementation of Image space gathering using a filter similar to the one used in the main method. */
int main( int ac, char** av )
{
	
	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	SyntheticAppArgs myArgs;
	
	// window size
	uint win_width = myArgs.win_width;
	uint win_height = myArgs.win_height;
	myArgs.rendering_size = { 1920, 1080 };
	// Window setup
	sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);
	// Check rendering size
	const Vector2u usedResolution(myArgs.rendering_size.get()[0], myArgs.rendering_size.get()[1]);

	

	const std::string basePath = myArgs.dataset_path;
	// Load scene scale.
	const float scale = syntheticIBR::loadScale(basePath);
	// Load geometry.
	GeometryInfos::Ptr geomInfos;
	const auto mesh = ProbesGatherScene::loadGeometry(basePath, geomInfos);
	// Load lightmap.
	ImageRGBA32F::Ptr lightImg;
	{
		ImageRGB32F lightImgSrc;
		lightImgSrc.load(basePath + "/lightmap/scene_light_map.exr", false, true);
		lightImg.reset(new ImageRGBA32F(lightImgSrc.w(), lightImgSrc.h()));
		// Transfer roughness info.
		ImageRGBA lightmapRough;
		lightmapRough.load(basePath + "/lightmap/scene_light_map_mat.png", false, true);
#pragma omp parallel for
		for (int y = 0; y < int(lightImgSrc.h()); ++y) {
			for (int x = 0; x < int(lightImgSrc.w()); ++x) {
				lightImg(x, y)[0] = lightImgSrc(x, y)[0];
				lightImg(x, y)[1] = lightImgSrc(x, y)[1];
				lightImg(x, y)[2] = lightImgSrc(x, y)[2];
				lightImg(x, y)[3] = float(lightmapRough(x, y)[3])/255.0f;
			}
		}
	}
	
	auto lightMap = std::make_shared<Texture2DRGBA32F>(lightImg, SIBR_GPU_LINEAR_SAMPLING);
	ImageRGBA::Ptr fresnelImg(new ImageRGBA());
	fresnelImg->load(basePath + "/lightmap/scene_light_map_mat.png", false, true);
	auto fresnelMap = std::make_shared<Texture2DRGBA>(fresnelImg, SIBR_GPU_LINEAR_SAMPLING);

	ImageSpaceGatheringView::Ptr mainView(new ImageSpaceGatheringView(mesh, lightMap, fresnelMap, usedResolution[0], usedResolution[1], scale));


	// Raycaster.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->init();
	raycaster->addMesh(*mesh);

	// Camera handler for main view.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	generalCamera->setup(mesh, Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()));
	generalCamera->setFPSCameraSpeed(3.f);
	generalCamera->setClippingPlanes(0.1f, 5000.0f);


	// Add views to mvm.
	MultiViewManager        multiViewManager(window, false);
	multiViewManager.addIBRSubView("Image space gathering", mainView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
	multiViewManager.addCameraForView("Image space gathering", generalCamera);
	const std::string screenshotsPath = basePath + "/save/isg/";
	multiViewManager.setExportPath(screenshotsPath);

	// Top view
	const std::shared_ptr<sibr::MultiMeshManager>   debugView(new MultiMeshManager("Debug view"));
	debugView->addMesh("ISG TopView", mesh);
	multiViewManager.addSubView("Top view", debugView, usedResolution / 4);
	
	mainView->registerDebugObjects(debugView, generalCamera);

	CHECK_GL_ERROR;

	// Main looooooop.

	while (window.isOpened())
	{
		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}
		window.bind();
		window.viewport().bind();

		glClear(GL_COLOR_BUFFER_BIT);

		multiViewManager.onUpdate(sibr::Input::global());
		multiViewManager.onRender(window);
		window.swapBuffer();
		CHECK_GL_ERROR
	}

	return EXIT_SUCCESS;
}
