/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <optix_device.h>

#include "projects/optix/renderer/OptixSharedStructures.h"

using namespace sibr;

namespace sibr {

	extern "C" __global__ void __anyhit__reflposUV()
	{ /*! for this simple example, this will remain empty */
	}

	extern "C" __global__ void __miss__reflposUV()
	{
		sibr::Vector4f& prd = *(sibr::Vector4f*)getPRD<sibr::Vector4f>();
		// set to constant white as background color
		prd = { 1.0f,1.0f, 1.0f, 0.0f };
	}

	extern "C" __global__ void __closesthit__reflposUV()
	{
		const TriangleMeshData& sbtData = *(const TriangleMeshData*)optixGetSbtDataPointer();
		const int   primID = optixGetPrimitiveIndex();

		const sibr::Vector3u index = sbtData.index[primID];
		const float u = optixGetTriangleBarycentrics().x;
		const float v = optixGetTriangleBarycentrics().y;
		
		// Determine the world space position and UV coordinates at the surface.
		sibr::Vector3f pos = (1.0f - u - v) * sbtData.vertex[index[0]]
			+ u * sbtData.vertex[index[1]]
			+ v * sbtData.vertex[index[2]];
		sibr::Vector2f uv = (1.0f - u - v) * sbtData.texcoord[index[0]]
			+ u * sbtData.texcoord[index[1]]
			+ v * sbtData.texcoord[index[2]];
		
		// Pack the UVs as two 16 bits integers in a 32 bits int, reinterpret as float.
		const unsigned int uv0 = min(65535, uint(max(0.0f, 65535.0f * uv[0])));
		const unsigned int uv1 = min(65535, uint(max(0.0f, 65535.0f * uv[1])));
		const unsigned int packUV = ((uv0 & 0xffff) << 16) | ((uv1 & 0xffff));
		const float result = __uint_as_float(packUV);

		sibr::Vector4f& prd = *(sibr::Vector4f*)getPRD<sibr::Vector4f>();
		prd = { pos[0], pos[1], pos[2], result };

	}

	extern "C" __global__ void __raygen__renderReflectedFrame()
	{
		// compute a test pattern based on pixel ID
		const int ix = optixGetLaunchIndex().x;
		const int iy = optixGetLaunchIndex().y;

		sibr::Vector4f positionPRD = { 0.f, 0.0f, 0.0f, 0.0f };

		// Pack the pointer to the result color in the payload.
		uint32_t u0, u1;
		packPointer(&positionPRD, u0, u1);

		// Compute pixel coordinates, flipping to be in the same orientation as OpenGL.
		sibr::Vector2f screen(float(ix) + 0.5f, optixLaunchParams.size[1] - float(iy) - 0.5f);
		
		// Generate ray direction
		const float4 value = tex2D<float4>(optixLaunchParams.buffers.positions, ix, iy);
		const float4 valueDir = tex2D<float4>(optixLaunchParams.buffers.directions, ix, iy);
		float3 cudaPos = { value.x, value.y, value.z };
		float3 cudaDir = { valueDir.x, valueDir.y, valueDir.z };
		optixTrace(optixLaunchParams.traversable,
			cudaPos, cudaDir, optixLaunchParams.minDist, 1e20f, 0.0f,
			OptixVisibilityMask(255), OPTIX_RAY_FLAG_DISABLE_ANYHIT,
			SURFACE_RAY_TYPE, RAY_TYPE_COUNT, SURFACE_RAY_TYPE /* missSBTIndex */, u0, u1);

		// Pack the floats in the target.
		const uint32_t fbIndex = 4 * (ix + iy * optixLaunchParams.size[0]);
		
		// Convert to 32-bit values.
		optixLaunchParams.dst[fbIndex+0] = __float_as_uint(positionPRD[0]);
		optixLaunchParams.dst[fbIndex+1] = __float_as_uint(positionPRD[1]);
		optixLaunchParams.dst[fbIndex+2] = __float_as_uint(positionPRD[2]);
		optixLaunchParams.dst[fbIndex+3] = __float_as_uint(positionPRD[3]);
	}
}
