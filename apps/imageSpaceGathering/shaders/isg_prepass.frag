/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420


in vec3 worldPos;
in vec3 worldNormal;
in vec2 uvs;
in flat float mid;


uniform vec3 camPos;
layout(binding = 0) uniform sampler2D lightmap;
layout(binding = 1) uniform sampler2D fresnelmap;

layout(location = 0) out vec4 outDirMat;
layout(location = 1) out vec4 outPosConf;
layout(location = 2) out vec4 outNormalRoughMat;
layout(location = 3) out vec4 outColor;
layout(location = 4) out vec4 outFresnel;


void main(void) {
	vec4 rgbsmooth = texture(lightmap, vec2(uvs.x, 1.0-uvs.y));
	outFresnel = texture(fresnelmap, vec2(uvs.x, 1.0-uvs.y));
	
	// Reflected direction, position, confidence, diffuse appearance.
	vec3 wo = normalize(worldPos - camPos);
	vec3 n = normalize(worldNormal);
	vec3 refl = reflect(wo, n);
	outDirMat = vec4(refl, 1.0);
	outPosConf = vec4(worldPos, rgbsmooth.a == 0.0 ? -1.0 : 1.0);
	outColor = vec4(rgbsmooth.rgb, 1.0);

	// Retrieve the real roughness from our packed smoothness
	float smoothness = pow(rgbsmooth.a, 2.2);
	float roughness = smoothness == 0.0 ? 0.99 : (1.0-((smoothness * 255.0)-1.0)/254.0);

	outNormalRoughMat = vec4(n, clamp(roughness, 0.0, 0.99) + mid*10.0); // mid id an int, roughness is a 0.0-1.0.
}