/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in vec2 texcoord;

layout(binding = 0) uniform sampler2D lightmap;
layout(binding = 1) uniform sampler2D reflectedPosAndUV;
layout(binding = 2) uniform sampler2D posConfidence;

layout(location = 0) out vec4 outColor;


void main(void) {
	// This works on a per texel basis technically.
	ivec2 coords = ivec2(floor(gl_FragCoord.xy));
	// Read the position and UVs.
	float uvpack = texelFetch(reflectedPosAndUV, coords, 0).a;
	// Read back UVs.
	uint uvpacku = floatBitsToUint(uvpack);
	uint u0 = (uvpacku >> 16) & 0xffff;
	uint u1 = (uvpacku) & 0xffff;
	vec2 uvs = vec2(u0, u1)/65535.0;
	// Read lightmap.
	vec4 rgbSmooth = textureLod(lightmap, vec2(uvs.x, 1.0-uvs.y), 0.0);
	outColor.rgb = rgbSmooth.rgb;
	// Transmit confidence.
	float confidence = texelFetch(posConfidence, coords, 0).a;
	outColor.a = confidence;
}
	
