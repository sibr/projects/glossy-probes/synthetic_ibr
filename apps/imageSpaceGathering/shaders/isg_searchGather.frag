/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(binding = 0) uniform sampler2D position;
layout(binding = 1) uniform sampler2D normal;
layout(binding = 2) uniform sampler2D reflectedPosition;
layout(binding = 3) uniform sampler2D reflectedColor;
layout(binding = 4) uniform sampler2D fresnel;

layout(location = 0) out vec4 outColor;

uniform float fovy;
uniform vec3 camPos;
uniform float sceneScale;

uniform float a;
uniform float b;
uniform float sigma;
uniform int sampleCountParamSearch;
uniform int sampleCountGather;

//=====================================================

#define M_PI 3.14159265358979323846

//=====================================================

// convert to phong exponent and bound this one
float thetaMaxFromRoughness(const float roughness)
{
	float phong = 2.0 / (roughness*roughness) - 2.0;
	const float epsi = 0.244;
	float expo = 1.0 / (phong + 1.0);
	return acos(pow(epsi, expo)) * 2;
}

//=====================================================

// Eq. 10
float rangeFct(const vec3 pos0, const vec3 posI, const vec3 n0, const vec3 nI)
{
	float l = distance(pos0, posI) / sceneScale;
	float n = 1. - dot(n0, nI);
	return exp(-(a*l*l + b*n*n)/sigma);
}

//=====================================================

float gaussian(const float x, const float sigma)
{
	return exp((-0.5 * x * x) / (sigma * sigma));
}

//=====================================================

// half vector == normal vector, since we only sample mirror reflections
vec4 fresnelFactor(const vec4 fresnelValue, const float incidentAngle)
{
	return fresnelValue + (1 - fresnelValue) * pow(1 - incidentAngle, 5);
}

//=====================================================

// directly from Shirley and Chiu 2005
vec2 squareToDisk(const vec2 p)
{	
	float r, phi;
	float pi4 = M_PI / 4.;

	if (p.x > -p.y)
	{
		if (p.x > p.y)
		{
			r = p.x;
			phi = pi4 * p.y / p.x;
		}
		else
		{
			r = p.y;
			phi = pi4 * (2 - p.x / p.y);
		}
	}
	else
	{
		if (p.x < p.y)
		{
			r = -p.x;
			phi = pi4 * (4. + p.y / p.x);
		}
		else
		{
			r = -p.y;
			if (p.y != 0) phi = pi4 *  (6. - p.x / p.y);
			else phi = 0;
		}
	}
	return r * vec2(cos(phi), sin(phi));
}

//=====================================================

void main(void)
{
	ivec2 coord = ivec2(floor(gl_FragCoord.xy));
	
	vec4 pos0 = texelFetch(position, coord, 0);
	if (pos0.a != 1.) 
	{
		outColor = vec4(0);
		return;
	}
	ivec2 res = textureSize(position, 0).xy;
	
	vec3 n0 = texelFetch(normal, coord, 0).xyz;
	vec4 fresnelValue = vec4(texelFetch(fresnel, coord, 0).rgb, 1);
	vec3 viewRayDir = camPos - pos0.xyz;
	float thetaIncident = dot(normalize(viewRayDir), n0);

	float roughness = fract(texelFetch(normal, coord, 0).a);

	if (roughness == 0)
	{
		outColor = texelFetch(reflectedColor, coord, 0);
		outColor *= fresnelFactor(fresnelValue, thetaIncident);
		return;
	}
	
	float thetaMax = thetaMaxFromRoughness(roughness);
	
	//-------parameter search------------------------
	
	float Rp = thetaMax / fovy * res.y;

	float paramSearchSampleSpacing = Rp / sampleCountParamSearch;

	float dstSum = 0;
	float weightSum = 0;

	for (float y = -sampleCountParamSearch; y <= sampleCountParamSearch; y++)
	{
		for (float x = -sampleCountParamSearch; x <= sampleCountParamSearch; x++)
		{
			vec2 offset = paramSearchSampleSpacing * squareToDisk(vec2(x, y));
			vec2 searchCoord = (coord + offset + vec2(0.5)) / res;
			if (any(lessThan(searchCoord, vec2(0))) || any(greaterThanEqual(searchCoord, vec2(1)))) continue;
			vec3 primaryPos = texture(position, searchCoord).xyz;
			vec3 reflectedPos = texture(reflectedPosition, searchCoord).xyz;
			vec3 nI = texture(normal, searchCoord).xyz;

			float weight = rangeFct(pos0.xyz, primaryPos, n0, nI);

			dstSum += weight * distance(primaryPos, reflectedPos);
			weightSum += weight;
		}
	}

	float E = dstSum / weightSum;
	
	//-------gathering-------------------------------

	float e = length(viewRayDir);
	float alpha = atan(E * tan(thetaMax), e + E);

	float Rs = alpha / fovy * res.y;
	float Rg = Rs * thetaIncident;
	float spatialSigma = Rg / 3.;

	float gatherSampleSpacing = Rg / sampleCountGather;

	vec3 colorSum = vec3(0);
	weightSum = 0;

	for (float y = -sampleCountGather; y <= sampleCountGather; y++)
	{
		for (float x = -sampleCountGather; x <= sampleCountGather; x++)
		{
			vec2 offset = gatherSampleSpacing * squareToDisk(vec2(x,y));
			vec2 gatherCoord = (coord + offset + vec2(0.5)) / res;
			if (any(lessThan(gatherCoord, vec2(0))) || any(greaterThanEqual(gatherCoord, vec2(1)))) continue;
			
			vec3 reflColor = texture(reflectedColor, gatherCoord).rgb;
			vec3 primaryPos = texture(position, gatherCoord).xyz;
			vec3 nI = texture(normal, gatherCoord).xyz;

			float spatialWeight = gaussian(length(offset), spatialSigma);
			float rangeWeight = rangeFct(pos0.xyz, primaryPos, n0, nI);
			float weight = spatialWeight * rangeWeight;

			colorSum += weight * reflColor;
			weightSum += weight;
		}
	}	
	
	outColor = vec4(colorSum / weightSum , 1);
	outColor *= fresnelFactor(fresnelValue, thetaIncident);
}
	
