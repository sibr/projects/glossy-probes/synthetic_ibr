/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420


layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_uvs;
layout(location = 3) in vec3 in_normal;

uniform mat4 mvp;

out vec3 worldPos;
out vec3 worldNormal;
out vec2 uvs;
out flat float mid;

void main(void) {
	gl_Position = mvp * vec4(in_vertex,1.0);
	// World space info.
	worldPos = in_vertex.xyz;
	worldNormal = in_normal;
	uvs = in_uvs;
	mid = in_color.x;
}
