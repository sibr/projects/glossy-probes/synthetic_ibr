/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "ImageSpaceGatheringView.hpp"
#include <core/graphics/GUI.hpp>

extern "C" char embedded_ptx_code_isg[];

sibr::ImageSpaceGatheringView::ImageSpaceGatheringView(
	const sibr::Mesh::Ptr& mesh, 
	const sibr::Texture2DRGBA32F::Ptr& lightmap, 
	const sibr::Texture2DRGBA::Ptr& fresnelmap, 
	uint w, uint h, float scale) :
	sibr::ViewBase(w,h)
{
	_mesh = mesh;
	_lightmap = lightmap;
	_fresnelmap = fresnelmap;
	
	_isgSearchGatherShader.init("ISG SearchGather",
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_searchGather.vert"),
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_searchGather.frag"));
	_fovy.init(_isgSearchGatherShader, "fovy");
	_isgEye.init(_isgSearchGatherShader, "camPos");
	_isgA.init(_isgSearchGatherShader, "a");
	_isgB.init(_isgSearchGatherShader, "b");
	_isgSigma.init(_isgSearchGatherShader, "sigma");
	_scale.init(_isgSearchGatherShader, "sceneScale");
	_isgSamplesSearch.init(_isgSearchGatherShader, "sampleCountParamSearch");
	_isgSamplesGather.init(_isgSearchGatherShader, "sampleCountGather");

	_isgA = 1.f;
	_isgB = 1.f;
	_isgSigma = 0.05f;
	_isgSamplesSearch = 4;
	_isgSamplesGather = 6;
	_scale = scale;

	_compositingShader.init("Compositing", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_blendcommon.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/basic_composite.frag"));
	_compositingMode.init(_compositingShader, "mode");
	_exposure.init(_compositingShader, "exposure");
	_clampToLDR.init(_compositingShader, "clampToLDR");
	_exposure = 1.0f;
	_clampToLDR = true;

	_prepassShader.init("Prepass",
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_prepass.vert"),
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_prepass.frag"));
	_prepassMvp.init(_prepassShader, "mvp");
	_prepassEye.init(_prepassShader, "camPos");

	_uvShader.init("Prepass",
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_combo.vert"),
		sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_combo.frag"));

	_raycaster.reset(new OptixRaycaster());
	OptixRaycaster::Options rayOpts;
	rayOpts.entryName = "__raygen__renderReflectedFrame";
	rayOpts.anyName = "__anyhit__reflposUV";
	rayOpts.closestName = "__closesthit__reflposUV";
	rayOpts.missName = "__miss__reflposUV";
	rayOpts.ptxCode = std::string(embedded_ptx_code_isg);
	rayOpts.debug = false;
	_raycaster->createPrograms(rayOpts);
	_raycaster->createScene(*_mesh);
	_raycaster->bindProgramsAndScene();

	resize(w, h);
}

void sibr::ImageSpaceGatheringView::resize(uint w, uint h) {
	// We first trigger a spec renderer resize.
		// This will invalidate the textures registered by the optix raycaster.
	_prepassRT.reset(new RenderTargetRGBA32F(w, h, 0, 5));
	_raycaster->registerSource(*_prepassRT, 1);
	_raycaster->registerDirection(*_prepassRT, 0);
	_reflectedPos.reset(new RenderTargetRGBA32F(w, h, 0, 1));
	_raycaster->registerDestination(_reflectedPos);
	_specularRT.reset(new RenderTargetRGBA32F(w, h, 0, 1));
	_isgRT.reset(new RenderTargetRGBA32F(w, h, 0, 1));

}
void sibr::ImageSpaceGatheringView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
{
	if (dst.w() != _specularRT->w() || dst.h() != _specularRT->h()) {
		resize(dst.w(), dst.h());
	}
	// First pass: render Gbuffer.
	{
		_prepassRT->clear();
		_prepassRT->bind();
		glViewport(0, 0, _prepassRT->w(), _prepassRT->h());

		_prepassShader.begin();
		_prepassMvp.set(eye.viewproj());
		_prepassEye.set(eye.position());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _lightmap->handle());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _fresnelmap->handle());
		_mesh->render();
		_prepassShader.end();
		_prepassRT->unbind();
	}
	
	// Second pass: render via optix.
	_raycaster->onRender();

	// Third pass:apply uv from optix.
	_specularRT->bind();
	_uvShader.begin();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _lightmap->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _reflectedPos->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _prepassRT->handle(1));
	RenderUtility::renderScreenQuad();
	_uvShader.end();
	_specularRT->unbind();


	// Recap of the various rts content:
	// _reflectedPos: XYZ reflected/secondary world space position, UVs packed in A
	// _specularRT: RGB reflected/secondary color, A=-1 for diffuse primary surfaces, 1 everywhere else.
	// _prepassRT: 
	//		attachment 0: reflected world space direction, A=1
	//		attachment 1: XYZ primary world space position,  A=-1 for diffuse primary surfaces, 1 everywhere else.
	//		attachment 2: XYZ world space normal, fract(A) = roughness (Mitsuba), floor(A/10) = primary material ID.
	//		attachment 3: RGB primary surface color
	//		attachment 4: RGB fresnel term

	// Fourth pass: Image space gathering
	if (_applyBlur) {
		const InputCamera cam(eye, dst.w(), dst.h());
		_fovy = cam.fovy();
		_isgEye = eye.position();

		_isgRT->bind();
		_isgSearchGatherShader.begin();
		_fovy.send();
		_isgEye.send();
		_isgA.send();
		_isgB.send();
		_isgSigma.send();
		_scale.send();
		_isgSamplesSearch.send();
		_isgSamplesGather.send();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _prepassRT->handle(1));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _prepassRT->handle(2));
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, _reflectedPos->handle());
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, _specularRT->handle(0));
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, _prepassRT->handle(4));


		RenderUtility::renderScreenQuad();

		_isgSearchGatherShader.end();
		_isgRT->unbind();
	}
	

	// Display result:
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	glDisable(GL_DEPTH_TEST);

	// Composite optional diffuse and specular layers.
	_compositingShader.begin();
	_clampToLDR.send();
	_exposure.send();
	_compositingMode.set(int(_layerMode));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _prepassRT->handle(3));
	glActiveTexture(GL_TEXTURE1);
	if (_applyBlur) glBindTexture(GL_TEXTURE_2D, _isgRT->handle());
	else glBindTexture(GL_TEXTURE_2D, _specularRT->handle());
	RenderUtility::renderScreenQuad();
	_compositingShader.end();
	
	dst.unbind();
	
}

void sibr::ImageSpaceGatheringView::onGUI()
{
	if(ImGui::Begin("ISG options")) {
		ImGui::RadioButton("Diffuse", (int*)&_layerMode, 1); ImGui::SameLine();
		ImGui::RadioButton("Specular", (int*)&_layerMode, 2); ImGui::SameLine();
		ImGui::RadioButton("Both", (int*)&_layerMode, 3); 
		
		ImGui::Checkbox("Apply blur", &_applyBlur);
		ImGui::InputFloat("a", (float*)&_isgA);
		ImGui::InputFloat("b", (float*)&_isgB);
		ImGui::InputFloat("sigma", (float*)&_isgSigma);
		ImGui::InputInt("Search Samples", (int*)&_isgSamplesSearch);
		ImGui::InputInt("Gather Samples", (int*)&_isgSamplesGather);
	}
	ImGui::End();
}

void sibr::ImageSpaceGatheringView::registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler)
{
	_debugView = debugView;
	_handler = handler;
	_debugView->addMesh("Proxy", _mesh).setColor({ 0.8f, 0.8f, 0.8f });
}


void sibr::ImageSpaceGatheringView::onUpdate(Input& input)
{
	// Live shader reloading for debug.
	if (input.key().isReleased(Key::P)) {

		const std::string shaderSrcPath = sibr::getBinDirectory() + "/../../src/projects/synthetic_ibr/apps/imageSpaceGathering/shaders/";
		const std::string shaderDstPath = sibr::getShadersDirectory("synthetic_ibr") + "/";
		const auto files = sibr::listFiles(shaderSrcPath, false, false, { "vert", "frag", "geom" });
		for (const auto & file : files) {
			sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
		}
		_prepassShader.init("Prepass",
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_prepass.vert"),
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_prepass.frag"));
		_uvShader.init("Prepass",
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_combo.vert"),
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_combo.frag"));
		_isgSearchGatherShader.init("ISG Phase 1",
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_searchGather.vert"),
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/isg_searchGather.frag"));
	}
	
}
