/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include "projects/synthetic_ibr/renderer/ProbesView.hpp"

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>


#define PROGRAM_NAME "sibr_synthetic_ibr_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " --path <dataset-path> (containing cameras.lookat, scene.xml, exr/*.exr)" " --suff exr file suffix" 	                                "\n"
;

/** Main viewer for Glossy Probe Reprojection for Interactive Global illumination. */
int main(int ac, char** av) {

	CommandLineArgs::parseMainArgs(ac, av);
	SyntheticAppArgs myArgs;

	// Window setup
	sibr::Window		window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);
	// Check rendering size
	const Vector2u usedResolution = myArgs.rendering_size.get().cast<unsigned int>();

	ProbesGatherScene::Ptr scene(new ProbesGatherScene(myArgs));

	// Create the view.
	ProbesView::Ptr	probesView(new ProbesView(scene, usedResolution[0], usedResolution[1], myArgs.lutPath, myArgs.ssdiff));
	
	// Raycaster.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->addMesh(scene->proxies()->proxy());

	// Camera handler for main view.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	generalCamera->setup(scene->proxies()->proxyPtr(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()));
	generalCamera->setFPSCameraSpeed(3.0f);
	generalCamera->setClippingPlanes(0.1f, 5000.0f);

	// Add views to the MVM.
	MultiViewManager        multiViewManager(window, false);
	multiViewManager.addIBRSubView("Probes render view", probesView, usedResolution); //ImGuiWindowFlags_NoBringToFrontOnFocus
	multiViewManager.addCameraForView("Probes render view", generalCamera);
	const std::string screenshotsPath = scene->basePath() + "/save/" + scene->name() + "/";
	multiViewManager.setExportPath(screenshotsPath);

	// Top view
	std::shared_ptr<sibr::MultiMeshManager>   debugView(nullptr);
	if (!myArgs.skipTopView) {
		debugView = std::shared_ptr<sibr::MultiMeshManager>(new MultiMeshManager("Debug view"));
		debugView->addMeshAsLines("Gizmo", RenderUtility::createAxisGizmo()).setDepthTest(false).setColorMode(MeshData::ColorMode::VERTEX);
		multiViewManager.addSubView("Top view", debugView, usedResolution);
	}
	probesView->registerDebugObjects(debugView, generalCamera);

	CHECK_GL_ERROR;

	// Main looooooop.
	
	while (window.isOpened()) {

		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}
		window.bind();
		window.viewport().bind();

		glClear(GL_COLOR_BUFFER_BIT);
		multiViewManager.onUpdate(sibr::Input::global());
		multiViewManager.onRender(window);

		CHECK_GL_ERROR;
		window.swapBuffer();
	}

	return EXIT_SUCCESS;




}