/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/graphics/Window.hpp>
#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/system/String.hpp"
#include "core/system/XMLTree.h"
#include "core/system/Transform3.hpp"


using namespace sibr;


struct EvalArgs : virtual AppArgs {
	Arg<bool> maps = { "maps", "Generate color maps" };
	RequiredArg<std::string> path = { "path", "Images path" };
	Arg<std::string> out = { "output", "", "Output image directory (for color maps generation only)" };
	Arg<float> scale = { "scale", 1.0f, "error scaling (for color maps generation only)" };
	Arg<bool> square = { "square", "squared energy (for color maps generation only)" };
};


// Metric evaluation.

double evalRMSE(ImageRGB::Ptr gt, ImageRGB::Ptr img) {
	double accum = 0.0;

	for (uint y = 0; y < gt->h(); ++y) {
		for (uint x = 0; x < gt->w(); ++x) {
			sibr::Vector3d dif = gt(x, y).cast<double>() - img(x, y).cast<double>();
			dif /= 255.0;
			accum += dif.squaredNorm();
		}
	}
	const double mse = accum / (gt->h() * gt->w() * 3);

	return std::sqrt(mse);
}

double evalRMSE(ImageL32F::Ptr gt, ImageL32F::Ptr img) {
	double accum = 0.0;

	for (uint y = 0; y < gt->h(); ++y) {
		for (uint x = 0; x < gt->w(); ++x) {
			double dif = gt(x, y)[0] - img(x, y)[0];
			accum += dif*dif;
		}
	}
	const double mse = accum / (gt->h() * gt->w());

	return std::sqrt(mse);
}


sibr::Vector3d getSSIM(const sibr::Vector3d & mean0, const sibr::Vector3d& mean1,
	const sibr::Vector3d& variance0, const sibr::Vector3d& variance1, const sibr::Vector3d& covariance)
{
	// compute SSIM
	const double k1 = 1e-2;//0.01;
	const double k2 = 3e-2;//0.03;
	const double L = 1; // value range (2^8 - 1 => 255, we have 0-1 => 1
	const double c1c = (k1 * L) * (k1 * L);
	const double c2c = (k2 * L) * (k2 * L);
	const sibr::Vector3d c1 = { c1c, c1c, c1c };
	const sibr::Vector3d c2 = { c2c, c2c, c2c };

	const sibr::Vector3d ssim = ((2.0 * mean0.cwiseProduct(mean1) + c1).cwiseProduct(2 * covariance + c2)).cwiseQuotient(((mean0.cwiseProduct(mean0) + mean1.cwiseProduct(mean1) + c1).cwiseProduct(variance0 + variance1 + c2)));

	return ssim;
}


double getSSIM(double mean0, double mean1,
	double variance0, double variance1, double covariance)
{
	// compute SSIM
	const double k1 = 1e-2;//0.01;
	const double k2 = 3e-2;//0.03;
	const double L = 1; // value range (2^8 - 1 => 255, we have 0-1 => 1
	const double c1 = (k1 * L) * (k1 * L);
	const double c2 = (k2 * L) * (k2 * L);

	double ssim = ((2.0 * mean0*(mean1) + c1)*(2 * covariance + c2))/(((mean0*(mean0) + mean1*(mean1) + c1)*(variance0 + variance1 + c2)));

	return ssim;
}

double gaussian(const double x, const double mu, const double sigma)
{
	return 1.0 / (sigma * std::sqrt(2.0 * M_PI)) * std::exp((-0.5 * ((x - mu) * (x - mu))) / (sigma * sigma));
}

double gaussian(const double x, const double sigma)
{
	return gaussian(x, 0, sigma);
}

double evalSSIM(ImageRGB::Ptr gt, ImageRGB::Ptr img) {


	double accum = 0.0;

	for (int py = 0; py < int(gt->h()); ++py) {
		for (int px = 0; px < int(gt->w()); ++px) {
			const sibr::Vector3d centerColor0 = gt(px, py).cast<double>() / 255.0;
			const sibr::Vector3d centerColor1 = img(px, py).cast<double>() / 255.0;
			
			sibr::Vector3d ssimOutput(0.0, 0.0, 0.0);

			//ivec2 position = ivec2(gl_FragCoord.xy);
			//ivec2 resolution = min(textureSize(image0, 0), textureSize(image1, 0));
			const int radius = 4;
			const double domainStdDev = std::max(radius, 1) / 3.2f;

			sibr::Vector3d mean0(0.0,.0,.0);
			sibr::Vector3d mean0Squared(0.0, .0, .0);
			sibr::Vector3d mean1(0.0, .0, .0);
			sibr::Vector3d mean1Squared(0.0, .0, .0);
			sibr::Vector3d mean0x1(0.0, .0, .0);
			double weightSum = 0;

			for (int y = -radius; y <= radius; y++)
			{
				for (int x = -radius; x <= radius; x++)
				{
					sibr::Vector2i newCoords = { px + x, py + y };
					if (newCoords[0] < 0 || newCoords[1] < 0 ||
						newCoords[0] >= int(gt->w()) || newCoords[1] >= int(gt->h())) {
						continue;
					}

					sibr::Vector3d color0 = gt(newCoords).cast<double>() / 255.0;
					sibr::Vector3d color1 = img(newCoords).cast<double>() / 255.0;
					const sibr::Vector2d offset(x, y);
					double dWeight = gaussian(offset.norm(), domainStdDev);
					double weight = dWeight;

					mean0 += weight * color0;
					mean0Squared += weight * color0.cwiseProduct(color0);

					mean1 += weight * color1;
					mean1Squared += weight * color1.cwiseProduct(color1);

					mean0x1 += weight * color0.cwiseProduct(color1);

					weightSum += weight;
				}
			}

			// normalize
			if (weightSum < 1e-10) continue;
			mean0 /= weightSum;
			mean0Squared /= weightSum;
			mean1 /= weightSum;
			mean1Squared /= weightSum;
			mean0x1 /= weightSum;

			// compute variances and covariance
			const sibr::Vector3d variance0 = mean0Squared - mean0.cwiseProduct(mean0);
			const sibr::Vector3d variance1 = mean1Squared - mean1.cwiseProduct(mean1);
			const sibr::Vector3d covariance = mean0x1 - mean0.cwiseProduct(mean1);

			// compute SSIM
			ssimOutput = getSSIM(mean0, mean1, variance0, variance1, covariance);
			accum += ssimOutput[0] + ssimOutput[1] + ssimOutput[2];
		}
	}

	return accum / (gt->h() * gt->w() * 3);
}


double evalSSIM(ImageL32F::Ptr gt, ImageL32F::Ptr img) {


	double accum = 0.0;

	for (int py = 0; py < int(gt->h()); ++py) {
		for (int px = 0; px < int(gt->w()); ++px) {
			const double centerColor0 = gt(px, py)[0];
			const double centerColor1 = img(px, py)[0];

			double ssimOutput = 0.0;

			//ivec2 position = ivec2(gl_FragCoord.xy);
			//ivec2 resolution = min(textureSize(image0, 0), textureSize(image1, 0));
			const int radius = 4;
			const double domainStdDev = std::max(radius, 1) / 3.2f;

			double mean0 = 0.0;
			double mean0Squared = 0.0;
			double mean1 = 0.0;
			double mean1Squared = 0.0;
			double mean0x1 = 0.0;
			double weightSum = 0;

			for (int y = -radius; y <= radius; y++)
			{
				for (int x = -radius; x <= radius; x++)
				{
					sibr::Vector2i newCoords = { px + x, py + y };
					if (newCoords[0] < 0 || newCoords[1] < 0 ||
						newCoords[0] >= int(gt->w()) || newCoords[1] >= int(gt->h())) {
						continue;
					}

					double color0 = gt(newCoords)[0];
					double color1 = img(newCoords)[0];
					const sibr::Vector2d offset(x, y);
					double dWeight = gaussian(offset.norm(), domainStdDev);
					double weight = dWeight;

					mean0 += weight * color0;
					mean0Squared += weight * color0 * color0;

					mean1 += weight * color1;
					mean1Squared += weight * color1 * color1;

					mean0x1 += weight * color0 * color1;

					weightSum += weight;
				}
			}

			// normalize
			if (weightSum < 1e-10) continue;
			mean0 /= weightSum;
			mean0Squared /= weightSum;
			mean1 /= weightSum;
			mean1Squared /= weightSum;
			mean0x1 /= weightSum;

			// compute variances and covariance
			const double variance0 = mean0Squared - mean0 * (mean0);
			const double variance1 = mean1Squared - mean1 * (mean1);
			const double covariance = mean0x1 - mean0 * (mean1);

			// compute SSIM
			ssimOutput = getSSIM(mean0, mean1, variance0, variance1, covariance);
			accum += ssimOutput;
		}
	}

	return accum / (gt->h() * gt->w());
}

void numericEval(const std::string& path) {
	const auto dirs = sibr::listSubdirectories(path);

	struct Scores {
		double dssimRGB = 0.0;
		double dssimLum = 0.0;
		double rmseRGB = 0.0;
		double rmseLum = 0.0;
		int count = 0;
	};

	std::map<std::string, Scores> avgs;

	for (const auto& dir : dirs) {
		const std::string dirPath = path + "/" + dir + "/";
		const auto imgs = sibr::listFiles(dirPath, false, false, { "png", "bmp" });
		ImageRGB::Ptr gt;
		std::vector<ImageRGB::Ptr> comps;
		ImageL32F::Ptr gtLum;
		std::vector<ImageL32F::Ptr> compsLum;
		std::vector<std::string> names;
		for (const auto& img : imgs) {
			if (img.find("_mitsu") != std::string::npos) {
				gt.reset(new ImageRGB());
				gt->load(dirPath + img, false);
				gtLum.reset(new ImageL32F(gt->w(), gt->h()));
#pragma omp parallel for
				for (int y = 0; y < int(gt->h()); ++y) {
					for (int x = 0; x < int(gt->w()); ++x) {
						const sibr::Vector3f rgb = gt(x, y).cast<float>() / 255.0f;
						gtLum(x, y)[0] = rgb.dot(sibr::Vector3f{ 0.2126f, 0.7152f, 0.0722f });
					}
				}
			} else {
				comps.emplace_back(new ImageRGB());
				comps.back()->load(dirPath + img, false);
				names.push_back(sibr::removeExtension(img));
				auto& ig = comps.back();
				compsLum.emplace_back(new ImageL32F(ig->w(), ig->h()));
				auto& lumIg = compsLum.back();
#pragma omp parallel for
				for (int y = 0; y < int(ig->h()); ++y) {
					for (int x = 0; x < int(ig->w()); ++x) {
						const sibr::Vector3f rgb = ig(x, y).cast<float>() / 255.0f;
						lumIg(x, y)[0] = rgb.dot(sibr::Vector3f{0.2126f, 0.7152f, 0.0722f});
					}
				}
			}
		}


		std::stringstream log;
		log << std::setprecision(20);
		for (int i = 0; i < comps.size(); ++i) {
			log << names[i] << ":";
			const double ssim = evalSSIM(gt, comps[i]);
			const double dssim = 0.5 * (1.0 - ssim);
			log << " dssim RGB: " << dssim;
			const double rmse = evalRMSE(gt, comps[i]);
			log << " rmse RGB: " << rmse; 
			const double ssimLum = evalSSIM(gtLum, compsLum[i]);
			const double dssimLum = 0.5 * (1.0 - ssimLum);
			log << " dssim lum: " << dssimLum;
			const double rmseLum = evalRMSE(gtLum, compsLum[i]);
			log << " rmse lum: " << rmseLum;
			log << std::endl;
			// Accumulate scores.
			// Base name.
			const std::string baseName = names[i].substr(names[i].find('_') + 1);
			avgs[baseName].dssimLum += dssimLum;
			avgs[baseName].dssimRGB += dssim;
			avgs[baseName].rmseLum += rmseLum;
			avgs[baseName].rmseRGB += rmse;
			avgs[baseName].count += 1;
		}
		std::cout << std::endl << log.str() << std::endl;
	}

	for (const auto& avgScore : avgs) {
		std::stringstream log;
		log << std::setprecision(20);
		log << avgScore.first << ":";
		const auto& sc = avgScore.second;
		log << " dssim Lum: " << (sc.dssimLum / double(sc.count));
		log << " dssim RGB: " << (sc.dssimRGB / double(sc.count));
		log << " rmse Lum: " << (sc.rmseLum / double(sc.count));
		log << " rmse RGB: " << (sc.rmseRGB / double(sc.count));
		std::cout << std::endl << log.str() << std::endl;
	}
}

void errorMap(const std::string& path, const std::string& pathOut, float scale, bool square) {
	sibr::makeDirectory(pathOut);

	const auto dirs = sibr::listSubdirectories(path);
	
	for (const auto& dir : dirs) {

		const std::string dirPath = path + "/" + dir + "/";
		const auto imgs = sibr::listFiles(dirPath, false, false, { "png", "bmp" });

		ImageRGB32F gt;
		std::vector<ImageRGB32F::Ptr> comps;
		std::vector<std::string> names;

		const std::string pathOutFrame = pathOut + "/" + dir + "/";
		sibr::makeDirectory(pathOutFrame);
		
		// Find the reference.
		for (const auto& img : imgs) {
			if (img.find("_mitsu") != std::string::npos) {
				gt.load(dirPath + img, false);
			} else {
				comps.emplace_back(new ImageRGB32F());
				comps.back()->load(dirPath + img, false);
				names.push_back(sibr::removeExtension(img));
			}
		}

		for (size_t iid = 0; iid < names.size(); ++iid) {
			const std::string fileOut = pathOutFrame + "/" + names[iid] + "_map.png";

			ImageRGB map(gt.w(), gt.h(), sibr::Vector3ub(0, 0, 0));

#pragma omp parallel for
			for (long y = 0; y < long(gt.h()); ++y) {
				for (long x = 0; x < long(gt.w()); ++x) {
					// Compute the error.
					const sibr::Vector3f& refCol = gt(x, y);
					const sibr::Vector3f& metCol = comps[iid](x, y);
					const sibr::Vector3f diff = metCol - refCol;
					const sibr::Vector3f rse = scale * (square ? (diff.cwiseProduct(diff)).eval() : diff.cwiseAbs().eval());
					const sibr::Vector3f err = 255.0f * sibr::clamp(rse, sibr::Vector3f(0.0f, 0.0f, 0.0f), sibr::Vector3f(1.0f, 1.0f, 1.0f));
					map(x, y) = err.cast<uchar>();
				}
			}
			map.save(fileOut);
		}
	}
}

/** Compute error metrics on images comapred to a reference rendering. Compute color error maps. */
int main(int ac, char** av) {
	CommandLineArgs::parseMainArgs(ac, av);
	EvalArgs args;
	if (args.maps) {
		errorMap(args.path, args.out, args.scale, args.square);
	}
	else {
		numericEval(args.path);
	}
	
	return EXIT_SUCCESS;
}
