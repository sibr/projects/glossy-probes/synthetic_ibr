# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "synthetic_ibrPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/glossy-probes/synthetic_ibr")
set(PROJECT_DESCRIPTION "Glossy Probe Reprojection for Interactive Global Illumination (paper reference : http://www-sop.inria.fr/reves/Basilic/2020/RLPWSD20/)")
set(PROJECT_TYPE "OURS")