/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/** @page glossyProbesComp Comparisons

 *  \tableofcontents
 
    For most external comparisons, you will have to record a path in SIBR and save it as a `lookat` file.


    \section secGPRGIGroundTruth Ground truth comparison

 	In the `mitsuba` repository, there is a `groundTruthRender.py` script.
 	From `mitsubaroot/mitsuba/`, run:

 		python groundTruthRender.py path/to/file.lookat root/scene/scene.xml --samples 1024 --output path/to/output_exr/

 	To generate a video or a PNG, the HDR renderings will have to be tonemaped. From `sibr/install/bin` run:

 		./conversionMitsubaRenders_rwdi.exe --path path/to/output_exr/ --output path/to/output_png/

 	The `--pref N` option can be specified to remove the first N letters of each file name (to get rid of the "Cam" prefix for instance).
 


    \section secGPRGIFalcor Real-time ray/path tracing comparisons

 	You will have to clone and install our modified version of Falcor, including DXR support: https://gitlab.inria.fr/sibr/projects/glossy-probes/falcor  
 	Follow the README for the complete setup, including NVAPI. Make sure that you are building the Release DirectX 12 version. Build the Falcor and Mogwai solutions.

 	For both comparisons, Falcor will look for the following hard-coded lookat path: `root/paths/path_falcor.lookat`. If a path at this predefined location is present, it will start playing. You can control playback in the `Time` panel (F9).  
 	To record in Falcor, press Shift+F12 to open the recording panel. Keep the `Uncompressed` format, set the framerate to 30FPS, and define a time range corresponding to the duration of your path. Press `Start recording`, select the destination file with the `avi` extension and record.

 	\subsection subsecFalcorRTRTLightmap Real-time ray-tracer and light map

 	Edit the `LightmapRTX.py` script found in Falcor `Bin/x64/Release/Data` directory: update the `sceneRootDir` variable to point to the scene `root/`.  
 	Start Mogwai.exe. Load the `LightmapRTX.py` script, the scene will load automatically. Under the `Lightmap Experiments` header, in the `TraceReflection` section you can adjust the number of bounces and which surfaces should be treated as glossy. You can enable the Optix denoiser in the `Denoiser` section.

 	\note To adjust the number of BRDF samples at the first (and optionally second) bounce, you will have to edit the defined `SAMPLES_COUNT_LRTX` in the source `Source/RenderPasses/LightmapRTX/Shaders/reflectionRay.slang.h` and recompile.

 	\subsection subsecFalcorRTPT Real-time path tracer

 	Start Mogwai.exe. Load the `PathTracer.py` script, found in Falcor `Bin/x64/Release/Data` directory. It will load the Arcade scene by default. You can drag-drop the desired Falcor-compatible model into Mogwai once the script is loaded (or edit the Python script to specify what scene to load). We only provide the bathroom scene in this Falcor specific format. Options can be adjusted in the GUI. Under the `PathTracerGraph` header, you can edit the sample count and ray depth in the `MegakernelPathTracer` section. You can enable the Optix denoiser in the `Denoiser` section.



    \section secGPRGIG3D Light field probes comparison

    Original implementation of 'Real-time global illumination using precomputed light field probes', McGuire, M., Mara, M., Nowrouzezahrai, D., & Luebke, D. (2017, February), https://dl.acm.org/doi/abs/10.1145/3023368.3023378 .

    \subsection setupG3D Setup

    You will have to clone and install G3D (https://casual-effects.com/g3d/www/index.html) by following the instructions at https://casual-effects.com/g3d/G3D10/readme.md.html?#windowsmanual , the current revision is 6994.  
    You can then clone the Light Field probes repository (https://gitlab.inria.fr/sibr/projects/glossy-probes/g3d-lightfield) in a `lightField` directory at the same level as `G3D10`, `external`,...  As this is the original implementation and the code is not officialy public, please first contact the original authors to request access to the paper implementation, we will then be able to give you access to this repository.  
    Open `lightField/light_field.sln`, build the main project in Release x64.

    \subsection prepareG3D Preparing a scene

    You first need a scene in the G3D format, defined by a `MYSCENE.Scene.Any` file and auxiliary data (geometry and textures). You can check the `bathroom` dataset for an example.

    You will have to render "regular" panoramic probes using Mitsuba. To do this for a given list of probes named NAME, use the following command from `mitsubaroot/mitsuba/`:

    	python g3dProbeRenders.py root/probes/NAME.positions root/scene/scene.xml --samples 1024 --output root/g3d/probes/

    You should then convert them to per-face images, from `sibr/install/bin` run:

    	./g3dUtilities_rwdi.exe --path root/ --name NAME

    This will also generate a specification file at `root/g3d/NAME.LightFieldModelSpecification.Any` that defines the extent and number of probes, along with the data location.   
    You will have to place this file in a location accessible to G3D (in `lightField/data-files/` for instance), rename it to `MYSCENE.LightFieldModelSpecification.Any` and make sure that in `lightField/source/App.cpp`, line 98 the proper `MYSCENE` name is specified for automatic loading.

    Running the `light_field` app should then load the scene and precomputed cubemaps and display the scene (loading can take a few minutes).

    \warning Make sure that the probes are loaded in the right order in G3D.

    \subsection videoG3D Playing a path

    To play a recorded path, it first has to be encoded as a scene animation in `MYSCENE.Scene.Any`.  
    From `sibr/install/bin` run:

    	./g3dUtilities_rwdi.exe --camera path/to/file.lookat

    It will generate a `path/to/file.g3dpath` file, containing a long list of matrices and timings. You have to copy this content to `MYSCENE.Scene.Any`, in the `Camera` object.
    You should then have in the scene file:

    	...
    	camera = Camera {
    		...
    		track = PFrameSpline{
				control = (...);
				time = (...);
				interpolation = "LINEAR";
			};
		};
		...

	This path will be loaded along with the scene. You can start playing it by selecting the `camera` track from the camera list in the `Scene Editor` panel (or press F2).  
	You can set recording options in the corresponding GUI panel, and press Record or F6 to start recording. Press F6 again to stop the recording and select the destination file. Give some time to G3D to save the video before quitting.

    \section secGPRGIISG Image-Space Gathering comparison

    Implementation of 'Image space gathering', Robison, A., & Shirley, P. (2009, August), https://dl.acm.org/doi/pdf/10.1145/1572769.1572784.   
 	In SIBR, build the imageSpaceGathering application and run it on an existing dataset with:

 		./imageSpaceGathering_rwdi.exe --path root/ (--rendering-size 1920 1080) (--no-topview) (--vsync 0)

 	You can then load a standard SIBR recorded path and record it using the SIBR tools.



    \section secGPRGIULR Unstructured Lumigraph comparison

 	In SIBR, build the syntheticULRApp application and run it on an existing dataset with:

 		./syntheticULRApp_rwdi.exe --path root/ --name NAME (--rendering-size 1920 1080) (--no-topview) (--vsync 0)

 	You can then load a standard SIBR recorded path and record it using the SIBR tools.



 	\section secGPRGIIEval Numeric evaluation

 	Numeric evaluation is performed on a subset of frames along a path, containing only the rendered glossy layer. Displaying only this layer can be done for each engine:

 	- in SIBR: select the "Specular" radio button in the viewer GUI
 	- in Falcor: toggle "Only display specular layer" in each application GUI
 	- in G3D: In the "Visualize" tab, select the FILTER_GLOSSY visualization mode
 	- in Mitsuba: add the `--speconly` flag when calling the script

 	\note Because Falcor and G3D can only export videos, you will have to extract frames using ffmpeg:

 		ffmpeg.exe -i VIDEOPATH -vf fps=30 OUTPATH/%8d.png

    \warning Make sure to use an uncompressed video format when possible

 	Once you have selected frames you want to evaluate metrics on, you should put them in the following hierarchy:

 		evaluation/
 			frame0/
 				XXX_mitsu.png < this is the reference offline rendering
 				XXX_ours.png
 				XXX_isg.png
 				...
 			frame1/
 				XXX_mitsu.png
 				XXX_ours.png
 				...
 			...

 	\warning All frame directories should contain the same number of images with the same suffixes. Furthermore, the `_mitsu` suffix is mandatory for the reference image.

 	You can then call, from `sibr/install/bin`:

 		./syntheticEvaluation_rwdi.exe --path evaluation/

 	This will output the SSIM and RMSE on both RGB and luminance images, for each frame and each method. It will also output the average metrics over all frames for each method.

 	If you want to generate error maps (squared error, scaled 10 times), you can run with the following options from `sibr/install/bin`:

 		./syntheticEvaluation_rwdi.exe --path evaluation/ --output maps/ --square --scale 10.0
 *
 */

