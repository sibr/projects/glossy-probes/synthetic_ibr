/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/graphics/Window.hpp>
#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/Input.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/system/String.hpp"
#include "core/system/XMLTree.h"
#include "core/system/Transform3.hpp"
#include "projects/synthetic_ibr/renderer/MeshOBJExporter.hpp"
#include "projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp"
#include <core\imgproc\PoissonReconstruction.hpp>
#include <core\assets\Config.hpp>
#include <array>

#include "core/view/MultiMeshManager.hpp"
#include "core/view/MultiViewManager.hpp"

#define PROGRAM_NAME "synthetic_ibr_probes_placement"

using namespace sibr;

struct ProbesPlacementAppArgs {
	RequiredArg<std::string> mesh = { "mesh", "path to a mesh or mitsuba scene" };
};


/** Write probe to disk. text file with X Y Z on each line. */
void saveProbes(const std::vector<sibr::Vector3f> & probes, const std::string & path) {
	const std::string outPath = sibr::removeExtension(path);
	sibr::Mesh debugMesh(false);
	debugMesh.vertices(probes);
	debugMesh.save(outPath + ".ply");

	// Output
	std::ofstream out(outPath + ".positions");
	for (const sibr::Vector3f& pos : probes) {
		out << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
	}
	out.close();
}

/** Load existing probes, same format as for the export. */
void loadProbes(const std::string & inputPath, sibr::Vector3i & steps, sibr::Vector3f & minis, sibr::Vector3f & maxis) {
	std::vector<sibr::Vector3f> probesLocs;
	std::ifstream file(inputPath);
	if (!file.is_open()) {
		SIBR_WRG << "unable to load file at path " << inputPath << "." << std::endl;
		return;
	}
	while (!file.eof()) {
		sibr::Vector3f v;
		file >> v[0] >> v[1] >> v[2];
		if (!file.fail()) {
			probesLocs.push_back(v);
		}
	}
	SIBR_LOG << "Found " << probesLocs.size() << " probes." << std::endl;
	if (probesLocs.empty()) {
		return;
	}

	minis = maxis = probesLocs[0];

	// Not super robust if floating point imprecisions.
	std::set<float> xs, ys, zs;
	for (const sibr::Vector3f& probe : probesLocs) {
		minis = minis.cwiseMin(probe);
		maxis = maxis.cwiseMax(probe);
		xs.insert(probe[0]);
		ys.insert(probe[1]);
		zs.insert(probe[2]);
	}
	// Check estimation.
	if (xs.size() * ys.size() * zs.size() != probesLocs.size()) {
		SIBR_WRG << "Inconsistent number of probes: " << xs.size() << "x" << ys.size() << "x" << zs.size() << " vs " << probesLocs.size() << "." << std::endl;
	}

	steps[0] = int(xs.size());
	steps[1] = int(ys.size());
	steps[2] = int(zs.size());
}

/** Place probes on a regular grid inside a box. */
void generateGrid(const sibr::Vector3i & grid, const sibr::Vector3f & minis, const sibr::Vector3f& maxis, std::vector<sibr::Vector3f> & probes) {
	probes.resize(grid.x() * grid.y() * grid.z());

	const sibr::Vector3f& v0 = minis;
	const sibr::Vector3f delta = maxis - minis;
	const sibr::Vector3f ax(delta[0], 0.0f, 0.0f);
	const sibr::Vector3f ay(0.0f, delta[1], 0.0f);
	const sibr::Vector3f az(0.0f, 0.0f, delta[2]);
	
	int iid = 0;
	for (int x = 0; x < grid.x(); ++x) {
		for (int y = 0; y < grid.y(); ++y) {
			for (int z = 0; z < grid.z(); ++z) {
				const sibr::Vector3f pos = v0 +
					float(x) / float(grid.x() - 1) * ax +
					float(y) / float(grid.y() - 1) * ay +
					float(z) / float(grid.z() - 1) * az;
				probes[iid] = pos;
				++iid;
			}
		}
	}
}

/** Generate box mesh for visualisation. */
void generateBox(const sibr::Vector3f& minis, const sibr::Vector3f& maxis, std::vector<sibr::Vector3f>& lines) {
	Eigen::AlignedBox3f newBox(minis); newBox.extend(maxis);
	lines = {
		newBox.corner(Eigen::AlignedBox3f::BottomLeftFloor), newBox.corner(Eigen::AlignedBox3f::TopLeftFloor),
		newBox.corner(Eigen::AlignedBox3f::TopLeftFloor), newBox.corner(Eigen::AlignedBox3f::TopRightFloor),
		newBox.corner(Eigen::AlignedBox3f::TopRightFloor), newBox.corner(Eigen::AlignedBox3f::BottomRightFloor),
		newBox.corner(Eigen::AlignedBox3f::BottomRightFloor), newBox.corner(Eigen::AlignedBox3f::BottomLeftFloor),

		newBox.corner(Eigen::AlignedBox3f::BottomLeftCeil), newBox.corner(Eigen::AlignedBox3f::TopLeftCeil),
		newBox.corner(Eigen::AlignedBox3f::TopLeftCeil), newBox.corner(Eigen::AlignedBox3f::TopRightCeil),
		newBox.corner(Eigen::AlignedBox3f::TopRightCeil), newBox.corner(Eigen::AlignedBox3f::BottomRightCeil),
		newBox.corner(Eigen::AlignedBox3f::BottomRightCeil), newBox.corner(Eigen::AlignedBox3f::BottomLeftCeil),

		newBox.corner(Eigen::AlignedBox3f::BottomLeftFloor), newBox.corner(Eigen::AlignedBox3f::BottomLeftCeil),
		newBox.corner(Eigen::AlignedBox3f::BottomRightFloor), newBox.corner(Eigen::AlignedBox3f::BottomRightCeil),
		newBox.corner(Eigen::AlignedBox3f::TopLeftFloor), newBox.corner(Eigen::AlignedBox3f::TopLeftCeil),
		newBox.corner(Eigen::AlignedBox3f::TopRightFloor), newBox.corner(Eigen::AlignedBox3f::TopRightCeil),
	};
}

int main(int ac, char** av) {
	CommandLineArgs::parseMainArgs(ac, av);
	ProbesPlacementAppArgs args;

	Window window("Probes placement");
	window.makeContextCurrent();

	// Load mesh.
	Mesh::Ptr mesh(new Mesh(true));
	bool success = false;
	if(sibr::getExtension(args.mesh) == "xml") {
		success = mesh->loadMtsXML(args.mesh);
	} else {
		success = mesh->load(args.mesh);
	}
	if(!success) {
		SIBR_ERR << "Unable to load mesh at path: " << args.mesh << "." << std::endl;
		return EXIT_FAILURE;
	}

	// Setup viewer and add mesh.
	MultiViewManager viewManager(window);
	const MultiMeshManager::Ptr viewer(new MultiMeshManager());
	viewManager.addSubView("Probes placement", viewer);
	viewer->addMesh("Scene", mesh).setColor({ 0.5f, 0.5f, 0.5f });

	// Grid settings.
	const Eigen::AlignedBox3f bbox = mesh->getBoundingBox();
	sibr::Vector3f minis = bbox.center() - 0.4f * bbox.diagonal();
	sibr::Vector3f maxis = bbox.center() + 0.4f * bbox.diagonal();
	sibr::Vector3i steps = { 3, 4, 5 };

	bool updateBox = true;
	bool updatePoints = true;
	std::vector<sibr::Vector3f> probes;
	
	while(window.isOpened()) {
		Input::poll();

		if(Input::global().key().isReleased(Key::Escape)) {
			window.close();
		}

		window.bind();
		window.viewport().bind();

		glClear(GL_COLOR_BUFFER_BIT);

		// Our options window.
		if (ImGui::Begin("Probes placement options")) {
			updateBox = ImGui::DragFloat3("Min.", &minis[0]) || updateBox;
			updateBox = ImGui::DragFloat3("Max.", &maxis[0]) || updateBox;
			
			if (ImGui::DragInt3("Size", &steps[0])){
				steps = steps.cwiseMax(sibr::Vector3i(0, 0, 0));
				updatePoints = true;
			}
			ImGui::Text("Count: %d probes", (steps[0] * steps[1] * steps[2]));

			if(ImGui::Button("Save probes...")) {
				std::string outputPath;
				if(sibr::showFilePicker(outputPath, FilePickerMode::Save, "", "positions") && !outputPath.empty()) {
					saveProbes(probes, outputPath);
				}
			}

			if (ImGui::Button("Load probes...")) {
				std::string inputPath;
				if (sibr::showFilePicker(inputPath, FilePickerMode::Default, "", "positions") && !inputPath.empty()) {
					loadProbes(inputPath, steps, minis, maxis);
					updateBox = true;
				}
			}

			if(updateBox) {
				const sibr::Vector3f minBack = minis;
				minis = minis.cwiseMin(maxis);
				maxis = maxis.cwiseMax(minBack);
				updatePoints = true;
			}
		}
		ImGui::End();

		// Update the box and probes visualisation.
		if(updateBox) {
			std::vector<sibr::Vector3f> endpoints;
			generateBox(minis, maxis, endpoints);
			viewer->addLines("Region of interest", endpoints, sibr::Vector3f(1.0f, 0.0f, 0.0f));
		}
		if(updatePoints) {
			generateGrid(steps, minis, maxis, probes);
			viewer->addPoints("Probes", probes, sibr::Vector3f(0.0f, 1.0f, 0.2f));
		}
		
		viewManager.onUpdate(sibr::Input::global());
		viewManager.onRender(window);
		
		window.swapBuffer();
	}


	return EXIT_SUCCESS;
	
}
