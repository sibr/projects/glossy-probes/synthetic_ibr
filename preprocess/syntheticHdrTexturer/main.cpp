/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "core/system/CommandLineArgs.hpp"
#include "core/assets/InputCamera.hpp"
#include "core/graphics/Image.hpp"
#include "core/graphics/Mesh.hpp"
#include "core/imgproc/MeshTexturing.hpp"
#include "projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp"
using namespace sibr;


struct TexturingAppArgs {
	Arg<std::string> outputPath = { "output","", "output texture path" };
	Arg<std::string> imagePath = { "path","", "path to RGB EXR texture" };
	Arg<float> exposure = { "exposure", 3.0f, "exposure value" };
};

/** Pack lightmap diffuse and material info. */
int main(int ac, char** av) {

	// Parse Command-line Args
	CommandLineArgs::parseMainArgs(ac, av);

	TexturingAppArgs args;

	SIBR_LOG << "[Texturing] Loading data..." << std::endl;

	// Load roughness+alpha.
	ImageRGBA smoothImg;
	const std::string smoothnessPath = sibr::removeExtension(args.imagePath) + "_mask.png";
	if(!smoothImg.load(smoothnessPath, false, true)) {
		return EXIT_FAILURE;
	}
	// We need a L8 mask.
	ImageL8 maskImg(smoothImg.w(), smoothImg.h());
#pragma omp parallel for
	for(int y = 0; y < int(smoothImg.h()); ++y) {
		for (int x = 0; x < int(smoothImg.w()); ++x) {
			const unsigned char & pix = smoothImg(x,y)[3];
			maskImg(x,y)[0] = pix >= 1 ? 255 : 0;
		}
	}
	
	// If we have the color image, flood fill it and save it.
	if(sibr::fileExists(args.imagePath)) {
		ImageRGB32F colorImg;
		colorImg.load(args.imagePath, false, true);
#pragma omp parallel for
		for (int y = 0; y < int(smoothImg.h()); ++y) {
			for (int x = 0; x < int(smoothImg.w()); ++x) {
				const float alph = float(smoothImg(x, y)[3])/255.0f;
				if(alph < 0.0001f) {
					continue;
				}
				colorImg(x, y)[0] /= alph;
				colorImg(x, y)[1] /= alph;
				colorImg(x, y)[2] /= alph;
			}
		}

		// Flood fill.
		auto dstImg = MeshTexturing::floodFill(colorImg, maskImg);
		// Save basic verison.
		ImageRGB ldrImg;
		syntheticIBR::convert(*dstImg, ldrImg);
		ldrImg.save(args.outputPath);
		// For Falcor.
		ImageRGB ldrImgGamma;
		syntheticIBR::gamma(*dstImg, ldrImgGamma, 1.0f / 2.2f);
		ldrImgGamma.save(sibr::removeExtension(args.outputPath) + "_gamma.png");
	}
	
	// If we also have the fresnel, we need to pack it with the smoothness parameter.
	const std::string fresnelPath = sibr::removeExtension(args.imagePath) + "_fresnel.png";
	if(sibr::fileExists(fresnelPath)) {
		ImageRGB fresnelImg;
		fresnelImg.load(fresnelPath, false, true);

		ImageRGBA matMap(fresnelImg.w(), fresnelImg.h());
		// Put smoothness in the alpha channel.
#pragma omp parallel for
		for(int y = 0; y < int(fresnelImg.h()); ++y) {
			for (int x = 0; x < int(fresnelImg.w()); ++x) {
				const sibr::Vector3ub & fresn = fresnelImg(x,y);
				matMap(x, y)[0] = fresn[0];
				matMap(x, y)[1] = fresn[1]; 
				matMap(x, y)[2] = fresn[2]; 
				matMap(x, y)[3] = smoothImg(x, y)[0]; 
			}
		}
		auto mapImg = MeshTexturing::floodFill(matMap, maskImg);
		mapImg->save(sibr::removeExtension(args.outputPath) + "_mat.png");
	}

	// Also store the raw roughness at a high precision.
	const std::string roughPath = sibr::removeExtension(args.imagePath) + "_rough.exr";
	if (sibr::fileExists(roughPath)) {
		ImageRGB32F roughImg;
		roughImg.load(roughPath, false, true);
		auto mapImg = MeshTexturing::floodFill(roughImg, maskImg);
		mapImg->saveHDR(sibr::removeExtension(args.outputPath) + "_rough_final.exr");
	}

	return EXIT_SUCCESS;

}