/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/graphics/Window.hpp>
#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/Input.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/system/String.hpp"
#include "core/system/XMLTree.h"
#include "core/system/Transform3.hpp"
#include "projects/synthetic_ibr/renderer/MeshOBJExporter.hpp"
#include "projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp"
#include <core\imgproc\PoissonReconstruction.hpp>
#include <core\assets\Config.hpp>
#include <array>

#define PROGRAM_NAME "synthetic_ibr_paths_placement"

const char* usage = "Usage: " PROGRAM_NAME " -path directory\n";


using namespace sibr;

struct ConvertMitsubaEXRArgs {
	RequiredArg<std::string> path = { "path" };
	RequiredArg<std::string> output = { "output" };
	Arg<int> prefLength = { "pref", 0, "length of the prefix to remove" };
	Arg<bool> lazy = { "lazy", "only generate if the destination doesn't exist." };
};

/** Tonemap and rename HDR Mitsuba renderings. */
int main(int ac, char** av) {
	sibr::CommandLineArgs::parseMainArgs(ac, av);
	ConvertMitsubaEXRArgs args;

	if (!sibr::directoryExists(args.output)) {
		sibr::makeDirectory(args.output);
	}
	//Gamma correct and LDR images in directory
	const auto files = sibr::listFiles(args.path, false, false, { "exr" });

	for (size_t iid = 0; iid < files.size(); ++iid) {
		const auto& file = files[iid];
		const std::string outputName = sibr::removeExtension(file).substr(args.prefLength.get());
		const std::string outputPath = args.output.get() + "/" + outputName + ".png";
		if(args.lazy && sibr::fileExists(outputPath)) {
			continue;
		}
		ImageRGB32F srcImg;
		srcImg.load(args.path.get() + "/" + file, true);
		ImageRGB dstImg;
		syntheticIBR::gamma(srcImg, dstImg, 1.0f / 2.2f);
		dstImg.save(outputPath, false);
	}
	return EXIT_SUCCESS;
}
