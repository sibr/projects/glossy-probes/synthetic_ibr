/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include "projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp"
#include "projects/synthetic_ibr/renderer/MeshOBJExporter.hpp"

#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/Image.hpp"
#include "core/raycaster/Raycaster.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/assets/Config.hpp"

using namespace sibr;


struct MapGenerationArgs {
	RequiredArg<std::string> path = { "path", "dataset root path" };
	RequiredArg<std::string> name = { "name", "scene name" };
	Arg<int> width = { "res", 2048, "output width" };
	Arg<bool> nobin = {"nobin", "save images data instead of bin"};
};


class ImageMatId {
public:
	ImageMatId(uint w, uint h) {
		tex8 = ImageRGB(w,h);
		texUV = ImageUV8(w, h);
	}

	void operator()(uint x, uint y, int matId)  {
		tex8(x, y)[0] = ((matId) & 0xff);
		tex8(x, y)[1] = ((matId >> 8) & 0xff);
		tex8(x, y)[2] = 0;
		texUV(x, y)[0] = tex8(x, y)[0];
		texUV(x, y)[1] = tex8(x, y)[1];
	}

	ImageRGB tex8;
	ImageUV8 texUV;

};


/** Generate geoemtric maps for a set of probes, using their adaptive parameterization. */
int main(int ac, char** av) {
	
	CommandLineArgs::parseMainArgs(ac, av);
	
	MapGenerationArgs args;
	const std::string basePath = args.path;
	const std::string sceneName = args.name;

	const std::string probesPath = basePath + "/probes/" + sceneName + ".positions";
	const std::string outputPath = basePath + "/flows/" + sceneName + "/";
	std::string warpsPath = basePath + "/warps/" + sceneName + "/inverse_flow/";
	const std::string lightmapPath = basePath + "/lightmap/";

	sibr::makeDirectory(outputPath);
	sibr::makeDirectory(outputPath + "/reflector/");
	sibr::makeDirectory(outputPath + "/reflected/");
	sibr::makeDirectory(outputPath + "/matid/");
	sibr::makeDirectory(outputPath + "/reflectedmatid/");

	const sibr::Vector2i resolution(args.width, args.width/2);
	const int w = resolution.x();
	const int h = resolution.y();

	// Mesh loading (we need the object IDs).
	MaterialMesh::Ptr msh(new MaterialMesh(false));
	const std::string meshPath = basePath + "/scene/scene_" + sceneName + ".xml";
	if (sibr::fileExists(meshPath)) {
		msh->loadMtsXML(meshPath);
	}
	else {
		msh->loadMtsXML(basePath + "/scene/scene.xml");
	}
	Raycaster raycaster;
	raycaster.addMesh(*msh);

	// Load the probes.
	ProbeLocations probes;
	probes.setupFromPath(probesPath);

	SIBR_LOG << "Loaded " << probes.locations().size() << " positions." << std::endl;
	const float sceneScale = syntheticIBR::loadScale(basePath);

	// Load the parameterizations.
	SIBR_LOG << "Loading parametrizations..." << std::endl;
	
	const auto warpFiles = sibr::listFiles(warpsPath, false, false, { "exr" });
	std::vector<sibr::ImageFloat2> warpMaps(probes.locations().size());
#pragma omp parallel for
	for (int wid = 0; wid < warpMaps.size(); ++wid) {
		const auto & warpFile = warpFiles[wid];
		// Extract the cam number.
		const std::string::size_type beg = warpFile.find_first_of("0123456789");
		const std::string::size_type end = warpFile.find_last_of("0123456789");
		const std::string idStr = warpFile.substr(beg, end - beg + 1);
		const int id = std::stoi(idStr);
		assert(id < warpMaps.size());
		warpMaps[id].load(warpsPath + "/" + warpFile, false, true);
	}
	std::cout << "Done." << std::endl;

	// Compute maps using the raycaster.
	SIBR_LOG << "Generating reflection maps." << std::endl;
	
	for (int cid = 0; cid < int(probes.locations().size()); ++cid) {
		const sibr::Vector3f & loc = probes.locations()[cid];
		const std::string dstName = "Cam" + sibr::intToString<3>(cid);
		SIBR_LOG << "Processing " << dstName << std::endl;

		ImageRGB32F reflected(w,h);
		ImageRGB32F reflector(w, h);
		ImageMatId reflectorId(w,h);
		ImageMatId reflectedId(w, h);

#pragma omp parallel for
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				const float sx = float(x) + 0.5f;
				const float sy = float(y) + 0.5f;
				// Create a ray and cast it.
				const sibr::Vector3f dir = syntheticIBR::pixelToDirWarped({ sx, sy }, resolution, warpMaps[cid]).normalized();
				const RayHit its = raycaster.intersect(Ray(loc, dir), 0.0f);
				if (!its.hitSomething()) {
					// No intersection with the scene.
					continue;
				}
				// Compute position and normal at the intersection.
				const sibr::Vector3f itsPos = loc + its.dist() * dir;
				const sibr::Vector3f itsNor = Raycaster::smoothNormal(*msh, its);

				// Get the ID of the material. Reserve index 0 for empty regions.
				const int matId = msh->matIds()[its.primitive().triID] + 1;

				// Store both values.
				reflector(x, y) = itsPos;
				reflectorId(x, y, matId);
				
				// Compute direction of the reflected ray, cast in the scene.
				const sibr::Vector3f newDir = (dir - 2.0f * itsNor.dot(dir) * itsNor).normalized();
				const RayHit reflIts = raycaster.intersect(Ray(itsPos, newDir), sceneScale*0.001f);
				if (!reflIts.hitSomething()) {
					// No intersection with the scene.
					continue;
				}

				// We hit the scene again, on the reflected geometry.
				const sibr::Vector3f reflPos = itsPos + reflIts.dist() * newDir;
				const int reflId = msh->matIds()[reflIts.primitive().triID] + 1;
				reflected(x, y) = reflPos;
				reflectedId(x, y, reflId);
			}
		}

		if (!args.nobin) {
			// Save images directly as binary blobs.
			syntheticIBR::saveImageBinary(reflector, outputPath + "/reflector/" + dstName + ".bin");
			syntheticIBR::saveImageBinary(reflected, outputPath + "/reflected/" + dstName + ".bin");
			syntheticIBR::saveImageBinary(reflectorId.texUV, outputPath + "/matid/" + dstName + ".bin");
			syntheticIBR::saveImageBinary(reflectedId.texUV, outputPath + "/reflectedmatid/" + dstName + ".bin");
			
		} else {
			// Else we have to save them as HDR or packed images.
			reflector.saveHDR(outputPath + "/reflector/" + dstName + ".exr", false);
			reflected.saveHDR(outputPath + "/reflected/" + dstName + ".exr", false);
			reflectorId.tex8.save(outputPath + "/matid/" + dstName + ".png", false);
			reflectedId.tex8.save(outputPath + "/reflectedmatid/" + dstName + ".png", false);
		}

	}

	// We also want to generate intersection maps using the lightmap mesh.
	SIBR_LOG << "Generating intersection maps." << std::endl;
	sibr::makeDirectory(outputPath + "/intersection/");
	sibr::makeDirectory(outputPath + "/roughness/");

	// This time we need to work on the mesh used at render time so that indices are the same.
	Mesh::Ptr mshPack(new Mesh(false));
	const std::string mshPackPath = basePath + "/lightmap/scene_light_map.obj";
	MeshOBJHandler::loadOBJ(mshPackPath, *mshPack);
	Raycaster raycasterPack;
	raycasterPack.addMesh(*mshPack);

	for (int cid = 0; cid < int(probes.locations().size()); ++cid) {
		const sibr::Vector3f & loc = probes.locations()[cid];
		const std::string dstName = "Cam" + sibr::intToString<3>(cid);
		SIBR_LOG << "Processing " << dstName << std::endl;

		ImageInt3 hitMap(w, h, {-1, 0, 0});
#pragma omp parallel for
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				const float sx = float(x) + 0.5f;
				const float sy = float(y) + 0.5f;
				// Create a ray and cast it.
				const sibr::Vector3f dir = syntheticIBR::pixelToDirWarped({ sx, sy }, resolution, warpMaps[cid]).normalized();
				const RayHit its = raycasterPack.intersect(Ray(loc, dir), 0.0f);
				if (!its.hitSomething()) {
					// No intersection with the scene.
					continue;
				}
				// Preserve the uint representation.
				const int id = int(its.primitive().triID);
				// Store barycentric coordinates in the same map, reinterpreting them as integers.
				const sibr::Vector2f bars(its.barycentricCoord().u, its.barycentricCoord().v);
				int uv0, uv1;
				std::memcpy(&uv0, &bars[0], sizeof(float));
				std::memcpy(&uv1, &bars[1], sizeof(float));
				hitMap(x, y) = { id, uv0, uv1 };
			}
		}
		syntheticIBR::saveImageBinary(hitMap, outputPath + "/intersection/" + dstName + ".bin");

	}
	return EXIT_SUCCESS;
	
}
