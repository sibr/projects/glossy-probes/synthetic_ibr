/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SimpleLineView.hpp"

namespace sibr {

	//===========================================================
	
	SimpleLineView::SimpleLineView(const std::string & _name) : name(_name)
	{
		initShaders();

		InputCamera cam;
		cam.setLookAt(Vector3f(0.5f, 0.5f, 1.25), Vector3f(0.5f, 0.5f, 0), Vector3f(0,1,0));

		

		camera_handler.switchMode(InteractiveCameraHandler::InteractionMode::FPS);
		camera_handler.setup(cam, Viewport(0, 0, 1, 1), camera_handler.getRaycaster());
	}

	//===========================================================

	void SimpleLineView::onUpdate(Input & input, const Viewport & vp)
	{
		if (!camera_handler.isSetup() && list_meshes.size() > 0) {
			for (const auto & mesh_data : list_meshes) {
				if (mesh_data.raycaster && mesh_data.meshPtr) {
					auto bbox = mesh_data.meshPtr->getBoundingBox();
					if (bbox.volume() > 0) {
						camera_handler.getRaycaster() = mesh_data.raycaster;
						TrackBall tb;
						tb.fromBoundingBox(bbox, vp);
						camera_handler.fromCamera(tb.getCamera());
						break;
					}
				}
			}
		}

		if (camera_handler.isSetup() && !camera_handler.getRaycaster()) {
			for (const auto & mesh : list_meshes) {
				if (mesh.raycaster) {
					camera_handler.getRaycaster() = mesh.raycaster;
					break;
				}
			}
		}

		if (selected_mesh_it_is_valid) {
			const auto & mesh = *selected_mesh_it;		
			if( mesh.raycaster) {
				camera_handler.getRaycaster() = mesh.raycaster;
			}
		}

		camera_handler.update(input, 1 / 60.0f, vp);
	}

	//===========================================================

	void SimpleLineView::onRender(const Viewport & viewport)
	{
		viewport.clear(backgroundColor);
		viewport.bind();
		
		renderMeshes();

		camera_handler.onRender(viewport);
	}

	//===========================================================

	void SimpleLineView::onRender(IRenderTarget & dst)
	{
		dst.bind();
		Viewport vp(0.0f, 0.0f, (float)dst.w(), (float)dst.h());
		onRender(vp);
		dst.unbind();
	}

	//===========================================================

	void SimpleLineView::initShaders()
	{
		const std::string folder = sibr::getShadersDirectory("core") + "/";
		colored_mesh_shader.initShader("colored_mesh_shader",
			loadFile(folder + "alpha_colored_mesh.vert"), 
			loadFile(folder + "alpha_colored_mesh.frag"));
	}

	//===========================================================

	void SimpleLineView::renderMeshes()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendEquation(GL_FUNC_ADD);

		for (const auto & mesh_data : list_meshes) {
			if (!mesh_data.active) {
				continue;
			}
			colored_mesh_shader.render(camera_handler.getCamera(), mesh_data);
		}

		glDisable(GL_BLEND);
	}

	//===========================================================

	MeshData & SimpleLineView::addMeshAsLines(const std::string & name, Mesh::Ptr mesh)
	{
		MeshData data(name, mesh, MeshData::LINES, Mesh::LineRenderMode);
		return addMeshData(data).setColor(meshColor).setDepthTest(false);
	}

	//===========================================================

	MeshData & SimpleLineView::addMeshData(MeshData & data, bool create_raycaster)
	{
		Raycaster::Ptr raycaster;
		if (create_raycaster) {
			raycaster = std::make_shared<Raycaster>();
			raycaster->init();
			raycaster->addMesh(*data.meshPtr);
		}
		data.raycaster = raycaster;

		list_meshes.push_back(data);


		auto box = data.meshPtr->getBoundingBox();
		if (!box.isEmpty()) {
			InputCamera cam = camera_handler.getCamera();
			cam.zfar(std::max(cam.zfar(), 5.0f*box.diagonal().norm()));
			camera_handler.fromCamera(cam);
		}

		return list_meshes.back();
	}

}