/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include <core/graphics/Mesh.hpp>
#include <core/graphics/Shader.hpp>
#include <core/view/InteractiveCameraHandler.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/view/MultiMeshManager.hpp>

#include <list>

namespace sibr {

	class SimpleLineView : public ViewBase {
		
		SIBR_CLASS_PTR(SimpleLineView);

	public:
	
		SimpleLineView(const std::string & _name = "SimpleLineView");
	
		MeshData & addMeshAsLines(const std::string & name, Mesh::Ptr mesh);
		
		// ViewBase interface
		void	onUpdate(Input& input, const Viewport & vp) override;
		void	onRender(const Viewport & viewport) override;
		void	onRender(IRenderTarget & dst);
		void	onGUI() override {}

	InteractiveCameraHandler & getCameraHandler() { return camera_handler; }
	MeshShadingShader & getMeshShadingShader() { return colored_mesh_shader; }

	protected:

		MeshData & addMeshData(MeshData & data, bool update_raycaster = false);
		void initShaders();
		void renderMeshes();

		using ListMesh = std::list<MeshData>;
		using Iterator = ListMesh::iterator;
	
		std::string							name;

		ListMesh							list_meshes;
		Iterator 							selected_mesh_it;
		bool								selected_mesh_it_is_valid = false;
		
		InteractiveCameraHandler			camera_handler;

		MeshShadingShader					colored_mesh_shader;

		Vector3f							backgroundColor = { 0.1f, 0.1f, 0.1f };
		Vector3f							meshColor = { 1.0f, 1.0f, 1.0f };
	};

}