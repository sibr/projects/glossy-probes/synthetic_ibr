/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "core/system/CommandLineArgs.hpp"
#include <core/graphics/Window.hpp>
#include "QHarmonicSolver.hpp"


#define PROGRAM_NAME "sibr_synthetic_ibr_qharmonic"

//==============================================================

void outputFlow(const string& outputPath, const Mesh::Vertices& verts, const Vector2u& gridRes)
{
	sibr::ImageRGB32F flow(gridRes.x(), gridRes.y(), Vector3f(0.f, 0.f, 0.f));
	for (unsigned int y = 0; y < gridRes.y(); y++)
	{
		for (unsigned int x = 0; x < gridRes.x(); x++)
		{
			flow(x, y) = verts.at(QHarmonicSolver::index2Dto1D(Vector2u(x, y), gridRes));
		}
	}
	cv::Mat3f f = flow.toOpenCVBGR();
	cv::imwrite(outputPath, f);
}

//==============================================================

// area of a triangle given its vertices
float triArea(const Vector2f& a, const Vector2f& b, const Vector2f& c)
{
	float factors = a.x() * (b.y() - c.y()) + b.x() * (c.y() - a.y()) + c.x() * (a.y() - b.y());
	return 0.5f * abs(factors);
}

//==============================================================

void outputEffectiveMagnification(const string& outputPath, Mesh::Ptr mesh, const ImageL32F& magMap)
{
	Vector2u mapRes = magMap.size();
	ImageRGB32F effectiveMag(mapRes.x(), mapRes.y(), Vector3f(0.f, 0.f, 0.f));
	const Mesh::Vertices& verts = mesh->vertices();
	const float inversePxArea = float(mapRes.prod());
	for (unsigned int y = 0; y < mapRes.y(); y++)
	{
		for (unsigned int x = 0; x < mapRes.x(); x++)
		{
			const unsigned int triIndex1 = 2 * QHarmonicSolver::index2Dto1D(Vector2u(x, y), mapRes);
			const unsigned int triIndex2 = triIndex1 + 1;
			const Vector3u tris1 = mesh->triangles().at(triIndex1);
			const Vector3u tris2 = mesh->triangles().at(triIndex2);
			const float area1 = triArea(verts.at(tris1.x()).xy(), verts.at(tris1.y()).xy(), verts.at(tris1.z()).xy());
			const float area2 = triArea(verts.at(tris2.x()).xy(), verts.at(tris2.y()).xy(), verts.at(tris2.z()).xy());
			const float combinedArea =  inversePxArea * (area1 + area2);
			effectiveMag(x, y) = Vector3f(combinedArea, magMap(x, y).x(), 0.f);
		}
	}
	cv::Mat3f m = effectiveMag.toOpenCVBGR();
	cv::flip(m, m, 0);
	cv::imwrite(outputPath, m);
}

//==============================================================

// convolve normal map with DCT patterns to detect geometric complexity
// this function uses low-level OGL, which should probably be replaced by corresponding SIBR wrappers
ImageL32F normalToComplexity(const ImageRGB32F& normalMap, GLuint framebuffer, const int dctRes = 16, const float weightExponent = 8.f)
{
	// upload normal map
	GLuint inputTex;
	glGenTextures(1, &inputTex);
	glBindTexture(GL_TEXTURE_2D, inputTex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, normalMap.w(), normalMap.h(), 0, GL_RGB, GL_FLOAT, normalMap.data());
	CHECK_GL_ERROR;

	// set up framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	GLuint complexityTex;
	glGenTextures(1, &complexityTex);
	glBindTexture(GL_TEXTURE_2D, complexityTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, normalMap.w(), normalMap.h(), 0, GL_RED, GL_FLOAT, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, complexityTex, 0);
	CHECK_GL_ERROR;

	// create shader
	const string folder = sibr::getShadersDirectory("synthetic_ibr") + "/";
	const std::string vertexShader = loadFile(folder + "dct_convolution.vert");
	const std::string  fragmentShader = loadFile(folder + "dct_convolution.frag");
	const char* vstr = vertexShader.c_str();
	const char* fstr = fragmentShader.c_str();
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vstr, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fstr, NULL);
	glCompileShader(fs);
	GLuint program = glCreateProgram();
	glAttachShader(program, fs);
	glAttachShader(program, vs);
	glLinkProgram(program);
	glUseProgram(program);
	glBindTexture(GL_TEXTURE_2D, inputTex);
	glUniform1i(glGetUniformLocation(program, "inputTexture2D"), 0);
	glProgramUniform1i(program, glGetUniformLocation(program, "dctResolution"), dctRes);
	glProgramUniform1f(program, glGetUniformLocation(program, "weightExponent"), weightExponent);
	CHECK_GL_ERROR;

	// perform convolutions
	glViewport(0, 0, normalMap.w(), normalMap.h());
	glClear(GL_COLOR_BUFFER_BIT);
	RenderUtility::renderScreenQuad();
	CHECK_GL_ERROR;

	// download result
	ImageL32F complexity(normalMap.w(), normalMap.h());
	glBindTexture(GL_TEXTURE_2D, complexityTex);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, complexity.data());
	glDeleteTextures(1, &complexityTex);
	glDeleteTextures(1, &inputTex);
	CHECK_GL_ERROR;

	return complexity;
}

//==============================================================

// reference: Sec. 4.2.1
ImageL32F applyMagnificationRules(
	ImageRGB32F& magInfoMap, 
	ImageRGB32F& normalMap, 
	const int kernelSize, 
	const float smoothnessGamma, 
	const float complexityWeight,
	const GLuint framebuffer)
{
	magInfoMap.flipH();
	cv::Mat3f magInfoMapCV = magInfoMap.toOpenCV();
	cv::Mat specularity;
	cv::Mat depth;
	cv::Mat facing;
	cv::extractChannel(magInfoMapCV, specularity, 0);
	cv::extractChannel(magInfoMapCV, depth, 1);
	cv::extractChannel(magInfoMapCV, facing, 2);

	double minVal;
	double maxVal;
	cv::minMaxLoc(depth, &minVal, &maxVal);
	depth /= maxVal + 10e-6;

	// get rid of isolated slanted pixels which arise from undersampling
	cv::dilate(facing, facing, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));

	// prepare epsilon for non-occupied space
	cv::Mat emptySpace = 1.f * specularity;
	cv::threshold(emptySpace, emptySpace, 0, 1.f, cv::THRESH_BINARY);
	cv::Mat specularMask = 1.f * emptySpace;
	emptySpace = (1.f - emptySpace) * QHarmonicSolver::EPS;

	// latitude cosine
	ImageL32F latCosImg(magInfoMap.size().x(), magInfoMap.size().y());
	for (unsigned int y = 0; y < latCosImg.size().y(); y++)
	{
		for (unsigned int x = 0; x < latCosImg.size().x(); x++)
		{
			const float coordY = 2.f * y / latCosImg.size().y() - 1.f;
			latCosImg(x, y).x() = cos(0.5f * float(M_PI) * coordY);
		}
	}
	cv::Mat latCos = latCosImg.toOpenCV();
		
	// gamma correction of specularity
	cv::pow(specularity, smoothnessGamma, specularity);

	// apply magnification rules
	cv::Mat magMap = specularity;
	cv::pow(depth, 2., depth);
	cv::multiply(depth, specularity, magMap);
	cv::multiply(magMap, latCos, magMap);
	cv::divide(magMap, facing + 10e-4f, magMap);

	// consider geometric complexity
	normalMap.flipH();
	ImageL32F complexityImg = normalToComplexity(normalMap, framebuffer);
	cv::Mat complexity = complexityImg.toOpenCV();
	cv::minMaxLoc(complexity, &minVal, &maxVal);
	complexity /= maxVal + 10e-6;
	cv::multiply(complexity, specularMask, complexity);
	
	// combine two sources:
	// 1. normalize mean of both (masked by specularity)
	// 2. add together
	cv::Mat mask;
	mask.convertTo(specularMask, CV_8UC1);
	double mean1 = cv::mean(magMap, mask)[0];
	double mean2 = cv::mean(complexity, mask)[0];
	double factor = mean1 / mean2 * complexityWeight;
	cv::add(magMap, complexity * factor, magMap);
	
	// add empty space filler epsilon
	magMap += emptySpace;

	// regularization: toroidal blur
	float sigma = kernelSize / 6.f;
	int halfKernelSize = int((kernelSize - 1) / 2.f);
	cv::Size magMapSize = magMap.size();
	cv::copyMakeBorder(magMap, magMap, 0, 0, halfKernelSize, halfKernelSize, cv::BORDER_WRAP);
	cv::GaussianBlur(magMap, magMap, cv::Size(kernelSize, kernelSize), sigma, sigma, cv::BORDER_DEFAULT);
	cv::Rect cropWindow(halfKernelSize, 0, magMapSize.width, magMapSize.height);
	magMap = magMap(cropWindow);

	ImageL32F ret;
	ret.fromOpenCV(magMap);

	return ret;
}

//==============================================================

// render a wireframe mesh
ImageRGB32F renderMeshVisualization(Mesh::Ptr mesh, const Vector2u& vizRes, GLuint framebuffer)
{
	// set up framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	GLuint vizTexture;
	glGenTextures(1, &vizTexture);
	glBindTexture(GL_TEXTURE_2D, vizTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, vizRes.x(), vizRes.y(), 0, GL_RGB, GL_FLOAT, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, vizTexture, 0);
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		SIBR_LOG << "Error while setting up framebuffer." << endl;

	// set up bufers
	GLuint vao, vbo, ebo;

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(
		GL_ARRAY_BUFFER, 
		mesh->vertices().size() * sizeof(Vector3f), 
		&mesh->vertices()[0], 
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER, 
		3 * mesh->triangles().size() * sizeof(unsigned int), 
		&mesh->triangles()[0], 
		GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f), (void*)0);
	glBindVertexArray(0);

	// create shader
	const string folder = sibr::getShadersDirectory("synthetic_ibr") + "/";
	const std::string vertexShader = loadFile(folder + "mesh_viz.vert");
	const std::string fragmentShader = loadFile(folder + "mesh_viz.frag");
	const char * vstr = vertexShader.c_str();
	const char * fstr = fragmentShader.c_str();		
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vstr, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fstr, NULL);
	glCompileShader(fs);
	GLuint program = glCreateProgram();
	glAttachShader(program, fs);
	glAttachShader(program, vs);
	glLinkProgram(program);

	// render
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glViewport(0, 0, vizRes.x(), vizRes.y());
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(program);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, GLsizei(mesh->triangles().size()) * 3, GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// download result
	ImageRGB32F vizImg(vizRes.x(), vizRes.y());
	glBindTexture(GL_TEXTURE_2D, vizTexture);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, vizImg.data());
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	CHECK_GL_ERROR;
	return vizImg;
}

//==============================================================

struct QHarmonicSolverArgs {
	RequiredArg<string> path = { "path", "path to the magnification map" };		
	RequiredArg<int> inv_res = { "inv_res", "inverse flow resolution"}; // (forward flow resolution is determined by resolution of magmap)
	RequiredArg<int> iterations = { "iterations", "number of solver iterations" };	
	RequiredArg<int> kernelSize = { "kernel_size", "size of blur kernel" };
	Arg<int> outputForward = { "output_forward", 0, "save forward flow" };
	Arg<float> complexityWeight = { "complexity_weight", 1, "weight of geometric complexity term" };
	Arg<int> outputViz = { "output_viz", 0, "save visualization" };
	Arg<int> outputMag = { "output_mag", 0, "save effective magnification" };
	Arg<int> viz_res = { "viz_res", 2000, "size of the mesh visualization" };
	Arg<float> gamma = { "gamma", 15.f, "gamma modification of material smoothness" };
};

//==============================================================

/** Compute adaptive parameterization from input geoemtric and material information. */
int main(int ac, char** av)
{	
	CommandLineArgs::parseMainArgs(ac, av);
	QHarmonicSolverArgs args;

	const string basePath = args.path;
	const int iterations = args.iterations;
	const int horizontalInvRes = args.inv_res;
	const Vector2u invRes(horizontalInvRes, horizontalInvRes / 2);
	const int horizontalVizRes = args.viz_res;
	const Vector2u vizRes(horizontalVizRes, horizontalVizRes / 2);
	const bool outputForward = args.outputForward == 1;
	const bool outputViz = args.outputViz == 1;
	const bool outputMag = args.outputMag == 1;
	const int kernelSize = args.kernelSize;
	const float smoothnessGamma = args.gamma;
	const float complexityWeight = args.complexityWeight;

	if (kernelSize % 2 != 1 || kernelSize < 0)
	{
		SIBR_WRG << "Kernel size must be a positive odd number" << std::endl;
		return 1;
	}

	const string magmapDir = basePath + "/magmap";
	const string normalDir = basePath + "/normal";

	if (!directoryExists(magmapDir) || !directoryExists(normalDir))
	{
		SIBR_WRG << "Path should contain directories: magmap, normal" << std::endl;
		return 1;
	}
	
	const auto magmap_files = listFiles(magmapDir, false, false, { "exr" });
	const auto normal_files = listFiles(normalDir, false, false, { "exr" });

	if (magmap_files.size() == 0 || magmap_files.size() != normal_files.size())
	{
		SIBR_WRG << "Directories magmap and normal cannot be empty and should contain equal number of files" << std::endl;
		return 1;
	}

	const string forwardFlowPath = basePath + "/forward_flow/";
	const string inverseFlowPath = basePath + "/inverse_flow/";
	const string outputVizPath = basePath + "/viz/";
	const string outputMagPath = basePath + "/magnification/";

	makeDirectory(inverseFlowPath);
	if (outputForward) makeDirectory(forwardFlowPath);
	if (outputViz) makeDirectory(outputVizPath);
	if (outputMag) makeDirectory(outputMagPath);

	float avg_time = 0.f;

	// create OGL context
	Window window(1, 1, PROGRAM_NAME);
	window.makeContextCurrent();

	GLuint framebuffer;
	glGenFramebuffers(1, &framebuffer);

	for (int i=0; i<magmap_files.size(); i++)
	{		
		const string magMapPath = magmapDir + "/" + magmap_files[i];
		const string normalPath = normalDir + "/" + normal_files[i];

		ImageRGB32F magInfoMap;
		ImageRGB32F normalMap;
		magInfoMap.load(magMapPath);
		normalMap.load(normalPath);

		ImageL32F magMap = applyMagnificationRules(magInfoMap, normalMap, kernelSize, smoothnessGamma, complexityWeight, framebuffer);
		Vector2u mapRes = magMap.size();
		Vector2u gridRes = mapRes + Vector2u::Ones();

		QHarmonicSolver solver;
		
		Mesh::Ptr origMesh = solver.createMesh(magMap.size());
		Mesh::Ptr modMesh = solver.createMesh(magMap.size());

		clock_t begin = clock();

		for (int i=0; i<iterations; i++)
			solver.qHarmonicRelaxation(modMesh, magMap);

		clock_t end = clock();
		avg_time += float(end - begin) / CLOCKS_PER_SEC * 1000.f;
		
		if (outputForward)
		{
			outputFlow(forwardFlowPath + "forward_" + magmap_files[i] + ".exr", modMesh->vertices(), gridRes);
		}

		if (outputMag)
		{
			outputEffectiveMagnification(outputMagPath + "magnification_" + magmap_files[i] + ".exr", modMesh, magMap);
		}

		// render mesh visualization
		if (outputViz)
		{
			ImageRGB32F vizImg = renderMeshVisualization(modMesh, vizRes, framebuffer);
			vizImg.flipH();
			vizImg.save(outputVizPath + "viz_" + magmap_files[i] + ".png", false);
		}

		// invert flow
		ImageRGB32F invFlow = solver.invertFlow(modMesh, invRes, framebuffer);
		invFlow.flipH();
		cv::Mat f = invFlow.toOpenCVBGR();
		cv::imwrite(inverseFlowPath + "inverse_" + magmap_files[i] + ".exr", f);
	}

	glDeleteFramebuffers(1, &framebuffer);

	avg_time /= magmap_files.size();

	SIBR_LOG << "solver took " << avg_time << " ms on average" << endl;
}