/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "core/graphics/Mesh.hpp"
#include <Eigen/Sparse>

using namespace std;
using namespace sibr;


class QHarmonicSolver
{

public:

	static const float EPS;

	// creates a simple polyplane
	Mesh::Ptr createMesh(const Vector2u& res);

	// perform quasi-harmonic relaxation
	void qHarmonicRelaxation(Mesh::Ptr mesh, const ImageL32F& magMap);

	// invert the flow field using rasterization
	ImageRGB32F invertFlow(Mesh::Ptr mesh, const Vector2u& gridRes, GLuint framebuffer);

	// convert 2D coordinate into 1D index
	static int index2Dto1D(const Vector2u& v, const Vector2u& res);

	// convert 1D index into 2D coordinate
	static Vector2u index1Dto2D(const unsigned int triId, const Vector2u quadRes);

private:

	// move boundary vertices proportional to boundary density
	void remapBoundaries(Mesh::Vertices& verts, const ImageL32F& magMap);

	// prepare right-hand side of the linear system
	Eigen::MatrixXf QHarmonicSolver::prepareRHS(
		const Mesh::Vertices& verts, 
		const Vector2u& res);

	// build system matrix
	Eigen::SparseMatrix<float> buildStiffnessMatrix(
		const Mesh::Vertices& verts,
		const Mesh::Triangles& tris,
		const ImageL32F& magMap);

	// stupid way of finding all inner mesh indices
	Eigen::VectorXi innerIndices(const Vector2u& res);

};
