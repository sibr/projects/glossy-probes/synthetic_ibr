/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "QHarmonicSolver.hpp"

//==============================================================

const float QHarmonicSolver::EPS = 2 * 10e-4f;

//==============================================================

int QHarmonicSolver::index2Dto1D(const Vector2u& v, const Vector2u& res)
{
	if (v.x() >= 0 && v.x() < res.x() && v.y() >= 0 && v.y() < res.y())
		return v.y() * res.x() + v.x();
	else
		return 0;
}

//==============================================================

// convert 1D index into 2D coordinate
Vector2u QHarmonicSolver::index1Dto2D(const unsigned int triId, const Vector2u quadRes)
{
	unsigned int index1D = triId / 2;
	return Vector2u(index1D % quadRes.x(), index1D / quadRes.x());
}

//==============================================================

Mesh::Ptr QHarmonicSolver::createMesh(const Vector2u& res)
{
	Mesh::Ptr mesh(new Mesh(true));

	Mesh::Vertices vertices = mesh->vertices();
	Mesh::Triangles tris = mesh->triangles();
	Mesh::UVs uvs = mesh->texCoords();

	const float dx = 1.0f / res.x();
	const float dy = 1.0f / res.y();

	for (unsigned int i = 0; i <= res.y(); i++)
	{
		float y = i * dy;
		for (unsigned int j = 0; j <= res.x(); j++)
		{
			float x = j * dx;

			// positions
			vertices.push_back(Vector3f(x, y, 0));

			// UVs
			uvs.push_back(Vector2f(x, y));

			// tris
			if (i == res.y() || j == res.x()) continue;
			unsigned int a = i * (res.x() + 1) + j;
			unsigned int b = a + 1;
			unsigned int d = a + (res.x() + 1);
			unsigned int c = d + 1;
			tris.push_back(Vector3u(a, c, b));
			tris.push_back(Vector3u(a, d, c));
		}
	}
	mesh->vertices(vertices);
	mesh->texCoords(uvs);
	mesh->triangles(tris);
	return mesh;
}

//==============================================================

void QHarmonicSolver::qHarmonicRelaxation(Mesh::Ptr mesh, const ImageL32F& magMap)
{
	const Vector2u& res = magMap.size();
	const Vector2u& gridRes = magMap.size() + Vector2u::Ones();
	Mesh::Vertices verts = mesh->vertices();
	Mesh::Triangles tris = mesh->triangles();
	int vertexCount = gridRes.prod();

	// step 1: build stiffness matrix
	Eigen::SparseMatrix<float> stiffnessMatrix = buildStiffnessMatrix(verts, tris, magMap);

	// step 2: set up boundary conditions
	remapBoundaries(verts, magMap);
	Eigen::MatrixXf uv = prepareRHS(verts, res);

	// step 3: exclude boundary vertices and build system
	const Eigen::VectorXi freeVertices = innerIndices(gridRes);
	unsigned int freeVertexCount = freeVertices.size();
	vector<Eigen::Triplet<float>> selectionTriplets;
	for (unsigned int i = 0; i < freeVertexCount; i++)
		selectionTriplets.push_back(Eigen::Triplet<float>(i, freeVertices(i), 1.0f));
	Eigen::SparseMatrix<float> selectionMatrix(freeVertexCount, vertexCount);
	selectionMatrix.setFromTriplets(selectionTriplets.begin(), selectionTriplets.end());

	const Eigen::SparseMatrix<float> systemMatrix = selectionMatrix * stiffnessMatrix * selectionMatrix.transpose();
	const Eigen::MatrixXf rhs = -selectionMatrix * stiffnessMatrix * uv;

	// step 4: solve system (Choleskey factorization)
	Eigen::SimplicialLDLT<Eigen::SparseMatrix<float>> sparseSolver(systemMatrix);
	const Eigen::MatrixXf x = sparseSolver.solve(rhs);

	// write out solution
	for (unsigned int i = 0; i < freeVertexCount; i++)
	{
		Vector2f& newVertex = x.row(i).xy();
		if (newVertex.x() > 0.f && newVertex.y() > 0.f && newVertex.x() < 1.f && newVertex.y() < 1.f)
		{
			verts.at(freeVertices(i)).x() = x.row(i).x();
			verts.at(freeVertices(i)).y() = x.row(i).y();
		}
	}

	mesh->vertices(verts);
}

//==============================================================

ImageRGB32F QHarmonicSolver::invertFlow(Mesh::Ptr mesh, const Vector2u& invRes, GLuint framebuffer)
{

	struct RenderVertex
	{
		Vector3f position;
		Vector2f texCoord;

		RenderVertex(const Vector3f& pos, const Vector2f& uv)
			: position(pos), texCoord(uv)
		{}
	};

	// set up framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	GLuint invFlowTexture;
	glGenTextures(1, &invFlowTexture);
	glBindTexture(GL_TEXTURE_2D, invFlowTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, invRes.x(), invRes.y(), 0, GL_RGB, GL_FLOAT, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, invFlowTexture, 0);
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		SIBR_LOG << "Error while setting up framebuffer." << endl;

	CHECK_GL_ERROR;

	// re-arrange data for single-buffer access
	vector<RenderVertex> v;
	for (int i = 0; i < mesh->vertices().size(); i++)
		v.emplace_back(mesh->vertices().at(i), mesh->texCoords().at(i));

	// set up bufers
	GLuint vao, vbo, ebo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(
		GL_ARRAY_BUFFER,
		v.size() * sizeof(RenderVertex),
		&v[0],
		GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		3 * mesh->triangles().size() * sizeof(unsigned int),
		&mesh->triangles()[0],
		GL_STATIC_DRAW);

	CHECK_GL_ERROR;

	// positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)0);
	// texture coords
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), (void*)offsetof(RenderVertex, texCoord));
	glBindVertexArray(0);

	CHECK_GL_ERROR;

	// create shader
	const string folder = sibr::getShadersDirectory("synthetic_ibr") + "/";
	const std::string vertexShader = loadFile(folder + "flow_inversion.vert");
	const std::string  fragmentShader = loadFile(folder + "flow_inversion.frag");
	const char * vstr = vertexShader.c_str();
	const char * fstr = fragmentShader.c_str();
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vstr, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fstr, NULL);
	glCompileShader(fs);

	

	GLuint program = glCreateProgram();
	glAttachShader(program, fs);
	glAttachShader(program, vs);
	glLinkProgram(program);
	CHECK_GL_ERROR;

	GLint shader_linked;
	glGetProgramiv(program, GL_LINK_STATUS, &shader_linked);
	if (!shader_linked) {
		GLint maxLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
		char* infoLog = new char[maxLength + 1];
		glGetProgramInfoLog(program, maxLength, NULL, infoLog);
		SIBR_WRG << "GLSL program failed to link " << std::endl
			<< "Shader linking log:" << std::endl
			<< infoLog << std::endl;
		delete[] infoLog;

	}


	//GLint u_res = glGetUniformLocation(program, "resolution");
	//glProgramUniform2f(program, u_res, float(invRes.x()), float(invRes.y()));
	//CHECK_GL_ERROR;

	// render
	glViewport(0, 0, invRes.x(), invRes.y());
	glClear(GL_COLOR_BUFFER_BIT);
	CHECK_GL_ERROR;
	glUseProgram(program);
	CHECK_GL_ERROR;
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, mesh->triangles().size() * 3, GL_UNSIGNED_INT, 0);

	CHECK_GL_ERROR;

	// download result
	ImageRGB32F invFlowImg(invRes.x(), invRes.y());
	glBindTexture(GL_TEXTURE_2D, invFlowTexture);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, invFlowImg.data());

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	CHECK_GL_ERROR;
	return invFlowImg;
}

//==============================================================

void QHarmonicSolver::remapBoundaries(Mesh::Vertices& verts, const ImageL32F& magMap)
{
	const Vector2u& res = magMap.size();
	const Vector2u& gridRes = res + Vector2u::Ones();

	// bottom & top independently
	Eigen::VectorXf cumSumBottom(res.x());
	Eigen::VectorXf cumSumTop(res.x());
	cumSumBottom(0) = magMap(0, 0).x();
	cumSumTop(0) = magMap(0, res.y() - 1).x();
	for (unsigned int i = 1; i < res.x(); i++)
	{
		cumSumBottom(i) = cumSumBottom(i - 1) + magMap(i, 0).x();
		cumSumTop(i) = cumSumTop(i - 1) + magMap(i, res.y() - 1).x();
	}
	cumSumBottom /= cumSumBottom(cumSumBottom.size() - 1);
	cumSumTop /= cumSumTop(cumSumTop.size() - 1);

	for (unsigned int i = 1; i < gridRes.x(); i++)
	{
		verts.at(index2Dto1D(Vector2u(i, 0), gridRes)).x() = cumSumBottom(i - 1);
		verts.at(index2Dto1D(Vector2u(i, res.y()), gridRes)).x() = cumSumTop(i - 1);
	}

	// left & right together
	Eigen::VectorXf cumSumSides(res.y());
	cumSumSides(0) = magMap(0, 0).x() + magMap(res.x() - 1, 0).x();
	for (unsigned int i = 1; i < res.y(); i++)
	{
		cumSumSides(i) = cumSumSides(i - 1) + magMap(0, i).x() + magMap(res.x() - 1, i).x();
	}
	cumSumSides /= cumSumSides(cumSumSides.size() - 1);

	for (unsigned int i = 1; i < gridRes.y(); i++)
	{
		verts.at(index2Dto1D(Vector2u(0, i), gridRes)).y() = cumSumSides(i - 1);
		verts.at(index2Dto1D(Vector2u(res.x(), i), gridRes)).y() = cumSumSides(i - 1);
	}
}

//==============================================================

Eigen::MatrixXf QHarmonicSolver::prepareRHS(const Mesh::Vertices& verts, const Vector2u& res)
{
	Vector2u gridRes = res + Vector2u::Ones();
	Eigen::MatrixXf uv = Eigen::MatrixXf::Zero(verts.size(), 2);
	for (unsigned int i = 0; i < res.x(); i++)
	{
		unsigned int lowerBoundaryIndex = index2Dto1D(Vector2u(i, 0), gridRes);
		unsigned int upperBoundaryIndex = index2Dto1D(Vector2u(i, gridRes.y() - 1), gridRes);
		uv.row(lowerBoundaryIndex) = verts.at(lowerBoundaryIndex).xy();
		uv.row(upperBoundaryIndex) = verts.at(upperBoundaryIndex).xy();
	}
	for (unsigned int i = 0; i < res.y(); i++)
	{
		unsigned int leftBoundaryIndex = index2Dto1D(Vector2u(0, i), gridRes);
		unsigned int rightBoundaryIndex = index2Dto1D(Vector2u(gridRes.x() - 1, i), gridRes);
		uv.row(leftBoundaryIndex) = verts.at(leftBoundaryIndex).xy();
		uv.row(rightBoundaryIndex) = verts.at(rightBoundaryIndex).xy();
	}
	return uv;
}

//==============================================================

Eigen::SparseMatrix<float> QHarmonicSolver::buildStiffnessMatrix(
	const Mesh::Vertices& verts,
	const Mesh::Triangles& tris,
	const ImageL32F& magMap)
{
	vector<Eigen::Triplet<float>> entries;

	for (unsigned int i = 0; i < tris.size(); i++)
	{
		Vector2u pxCoord = index1Dto2D(i, magMap.size());
		float magnification = magMap(pxCoord.x(), pxCoord.y()).x();
		Vector3u triInd = tris.at(i);

		Vector2f p1 = verts.at(triInd.x()).xy();
		Vector2f p2 = verts.at(triInd.y()).xy();
		Vector2f p3 = verts.at(triInd.z()).xy();

		Vector2f d23 = p3 - p2;
		Vector2f d31 = p1 - p3;
		Vector2f d12 = p2 - p1;

		Vector2f g1 = Vector2f(-d23.y(), d23.x());
		Vector2f g2 = Vector2f(-d31.y(), d31.x());
		Vector2f g3 = Vector2f(-d12.y(), d12.x());

		Vector3f value = 0.25f * Vector3f(
			g2.x() * g3.x() + g2.y() * g3.y(),
			g3.x() * g1.x() + g3.y() * g1.y(),
			g1.x() * g2.x() + g1.y() * g2.y());

		// just scaling for now, anisotropies possible
		value /= magnification;

		if (value.z() != 0)
		{
			entries.push_back(Eigen::Triplet<float>(triInd.x(), triInd.y(), value.z()));
			entries.push_back(Eigen::Triplet<float>(triInd.y(), triInd.x(), value.z()));
		}
		if (value.x() != 0)
		{
			entries.push_back(Eigen::Triplet<float>(triInd.y(), triInd.z(), value.x()));
			entries.push_back(Eigen::Triplet<float>(triInd.z(), triInd.y(), value.x()));
		}
		if (value.y() != 0)
		{
			entries.push_back(Eigen::Triplet<float>(triInd.z(), triInd.x(), value.y()));
			entries.push_back(Eigen::Triplet<float>(triInd.x(), triInd.z(), value.y()));
		}
		if (value.y() != -value.z())
		{
			entries.push_back(Eigen::Triplet<float>(triInd.x(), triInd.x(), -value.y() - value.z()));
		}
		if (value.z() != -value.x())
		{
			entries.push_back(Eigen::Triplet<float>(triInd.y(), triInd.y(), -value.z() - value.x()));
		}
		if (value.x() != -value.y())
		{
			entries.push_back(Eigen::Triplet<float>(triInd.z(), triInd.z(), -value.x() - value.y()));
		}
	}
	Eigen::SparseMatrix<float> matrix(verts.size(), verts.size());
	matrix.setFromTriplets(entries.begin(), entries.end());
	return matrix;
}

//==============================================================

Eigen::VectorXi QHarmonicSolver::innerIndices(const Vector2u& res)
{
	Eigen::VectorXi v((res.x() - 2) * (res.y() - 2));
	const int fullVertexCount = res.prod();
	int insertionIndex = 0;
	for (unsigned int i = res.x() + 1; i < fullVertexCount - (res.x() + 1); i++)
	{
		const Vector2i coord2D(i % res.x(), i / res.x());
		if (coord2D.x() != 0 &&
			coord2D.y() != 0 &&
			coord2D.x() != res.x() - 1 &&
			coord2D.y() != res.y() - 1)
		{
			v(insertionIndex) = i;
			insertionIndex++;
		}
	}
	return v;
}

