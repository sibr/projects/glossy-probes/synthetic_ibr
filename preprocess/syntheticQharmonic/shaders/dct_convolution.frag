/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 400

layout(location = 0) out float convOut;

uniform sampler2D inputTexture2D;

uniform int dctResolution;
uniform float weightExponent;

#define M_PI 3.14159265358979323846

//=======================================================================

float cosFactor(int m, int p)
{
	return cos(M_PI * (2 * m + 1) * p / (2 * dctResolution));
}

//=======================================================================

float alpha(int p)
{
	if (p == 0) return 1.f / sqrt(dctResolution);
	else return sqrt(2.f / dctResolution);
}

//================================================

float dctValue(int pattern, ivec2 coord)
{
	int p = pattern % dctResolution;
	int q = int(pattern / dctResolution);

	// weigh high-frequency patterns more
	float dst = min(float(q), float(p)) / dctResolution;
	float weight = (weightExponent > 0) ? pow(dst, weightExponent) : 1;

	float value = alpha(p) * alpha(q) * cosFactor(coord.x, p) * cosFactor(coord.y, q);
	value *= weight;
	
	return value;
}

//================================================

void main()
{
	ivec2 coord2D = ivec2(gl_FragCoord.xy);
	int dctPatterns = dctResolution * dctResolution;

	convOut = 0;

	// iterate over DCT patterns
	for (int i = 0; i < dctPatterns; i++)
	{
		vec3 filterOut = vec3(0);
		
		// iterate over DCT coefficients to convolve
		for (int y = 0; y < dctResolution; y++)
		{
			for (int x = 0; x < dctResolution; x++)
			{
				float filterWeight = dctValue(i, ivec2(x, y));
				ivec2 imgCoord = coord2D + ivec2(x, y) - ivec2(0.5 * dctResolution);
				vec3 inTexel = texelFetch(inputTexture2D, imgCoord, 0).xyz;
				vec3 filtered = filterWeight * inTexel;
				filterOut += filtered;
			}
		}
		// sum over absolute values of responses in all 3 dimensions
		convOut += dot(abs(filterOut), vec3(1));
	}
	convOut /= dctPatterns;
}