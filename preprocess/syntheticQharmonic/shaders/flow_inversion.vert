/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 400

in vec3 p;
in vec2 uv;

out vec2 v_uv;

//uniform vec2 resolution;

void main()
{
	gl_Position.xy = 2. * p.xy - vec2(1);
	//gl_Position.xy /= vec2(1) + vec2(1) / resolution;	// subtle correction
	gl_Position.zw = vec2(p.z, 1.0);
	v_uv = vec2(uv.x, 1.0 - uv.y);
}