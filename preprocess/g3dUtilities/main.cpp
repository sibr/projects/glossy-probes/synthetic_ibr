/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/graphics/Window.hpp>
#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "core/system/String.hpp"
#include "core/system/XMLTree.h"
#include "core/system/Transform3.hpp"
#include "projects/synthetic_ibr/renderer/MeshOBJExporter.hpp"
#include <core/assets/InputCamera.hpp>

#include <projects/synthetic_ibr/renderer/SyntheticIBRComponents.hpp>

using namespace sibr;

struct G3DHelperArgs {
	sibr::Arg<std::string> path = { "path", "", "Dataset path" };
	sibr::Arg<std::string> name = { "name", "", "Probe list name" };
	sibr::Arg<std::string> cameraPath = { "camera", "", "path to a lookat path to convert to a list of matrices that can be put in a G3D scene" };
};

/// Helpers for Mitsuba shape lights.

sibr::Matrix4f parseTransformation(rapidxml::xml_node<>* nodeTrans) {
	if (!nodeTrans) {
		return sibr::Matrix4f::Identity();
	}

	sibr::Matrix4f m1;
	rapidxml::xml_node<>* nodeM1 = nodeTrans
		->first_node("matrix");
	rapidxml::xml_node<>* nodeT = nodeTrans->
		first_node("translate");
	rapidxml::xml_node<>* nodeR = nodeTrans->
		first_node("rotate");
	rapidxml::xml_node<>* nodeS = nodeTrans->
		first_node("scale");
	//matrix case
	if (nodeM1) {
		std::string matrix1 = nodeM1->
			first_attribute("value")->value();

		std::istringstream issM1(matrix1);
		std::vector<std::string> splitM1(
			std::istream_iterator<std::string>{issM1},
			std::istream_iterator<std::string>());
		m1 <<
			std::stof(splitM1[0]), std::stof(splitM1[1]), std::stof(splitM1[2]), std::stof(splitM1[3]),
			std::stof(splitM1[4]), std::stof(splitM1[5]), std::stof(splitM1[6]), std::stof(splitM1[7]),
			std::stof(splitM1[8]), std::stof(splitM1[9]), std::stof(splitM1[10]), std::stof(splitM1[11]),
			std::stof(splitM1[12]), std::stof(splitM1[13]), std::stof(splitM1[14]), std::stof(splitM1[15]);
	}
	else { //separates transformations case

		auto getAxesValues = [&](rapidxml::xml_node<>* n,
			float& x, float& y, float& z) {
				if (n) {
					if (n->first_attribute("x"))
						x = std::stof(n->first_attribute("x")
							->value());
					if (n->first_attribute("y"))
						y = std::stof(n->first_attribute("y")
							->value());
					if (n->first_attribute("z"))
						z = std::stof(n->first_attribute("z")
							->value());
				}
		};
		float scaleX = 1.f, scaleY = 1.f, scaleZ = 1.f;
		float rotateX = 0.f, rotateY = 0.f, rotateZ = 0.f;
		float translateX = 0.f, translateY = 0.f, translateZ = 0.f;
		getAxesValues(nodeS, scaleX, scaleY, scaleZ);
		getAxesValues(nodeR, rotateX, rotateY, rotateZ);
		getAxesValues(nodeT, translateX, translateY, translateZ);

		Transform3<float> transform;
		transform.rotate(sibr::Vector3f(
			rotateX,
			rotateY,
			rotateZ));
		transform.translate(sibr::Vector3f(
			translateX,
			translateY,
			translateZ));
		m1 <<
			scaleX, 0.f, 0.f, 0.f,
			0.f, scaleY, 0.f, 0.f,
			0.f, 0.f, scaleZ, 0.f,
			0.f, 0.f, 0.f, 1.f;
		m1 = transform.matrix() * m1;
	}

	return m1;
}

void convertShapesToMeshes(int argc, char** argv) {
	struct FixMitsubaArgs {
		RequiredArg<std::string> path = { "path" };
	} args1;
	// Mitsuba conversion.
	const std::string outputDir = sibr::parentDirectory(args1.path) + "/shapes/";
	sibr::makeDirectory(outputDir);


	const std::string objString = "obj";
	const std::string filenameString = "filename";
	const std::string stringString = "string";


	XMLTree doc(args1.path);
	rapidxml::xml_node<>* nodeScene = doc.first_node("scene");


	int id = 0;
	// Look over all shapes.
	// For now we won't support instances and this kind of stuff.
	for (rapidxml::xml_node<>* node = nodeScene->first_node("shape");
		node; node = node->next_sibling("shape")) {
		const std::string type = node->first_attribute("type")->value();
		bool replaced = false;
		std::string objname;
		// Can we find a transformation.
		rapidxml::xml_node<>* tfnode = node->first_node("transform");
		if (type == "rectangle") {
			const sibr::Matrix4f tf = parseTransformation(tfnode);

			std::vector<sibr::Vector3f> verts = { {-1.0f, -1.0f, 0.0f}, {1.0f, -1.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {-1.0f, 1.0f, 0.0f} };
			for (int i = 0; i < verts.size(); ++i) {
				verts[i] = (tf * sibr::Vector4f(verts[i][0], verts[i][1], verts[i][2], 1.0f)).xyz();
			}
			const std::vector<sibr::Vector3u> tris = { {0, 1, 2} ,{0 ,2, 3 } };
			Mesh mesh(false);
			mesh.vertices(verts);
			mesh.triangles(tris);
			mesh.generateNormals();
			objname = "shape_" + std::to_string(id) + ".obj";
			MeshOBJHandler::saveOBJ(mesh, outputDir + "/" + objname);
			replaced = true;

		}
		else if (type == "disk") {
			const sibr::Matrix4f tf = parseTransformation(tfnode);
			std::vector<sibr::Vector3f> vertices(257);
			std::vector<sibr::Vector3u> triangles(256);
			vertices[0] = { 0.0f, 0.0f, 0.0f };
			for (int i = 1; i < 257; ++i) {
				const float angle = float(i - 1) / 257.0f * 2.0f * float(M_PI);
				vertices[i] = { std::cos(angle), std::sin(angle), 0.0f };
				triangles[i - 1] = { 0, uint(i), i == 256 ? 1 : uint(i + 1) };
			}
			for (int i = 0; i < 257; ++i) {
				vertices[i] = (tf * sibr::Vector4f(vertices[i][0], vertices[i][1], vertices[i][2], 1.0f)).xyz();
			}
			Mesh mesh(false);
			mesh.vertices(vertices);
			mesh.triangles(triangles);
			mesh.generateNormals();
			objname = "shape_" + std::to_string(id) + ".obj";
			MeshOBJHandler::saveOBJ(mesh, outputDir + "/" + objname);
			replaced = true;
		}

		if (replaced) {

			node->first_attribute("type")->value(objString.c_str());
			if (tfnode) {
				node->remove_node(tfnode);
			}
			objname = "shapes/" + objname;
			const char* newvalue = doc.allocate_string(objname.c_str(), objname.size() + 1);
			rapidxml::xml_node<>* newnode = doc.allocate_node(rapidxml::node_type::node_element, "string");
			rapidxml::xml_attribute<>* newattr = doc.allocate_attribute("name", filenameString.c_str());
			rapidxml::xml_attribute<>* newattr1 = doc.allocate_attribute("value", newvalue);
			newnode->append_attribute(newattr);
			newnode->append_attribute(newattr1);
			node->append_node(newnode);
			++id;
		}
	}
	// Save xml.
	doc.save(sibr::removeExtension(args1.path) + "_edited.xml");
}


/// Helpers for G3D probe generation.

sibr::Vector2f worldToSpherical(const sibr::Vector3f& world_coord) {
	const sibr::Vector3f n_world_coord = world_coord.normalized();
	const float longitude = std::atan2(-n_world_coord[0], n_world_coord[2]);
	const float latitude = std::acos(n_world_coord[1]);
	const sibr::Vector2f norm_coord = { (longitude / M_PI) * 0.5f + 0.5f, (latitude / M_PI) };
	return norm_coord;
}

void resampleCubemaps(const std::string & basePath, const std::string & outPath) {
	sibr::makeDirectory(outPath);

	const int tSize = 1024;
	auto files = sibr::listFiles(basePath, false, false, { "exr" });
	for (const auto& file : files) {
		ImageRGB32F latlong;
		latlong.load(basePath + "/" + file);
		const sibr::Vector2f dims(float(latlong.w()), float(latlong.h()));
		const std::vector<int> mainDir = { 0,0,1,1,2,2 };
		const std::vector<int> mainSgn = { 1,-1,1,-1,1,-1 };
		const std::vector<int> xDir = { 2,2,0,0,0,0 };
		const std::vector<int> xSgn = { -1,1,1,1,1,-1 };
		const std::vector<int> yDir = { 1,1,2,2,1,1 };
		const std::vector<int> ySgn = { -1,-1,1,-1,-1,-1 };
		for (int fid = 0; fid < 6; ++fid) {
			ImageRGB32F face(tSize, tSize);
#pragma omp parallel for
			for (int y = 0; y < tSize; ++y) {
				for (int x = 0; x < tSize; ++x) {
					sibr::Vector3f pix;
					pix[mainDir[fid]] = float(mainSgn[fid] * tSize / 2);
					pix[xDir[fid]] = float(xSgn[fid] * (x - tSize / 2));
					pix[yDir[fid]] = float(ySgn[fid] * (y - tSize / 2));
					const sibr::Vector2f uv = worldToSpherical(pix);
					face(x, y) = latlong.bilinear(uv.cwiseProduct(dims));
				}
			}
			face.saveHDR(outPath + "/" + sibr::removeExtension(file) + "_" + std::to_string(fid) + ".exr");
		}
	}
}

std::string convertLookAtToG3D(const std::string& path) {

	auto cams = InputCamera::loadLookat(path, { {1920, 1080} });
	std::stringstream out;
	out << "track = PFrameSpline{" << "\n";
	out << "\tcontrol = (\n";
	int id = 0;
	for (const auto& cam : cams) {
		const Matrix4f m = cam->view().inverse();
		out << "\t\tMatrix4(";
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				out << m(i, j) << ((i == 3 && j == 3) ? ")" : ",");
			}
		}
		out << (id == int(cams.size()) - 1 ? "" : ",") << "\n";
		++id;
	}
	out << "\t);\n\ttime = (";
	id = 0;
	const float framerate = 30.0f;
	for (const auto& cam : cams) {
		out << (float(id) / framerate) << (id == cams.size() - 1 ? "" : ",");
		++id;
	}
	out << ");\n";
	out << "\tinterpolationMode = \"LINEAR\";\n};\n";
	return out.str();
}

/** Converts probes to the G3D layout, and convert camera paths to G3D splines. */
int main(int argc, char** argv) {

	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	const G3DHelperArgs args;

	// If we have a camera path, convert it and exit.
	if(!args.cameraPath.get().empty()) {
		const auto str = convertLookAtToG3D(args.cameraPath);
		std::ofstream outCam(sibr::removeExtension(args.cameraPath) + ".g3dpath");
		if(outCam.is_open()) {
			outCam << str << std::endl;
			outCam.close();
		} else {
			SIBR_WRG << "Unable to open output file." << std::endl;
		}
		return EXIT_SUCCESS;
	}

	
	// Else we are in conversion mode.
	const std::string inPath = args.path.get() + "/g3d/probes/";
	const std::string outPath = args.path.get() + "/g3d/probescube/";
	// Resample the cubemaps.
	resampleCubemaps(inPath, outPath);
	// We also need to generate the proper specification file based on the probe locations.
	ProbeLocations probes;
	probes.setupFromPath(args.path.get() + "/probes/" + args.name.get() + ".positions");
	sibr::Vector3f mini = probes.locations()[0];
	sibr::Vector3f maxi = probes.locations()[0];
	for(int i = 1; i < probes.locations().size(); ++i) {
		mini = mini.cwiseMin(probes.locations()[i]);
		maxi = maxi.cwiseMax(probes.locations()[i]);
	}
	// We also need to find the number of probes on each axis.
	// Minimal spacing between two probes on each axis (assume that all probes are placed along the same axis).
	sibr::Vector3f minDist = (maxi - mini) / float(probes.locations().size());
	std::array<std::vector<float>, 3> coords;

	// Check if the current probe is far from all encountered one on an axis.
	for(const sibr::Vector3f & pos : probes.locations()) {
		for(int i = 0; i < 3; ++i) {
			bool farFromAll = true;
			for(float coord : coords[i]) {
				if(std::abs(coord - pos[i]) <= minDist[i]) {
					farFromAll = false;
					break;
				}
			}
			if(farFromAll) {
				coords[i].emplace_back(pos[i]);
			}
		}
	}
	const sibr::Vector3i count = { int(coords[0].size()), int(coords[1].size()), int(coords[2].size()) };
	
	std::ofstream strs(args.path.get() + "/g3d/" + args.name.get() + ".LightFieldModelSpecification.Any");
	if(!strs.is_open()) {
		SIBR_ERR << "Unable to open specification file to write." << std::endl;
		return EXIT_FAILURE;
	}
	strs << "LightFieldModel::Specification {" << "\n";
	strs << "\tprobeDimensions = AABox(Point3(" << mini[0] << "," << mini[1] << "," << mini[2] << "), Point3(" << maxi[0] << "," << maxi[1] << "," << maxi[2] << "));" << "\n";
	strs << "\tprobeCounts = Vector3int32(" << count[0] << "," << count[1] << "," << count[2] << ");" << "\n";
	strs << "\toctResolution = 1024;" << "\n";
	strs << "\tprobesBasePath = \"" << outPath << "\";"<< "\n";
	strs << "};" << std::endl;
	strs.close();
	return EXIT_SUCCESS;
}