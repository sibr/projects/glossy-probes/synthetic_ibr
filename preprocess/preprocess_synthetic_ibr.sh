# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


MITSUBA_PATH="D:/sirodrig/code/mitsuba/mitsuba/"
SIBR_PATH="D:/sirodrig/code/sibr/install/bin/"

SCENE_PATH=$1
STEP=$2
SCENE_NAME=$3
SCENE_W=$4
LIGHTMAP_SAMPLES=$3
LIGHTMAP_W=$4

printHelpMessageAndExit(){
	echo "Usage:"
	echo "* Scene generation:"
	echo "	preprocess_synthetic_ibr.sh path/to/scene step samples_count width"
	echo "	where step in (uvmap, lightmap, merge)"
	echo "* Probe generation:"
	echo "	preprocess_synthetic_ibr.sh path/to/scene step probes_set_name width"
	echo "	where step in (preproc, render, pack)"
	exit 0
}

if [[ $# -ne 4 ]]; then
	printHelpMessageAndExit
fi


if [ "$STEP" == "uvmap" ]; then

cd ${SIBR_PATH}
mkdir -p ${SCENE_PATH}/lightmap/
./syntheticUVMapper_rwdi.exe --path ${SCENE_PATH}/scene/scene.xml --output ${SCENE_PATH}/lightmap/scene_light_map.obj --size ${LIGHTMAP_W}
./curvatureEstimation_rwdi.exe --path ${SCENE_PATH}
 
elif [ "$STEP" == "lightmap" ]; then

cd ${MITSUBA_PATH}
python lightmapRender.py  ${SCENE_PATH}/lightmap/scene_light_map.obj  ${SCENE_PATH}/scene/scene.xml --samples ${LIGHTMAP_SAMPLES} --output ${SCENE_PATH}/lightmap/scene_light_map.exr --res ${LIGHTMAP_W}

elif [ "$STEP" == "merge" ]; then

cd ${SIBR_PATH}
./syntheticHdrTexturer_rwdi.exe --path ${SCENE_PATH}/lightmap/scene_light_map.exr --output ${SCENE_PATH}/lightmap/scene_light_map.png
 
elif [ "$STEP" == "preproc" ]; then

cd ${MITSUBA_PATH}
mkdir -p ${SCENE_PATH}/warps/
mkdir -p ${SCENE_PATH}/warps/${SCENE_NAME}
python magnificationMapRender.py ${SCENE_PATH}/probes/${SCENE_NAME}.positions ${SCENE_PATH}/scene/scene.xml --samples 32 --output ${SCENE_PATH}/warps/${SCENE_NAME}/ --res 256

cd ${SIBR_PATH}
./syntheticQharmonic_rwdi.exe --path ${SCENE_PATH}/warps/${SCENE_NAME}/ --inv_res $SCENE_W --iterations 10 --kernel_size 15 --output_forward 1 --complexity_weight 1.0
./syntheticMapsGeneration_rwdi.exe --path ${SCENE_PATH} --name ${SCENE_NAME} --res $SCENE_W

elif [ "$STEP" == "render" ]; then

cd ${MITSUBA_PATH}
mkdir -p ${SCENE_PATH}/images/
mkdir -p ${SCENE_PATH}/images/raw/
python warpFieldRender.py ${SCENE_PATH}/probes/${SCENE_NAME}.positions ${SCENE_PATH}/scene/scene.xml ${SCENE_PATH}/warps/${SCENE_NAME}/inverse_flow/ --samples 256 --output ${SCENE_PATH}/images/raw/${SCENE_NAME}warp --mode 3 --res $SCENE_W

elif [ "$STEP" == "pack" ]; then

mv ${SCENE_PATH}/images/raw/${SCENE_NAME}warp_3 ${SCENE_PATH}/images/${SCENE_NAME}/
cd ${SIBR_PATH}
./imageBinPacker_rwdi.exe --path ${SCENE_PATH}/images/${SCENE_NAME}/

else
	printHelpMessageAndExit
fi
