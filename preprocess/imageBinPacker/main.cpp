/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/system/CommandLineArgs.hpp>
#include <core/system/String.hpp>
#include <core/system/Utils.hpp>
#include <core/graphics/Image.hpp>

#define PROGRAM_NAME "imageBinPacker"
using namespace sibr;

struct ImageBinPackerArgs :
	virtual AppArgs {
	RequiredArg<std::string> path = { "path", "path to the exr images directory" };
	Arg<std::string> output = { "output", "", "output directory path" };
};

/** Pack images in a binary format, smaller on disk, faster to load.
 *\todo Add a viewer to simplify scene debug.*/
int main(int ac, char** av) {

	sibr::CommandLineArgs::parseMainArgs(ac, av);
	ImageBinPackerArgs args;

	// Input/output paths.
	const std::string inputPath = args.path;
	std::string outputPath = args.output;
	if(outputPath.empty()) {
		outputPath = inputPath;
	}
	sibr::makeDirectory(outputPath);

	const auto images = sibr::listFiles(inputPath, false, false, { "exr" });

	for (const auto & image : images) {
		ImageRGB32F img;
		img.load(inputPath + "/" + image, false, false);
		img.saveByteStream(outputPath + "/" + sibr::removeExtension(image) + ".bin", true);
	}
	return EXIT_SUCCESS;
}