/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "core/system/CommandLineArgs.hpp"
#include "core/system/String.hpp"
#include "core/graphics/Image.hpp"
#include "core/graphics/Texture.hpp"
#include "core/graphics/Window.hpp"
#include "core/view/MultiViewManager.hpp"
#include "RoughnessTestView.hpp"
#include "BRDFTestView.hpp"

using namespace sibr;

#define PROGRAM_NAME "roughnessTest"



struct RoughnessTestArgs : public WindowAppArgs {
	Arg<std::string> output = {"output","","destination path for the LUT"};
	Arg<bool> brdfViewer = { "brdf","Show the BRDF viewer GUI" };
};

int main(int ac, char** av) {

	CommandLineArgs::parseMainArgs(ac, av);
	RoughnessTestArgs args;

	// Window setup
	Window window(PROGRAM_NAME, sibr::Vector2i(50, 50), args);
	
	const std::shared_ptr<BRDFTestView> imgView(new BRDFTestView());

	// Display viewer if required.
	if (args.brdfViewer) {

		// Add views to the MVM.
		MultiViewManager multiViewManager(window, false);
		multiViewManager.addSubView("BRDF view", imgView, {1024,1024});

		// Main looooooop.
		while (window.isOpened()) {

			sibr::Input::poll();
			window.makeContextCurrent();
			if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
				window.close();
			}
			window.bind();
			window.viewport().bind();

			glClear(GL_COLOR_BUFFER_BIT);
			multiViewManager.onUpdate(sibr::Input::global());
			multiViewManager.onRender(window);

			CHECK_GL_ERROR;
			window.swapBuffer();
		}
		
	} else {

		// Just generate the lookup table and save it.
		const sibr::Vector3f mins = { 0.0f, 0.01f, 0.01f };
		const sibr::Vector3f maxs = { 0.5f, 10.0f, 10.0f };
		const int res = 32;
		const bool powerSampling = true;

		SIBR_LOG << "Sampling..." << std::endl;
		imgView->sample(res, mins, maxs, powerSampling, args.output);
		SIBR_LOG << "Done." << std::endl;
		window.close();
	}

	

	return EXIT_SUCCESS;

}
