/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include <core/system/Config.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Image.hpp>
# include <core/graphics/Shader.hpp>
# include <core/view/ViewBase.hpp>

namespace sibr {

	/** Basic image comparison, looking at luminance and channels along line cuts. */
	class RoughnessTestView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(RoughnessTestView);

	public:

		RoughnessTestView(const std::string & path);

		void onRender(const Viewport & vpRender) override;

		void onUpdate(Input& input, const Viewport & vp) override;

		void onGUI() override;

	protected:
		void updateVisuGT();
		void updateValues();
		
		ImageRGB32F::Ptr _gt;
		ImageRGB32F::Ptr _gtCopy;
		Texture2DRGB32F::Ptr _gtTex;
		struct TexInfos {
			ImageRGB32F::Ptr img;
			ImageRGB32F::Ptr proc;
			Texture2DRGB32F::Ptr tex;
			Texture2DRGB32F::Ptr procTex;
			std::string name;
			bool showBlur = false;
		};
		std::vector<TexInfos> _probes;
		GLShader _blit;
		int _row = 100;
		int _probe = 0;
		float _scale = 1.0f;
		float _intensity = 1.0f;

		sibr::Vector2f _minmax = {0.0f, 1.5f};
		struct Values {
			std::vector<float> r;
			std::vector<float> g;
			std::vector<float> b;
			std::vector<float> l;

			void resize(size_t size) {
				r.resize(size);
				g.resize(size);
				b.resize(size);
				l.resize(size);
			}

			void computeLuminance() {
				for (uint i = 0; i < r.size(); ++i) {
					l[i] = (r[i] + g[i] + b[i]) / 3.0f;
				}
			}
		};

		Values _valuesGT;
		Values _valuesProbe;
		Values _valuesProbeProc;
	};

}