/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include <core/system/Config.hpp>
# include <core/assets/InputCamera.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Image.hpp>
# include <core/graphics/Shader.hpp>
# include <core/view/ViewBase.hpp>
#include <core\view\InteractiveCameraHandler.hpp>

namespace sibr {

	/** Visualize BRDF footprint in a simple case (frontal light and camera, fixed isotropic roughness).
	 * Allows for the generation of a variance lookup table by fitting a 1D gaussian to the BRDF footprint
	 * for a set of roughnesses and view/light distances.
	 */
	class BRDFTestView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(BRDFTestView);

	public:

		BRDFTestView();

		void onRender(const Viewport & vpRender) override;

		void onUpdate(Input& input, const Viewport& vp) override;

		void onGUI() override;

		/** Tabulate the variance corresponding to a set of roughness, light distance, view distance values.
		 * \param res sampling resolution on each axis
		 * \param mins minimal values for roughness, light-surface distance, camera-surface distance
		 * \param maxs maximal values for roughness, light-surface distance, camera-surface distance
		 * \param powerSampling use a power scheme when spacing samples
		 * \param outputPath destination file path
		 */
		void sample(int res, const sibr::Vector3f& mins, const sibr::Vector3f& maxs, bool powerSampling, const std::string& outputPath);

	protected:

		/** Evaluate variance by performing a render of the BRDF and fitting a gaussian. */
		float computeVar(float roughness, float lightDist, float camDist);

		void initShaders();

		void updateCamAndLight();

		/** Update the lookup table. */
		void uploadTex3D(const std::vector<float>& data, int res);

		GLShader _blit;
		GLShader _brdfView;
		GLuniform<sibr::Vector3f> _camPos;
		GLuniform<sibr::Vector3f> _lightPos;
		GLuniform<sibr::Vector3f> _planeNormal;
		GLuniform<sibr::Vector3f> _imgCorner;
		GLuniform<sibr::Vector3f> _imgDx;
		GLuniform<sibr::Vector3f> _imgDy;
		GLShader _varView;
		GLuniform<int> _axis;
		GLuniform<int> _slice;
		GLuniform<sibr::Vector2f> _scales;
		GLuniform<sibr::Matrix4f> _mvp;
		GLuniform<bool> _mode2D;
		GLShader _gizmo;
		GLuniform<sibr::Matrix4f> _mvpGiz;

		GLuniform<float> _roughness;
		float _camDist = 1.0f;
		float _lightDist = 1.0f;
		float _fov = 70.0f * float(M_PI) / 180.0f;
		
		InputCamera _cam;
		RenderTargetRGB32F::Ptr _dst;
		ImageRGB32F _dstImg;
		std::vector<float> _values;
		std::vector<float> _fit;
		double _var = 0.0;

		GLuint _tex3D;
		bool _show3D = false;
		Mesh::Ptr _grid;
		Mesh::Ptr _axes;
		InteractiveCameraHandler _handler; 

		sibr::Vector3f _mins = {0.0f, 0.01f, 0.01f};
		sibr::Vector3f _maxs = { 0.5f, 10.0f, 10.0f };
		int _res = 32;
		bool _powerSampling = true;
	};

}