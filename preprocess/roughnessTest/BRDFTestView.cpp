/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "BRDFTestView.hpp"
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/graphics/GUI.hpp>


namespace sibr {

	BRDFTestView::BRDFTestView()
	{
		
		initShaders();
		_roughness = 0.05f;
		_planeNormal = { 0.0f, 0.0f, 1.0f };
		_axis = 0;
		_slice = 0;
		_scales = { 0.0f, 1.0f };
		_mode2D = true;

		updateCamAndLight();
		_dst.reset(new RenderTargetRGB32F(1024, 1024));
		_var = computeVar(_roughness, _lightDist, _camDist);


		Eigen::AlignedBox3f box(Eigen::Vector3f( -1.0f,-1.0f,-1.0f ), Eigen::Vector3f( 1.0f,1.0f,1.0f ));
		_handler.setup(box, Viewport(0,0,1024,1024), nullptr);
		_handler.switchMode(InteractiveCameraHandler::InteractionMode::TRACKBALL);

		_axes.reset(new Mesh(true));
		std::vector<sibr::Vector3f> verts = { {-1.0f, 0.0f, -1.0f}, {1.0f, 0.0f, -1.0f} , {-1.0f, 0.0f, 1.0f} };
		std::vector<sibr::Vector3f> cols = { {1.0f, 1.0f, 1.0f}, {0.0, 1.0f, 0.0f} , {1.0, 0.0f, 0.0f} };
		_axes->vertices(verts);
		_axes->colors(cols);
		std::vector<sibr::Vector3u> tris = { {0,1,2} };
		_axes->triangles(tris);
	}

	void BRDFTestView::initShaders() {
		_blit.init("Blit", sibr::loadFile(
			sibr::getShadersDirectory("core") + "/texture.vert"),
			sibr::loadFile(sibr::getShadersDirectory("core") + "/texture.frag"));
		_brdfView.init("BRDFViewer", sibr::loadFile(
			sibr::getShadersDirectory("core") + "/texture.vert"),
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/brdf_viewer.frag"));
		_varView.init("varViewer", sibr::loadFile(
			sibr::getShadersDirectory("synthetic_ibr") + "/variance_viewer.vert"),
			sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/variance_viewer.frag"));
		_gizmo.init("Gizmo", sibr::loadFile(
			sibr::getShadersDirectory("core") + "/colored_mesh.vert"),
			sibr::loadFile(sibr::getShadersDirectory("core") + "/colored_mesh.frag"));

		_roughness.init(_brdfView, "roughness");
		_planeNormal.init(_brdfView, "planeNormal"); _camPos.init(_brdfView, "camPos");
		_lightPos.init(_brdfView, "lightPos");
		_imgCorner.init(_brdfView, "imgCorner");
		_imgDx.init(_brdfView, "imgDx");
		_imgDy.init(_brdfView, "imgDy");


		_axis.init(_varView, "axis");;
		_slice.init(_varView, "slice");;
		_scales.init(_varView, "scales");
		_mvp.init(_varView, "mvp");
		_mode2D.init(_varView, "mode2D");

		_mvpGiz.init(_gizmo, "MVP");
	}

	void BRDFTestView::updateCamAndLight() {
		_cam.size(1024, 1024);
		_cam.setLookAt({ 0.0f, 0.0f, _camDist }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f });
		_cam.perspective(_fov, 1.0f, 0.01f, 100.0f);
		_camPos = _cam.position();
		CameraRaycaster::computePixelDerivatives(_cam, _imgDx.get(), _imgDy.get(), _imgCorner.get());

		// Light: reflect camera dir around plane normal, origin is on the plane.
		_planeNormal = _planeNormal.get().normalized();
		const sibr::Vector3f refCamDir = _cam.position().normalized();
		const sibr::Vector3f lightDir = 2.0f * _planeNormal.get().dot(refCamDir) * _planeNormal - refCamDir;
		_lightPos = _lightDist * lightDir.normalized();
	}

	void BRDFTestView::onUpdate(Input& input, const Viewport& vp) {
		if (input.key().isReleased(Key::Code::F8)) {
			// Copy shader.
			const std::string shaderSrcPath = sibr::getBinDirectory() + "/../../src/projects/synthetic_ibr/renderer/shaders/";
			const std::string shaderDstPath = sibr::getShadersDirectory("synthetic_ibr") + "/";
			sibr::copyFile(shaderSrcPath + "brdf_viewer.frag", shaderDstPath + "brdf_viewer.frag", true);
			sibr::copyFile(shaderSrcPath + "variance_viewer.frag", shaderDstPath + "variance_viewer.frag", true);
			sibr::copyFile(shaderSrcPath + "variance_viewer.vert", shaderDstPath + "variance_viewer.vert", true);
			initShaders();
			updateCamAndLight();
		}

		_handler.update(input, 1.0f / 60.0f, vp);
	}
	
	void BRDFTestView::sample(int res, const sibr::Vector3f & mins, const sibr::Vector3f& maxs, bool powerSampling, const std::string& outputPath) {
		

		// Iterate over the range of values we are interested in, with a specific sampling scheme.
		std::vector<float> values(res * res * res);
		const float expD = powerSampling ? 2.0f : 1.0f;
		
		for (int dz = 0; dz < res; ++dz) {
			for (int dy = 0; dy < res; ++dy) {
				for (int dx = 0; dx < res; ++dx) {
					const float ddx = std::pow(float(dx) / (res - 1), expD);
					const float ddy = std::pow(float(dy) / (res - 1), expD);
					const float ddz = std::pow(float(dz) / (res - 1), expD);
					const float rough = mins[0] + ddx * (maxs[0] - mins[0]);
					const float light = mins[1] + ddy * (maxs[1] - mins[1]);
					const float cam   = mins[2] + ddz * (maxs[2] - mins[2]);
					values[dz*res*res + dy*res + dx] = computeVar(rough, light, cam);
				}
				std::cout << ".";
			}
			std::cout << "|";
		}
		std::cout << std::endl;
		// Fix monotonicity.
		for (int i = 0; i < 1; ++i) {
			for (int dz = res - 1; dz >= 0; --dz) {
				for (int dy = res - 2; dy >= 0; --dy) {
					for (int dx = res - 1; dx >= 0; --dx) {
						float currval = values[dz * res * res + dy * res + dx];
						const float currvalNextLum = values[dz * res * res + (dy + 1) * res + dx];
						values[dz * res * res + dy * res + dx] = std::min(currval, currvalNextLum);
					}
				}
			}
			for (int dz = res - 2; dz >= 0; --dz) {
				for (int dy = res - 1; dy >= 0; --dy) {
					for (int dx = res - 1; dx >= 0; --dx) {
						float currval = values[dz * res * res + dy * res + dx];
						const float currvalNext = values[(dz+1) * res * res + dy * res + dx];
						values[dz * res * res + dy * res + dx] = std::max(currval, currvalNext);
					}
				}
			}
			for (int dz = res - 1; dz >= 0; --dz) {
				for (int dy = res - 1; dy >= 0; --dy) {
					for (int dx = res - 2; dx >= 0; --dx) {
						float currval = values[dz * res * res + dy * res + dx];
						const float currvalNext = values[dz  * res * res + dy * res + dx + 1];
						values[dz * res * res + dy * res + dx] = std::min(currval, currvalNext);
					}
				}
			}
		}
		
		ByteStream ofile;
		ofile << res;
		ofile << mins[0] << maxs[0];
		ofile << mins[1] << maxs[1];
		ofile << mins[2] << maxs[2];
		for (int dx = 0; dx < res*res*res; ++dx) {
			ofile << values[dx];
		}
		ofile.saveToFile(outputPath);

		{
			float min = values[0];
			float max = values[0];
			for (int dx = 0; dx < res * res * res; ++dx) {
				min = std::min(min, values[dx]);
				max = std::max(max, values[dx]);
			}
			_scales.get()[0] = min;
			_scales.get()[1] = max;
		}

		uploadTex3D(values, res);
		_show3D = true;
	}


	void BRDFTestView::uploadTex3D(const std::vector<float>& data, int res) {
		
		CHECK_GL_ERROR;
		glGenTextures(1, &_tex3D);
		glBindTexture(GL_TEXTURE_3D, _tex3D);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		CHECK_GL_ERROR;
		glTexImage3D(GL_TEXTURE_3D,
			0,
			GL_R32F,
			res,res,res, 0,
			GL_RED,
			GL_FLOAT,
			data.data()
		);
		CHECK_GL_ERROR;
		glBindTexture(GL_TEXTURE_3D, 0);

		// Create the correpsonding mesh.
		_grid.reset(new Mesh(true));
		std::vector<sibr::Vector3f> vertices(res*res);
		int bnd = res / 2;
		for (int dy = 0; dy < res; dy += 1) {
			for (int dx = 0; dx < res; dx += 1) {
				vertices[dy * res + dx] = { float(dx) / (res - 1) * 2.0f - 1.0f,  0.0f, float(dy) / (res - 1) * 2.0f - 1.0f };
			}
		}
		std::vector<sibr::Vector3u> tris;
		for (int dy = 0; dy < res - 1; ++dy) {
			for (int dx = 0; dx < res - 1; ++dx) {
				uint id = dy * res + dx;
				tris.emplace_back(id, id + 1, id + 1 + res);
				tris.emplace_back(id, id + 1 + res, id + res);
			}
		}
		_grid->vertices(vertices);
		_grid->triangles(tris);
		_handler.setup(_grid, sibr::Viewport(0,0,1024, 1024));
	}

	void BRDFTestView::onGUI() {
		_handler.onGUI("brdf cam view");

		if (ImGui::Begin("BRDF viewer options")) {

			bool dirty = false;
			

			if (ImGui::SliderFloat("Roughness", &_roughness.get(), 0.0f, 1.0f)) {
				dirty = true;
			}

			if (ImGui::DragFloat("Light distance", &_lightDist, 0.05f)) {
				dirty = true;
			}
			if (ImGui::DragFloat("Camera distance", &_camDist, 0.05f)) {
				dirty = true;
			}

			if (dirty) {
				_var = computeVar(_roughness, _lightDist, _camDist);
			}
			ImGui::Text("Estimated variance: %f", _var);

			ImGui::InputInt("Res.", &_res); ImGui::SameLine();
			ImGui::Checkbox("Power sampling.", &_powerSampling);
			ImGui::InputFloat3("Mins (rough, light, cam)", &_mins[0]);
			ImGui::InputFloat3("Maxs (rough, light, cam)", &_maxs[0]);
			if (ImGui::Button("Sample")) {
				const std::string path = "./vars" + std::string(_powerSampling ? "_pow_" : "_") + std::to_string(_res) + "_" +
					std::to_string(_mins[0]) + "_" + std::to_string(_mins[1]) + "_" + std::to_string(_mins[2]) + "_" +
					std::to_string(_maxs[0]) + "_" + std::to_string(_maxs[1]) + "_" + std::to_string(_maxs[2]) + ".bin";
				sample(_res, _mins, _maxs, _powerSampling, path);
			}

			if (ImGui::Button("Load samples")) {
				std::string path;
				if (sibr::showFilePicker(path, FilePickerMode::Default) && !path.empty()) {
					ByteStream ifile;
					ifile.load(path);
					int res;
					sibr::Vector3f mins, maxs;
					ifile >> res;
					ifile >> mins[0] >> maxs[0];
					ifile >> mins[1] >> maxs[1];
					ifile >> mins[2] >> maxs[2];
					std::vector<float> values(res*res*res);
					for (int dx = 0; dx < res*res*res; ++dx) {
						ifile >> values[dx];
					}
					{
						float min = values[0];
						float max = values[0];
						for (int dx = 0; dx < res * res * res; ++dx) {
							min = std::min(min, values[dx]);
							max = std::max(max, values[dx]);
						}
						_scales.get()[0] = min;
						_scales.get()[1] = max;
					}
					uploadTex3D(values, res);
					_show3D = true;
				}
			}
			if (_show3D) {
				ImGui::InputInt("Slice", &_slice.get());
				ImGui::InputInt("Axis", &_axis.get());
				ImGui::DragFloat2("Scales", &_scales.get()[0], 1.0f);
				ImGui::Checkbox("Show 2D", &_mode2D.get());

				if (_axis == 0) {
					ImGui::Text("Constant roughness, X: Light, Y: Cam");
				} else if (_axis == 1) {
					ImGui::Text("Constant light, X: Roughness, Y: Cam");
				} else if (_axis == 2) {
					ImGui::Text("Constant camera, X: Roughness, Y: Light");
				}
			}
		}
		ImGui::End();

		// Display plot lines.
		if (ImGui::Begin("Profile BRDF")) {
			ImGui::PlotMultiLines("BRDF values", { _values.data(), _fit.data() }, int(_values.size()), 
				{ ImVec4(1.0f, 0.0f,1.0f,1.0f), ImVec4(0.0f, 1.0f,1.0f,1.0f) }, FLT_MAX, FLT_MAX, ImVec2(800, 300));
		}
		ImGui::End();
	}

	float BRDFTestView::computeVar(float roughness, float lightDist, float camDist){
		// Render the BRDF footprint to a texture.
		_lightDist = lightDist;
		_roughness = roughness;
		_camDist = camDist;
		updateCamAndLight();

		_dst->clear();
		_dst->bind();
		glViewport(0, 0, _dst->w(), _dst->h());
		_brdfView.begin();
		_camPos.send();
		_lightPos.send();
		_planeNormal.send();
		_imgCorner.send();
		_imgDx.send();
		_imgDy.send();
		_roughness.send();
		RenderUtility::renderScreenQuad();
		_brdfView.end();
		_dst->unbind();

		_dst->readBack(_dstImg);

		_values.resize(_dst->w());
		_fit.resize(_dst->w());
		
		// Read values along a line for visualisation.
		double sumline = 0.0;
		for (int x = 0; x < int(_dstImg.w()); ++x) {
			const float val = _dstImg(x, _dstImg.h() / 2)[0];
			sumline += val;
			_values[x] = val;
		}
		for (int x = 0; x < int(_dstImg.w()); ++x) {
			_values[x] /= float(sumline);
		}
		
		// Fit over a central horizontal line.
		double totalWeight = 0.0;
		const int y = 512;
		for (int x = 0; x < int(_dstImg.w()); ++x) {
			const float val = _dstImg(x, y)[0];
			totalWeight += val;
		}
		// Variance computation.
		double sum = 0.0;
		double sum2 = 0.0;
		for (int x = 0; x < int(_dstImg.w()); ++x) {
			const float val = _dstImg(x, y)[0];
			const double shift = sibr::Vector2f(x, y).norm();
				
			sum += val * shift;
			sum2 += val * (shift * shift);
		}

		const double avg = sum / totalWeight;
		const double var = ((sum2 / totalWeight) - (avg * avg));

		// Fit visualisation.
		for (int x = 0; x <int( _dstImg.w()); ++x) {
			const float val = float(std::abs(x - 512));
			_fit[x] = float((1.0/(std::sqrt(2 * M_PI * var))) * exp(-0.5 * val * val / var));
		}

		return float(var);
	}

	void BRDFTestView::onRender(const Viewport & vpRender){
		
		vpRender.bind();
		vpRender.clear();
		
		// Display a 3D wireframe patch where the height is determined by the variance value.
		if (_show3D) {
			_varView.begin();
			_slice.send();
			_axis.send();
			_scales.send();
			_mvp.set(_handler.getCamera().viewproj());
			_mode2D.send();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_3D, _tex3D);
			_grid->render(false, false, _mode2D ? sibr::Mesh::FillRenderMode : sibr::Mesh::LineRenderMode);
			
			_varView.end(); 
			if (!_mode2D) {
				_gizmo.begin();
				glPointSize(25.0f);
				_mvpGiz.set(_handler.getCamera().viewproj());
				_axes->render(false, false, sibr::Mesh::PointRenderMode);
				_gizmo.end();
			}
			_handler.onRender(vpRender);
		}
		else {

			// Just render the footprint.
			_blit.begin();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _dst->handle());
			RenderUtility::renderScreenQuad();
			_blit.end();
		}
		

		CHECK_GL_ERROR;
	}

}