/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "RoughnessTestView.hpp"
#include <core/graphics/GUI.hpp>


namespace sibr {

	RoughnessTestView::RoughnessTestView(const std::string& path)
	{
		_blit.init("Blit", sibr::loadFile(
			sibr::getShadersDirectory("core") + "/texture.vert"),
			sibr::loadFile(sibr::getShadersDirectory("core") + "/texture.frag"));
		
		// Load the groundtruth.
		const auto files = sibr::listFiles(path, false, false, { "exr" });
		for (const auto& file : files) {
			if (file.find("mitsuba") == 0) {
				_gt.reset(new ImageRGB32F());
				_gt->load(path + "/" + file);
				_gtTex.reset(new Texture2DRGB32F(_gt));
			} else if (file.find("probe") == 0) {
				_probes.emplace_back();
				_probes.back().img.reset(new ImageRGB32F());
				_probes.back().img->load(path + "/" + file);
				_probes.back().name = sibr::removeExtension(file);
				_probes.back().tex.reset(new Texture2DRGB32F(_probes.back().img));
			}
		}
		updateVisuGT();

	}

	void RoughnessTestView::onUpdate(Input& input, const Viewport & vp) {
		
	}

	void blur(ImageRGB32F::Ptr & img, ImageRGB32F::Ptr & dst, float scale, float intensity) {
		const int radius = 6;
		const int shift = 2;
		float weights[7 * 7];
		weights[0] = 0.0123702f; weights[1] = 0.0119896f; weights[2] = 0.0109166f; weights[3] = 0.00933743f; weights[4] = 0.00750277f; weights[5] = 0.00566333f; weights[6] = 0.00401586f;
		weights[7] = 0.0119896f; weights[8] = 0.0116207f; weights[9] = 0.0105807f; weights[10] = 0.00905013f; weights[11] = 0.00727193f; weights[12] = 0.00548909f; weights[13] = 0.0038923f;
		weights[14] = 0.0109166f; weights[15] = 0.0105807f; weights[16] = 0.00963384f; weights[17] = 0.00824021f; weights[18] = 0.00662115f; weights[19] = 0.00499785f; weights[20] = 0.00354396f;
		weights[21] = 0.00933743f; weights[22] = 0.00905013f; weights[23] = 0.00824021f; weights[24] = 0.00704819f; weights[25] = 0.00566333f; weights[26] = 0.00427487f; weights[27] = 0.0030313f;
		weights[28] = 0.00750277f; weights[29] = 0.00727193f; weights[30] = 0.00662115f; weights[31] = 0.00566333f; weights[32] = 0.00455058f; weights[33] = 0.00343492f; weights[34] = 0.0024357f;
		weights[35] = 0.00566333f; weights[36] = 0.00548909f; weights[37] = 0.00499785f; weights[38] = 0.00427487f; weights[39] = 0.00343492f; weights[40] = 0.00259279f; weights[41] = 0.00183854f;
		weights[42] = 0.00401586f; weights[43] = 0.0038923f; weights[44] = 0.00354396f; weights[45] = 0.0030313f; weights[46] = 0.0024357f; weights[47] = 0.00183854f; weights[48] = 0.0013037f;

		const float stepScale = scale;
		dst.reset(new ImageRGB32F(img->w(), img->h()));
#pragma omp parallel for
		for (int y = 0; y < int(img->h()); ++y) {
			for (int x = 0; x < int(img->w()); ++x) {
				// We assume src and dst have the same res.
				const sibr::Vector2i coords(x, y);

				// Treat center aside.
				const sibr::Vector3f col = img(coords);

				sibr::Vector4f accum = weights[0] * col.homogeneous();

				// Read geometric info.

				float radiusScale = 1.0f;//1.0/max(0.01, refDepth)*
				float delta = shift;
				delta *= radiusScale;
				delta *= stepScale;
				// Start from center, ensuring decreasing weights.
				// Explore to the right/down, ensuring decreasing weights.
				for (int dy = -radius; dy <= radius; ++dy) {
					for (int dx = -radius; dx <= radius; ++dx) {
						const sibr::Vector2f newCoordsF = coords.cast<float>() + delta * sibr::Vector2f(dx, dy);
						// For now, discretize. might want to use textureLod later on.
						const sibr::Vector2i newCoords(std::round(newCoordsF[0]), std::round(newCoordsF[1]));

						const sibr::Vector3f ncol = img(sibr::clamp(newCoords[0], 0, int(img->w()) - 1), sibr::clamp(newCoords[1], 0, int(img->h()) - 1));
						float finalWeight = weights[abs(dy) * 7 + abs(dx)];
						accum += finalWeight * ncol.homogeneous();
					}
				}

				if (accum[3] != 0.0f) {

					dst(x, y) = intensity * accum.xyz() / accum[3];
				}
				else {
					dst(x, y) = { 0.0f, 0.0f,0.0f };
				}


			}
		}

	}


	void RoughnessTestView::updateVisuGT() {
		_gtCopy.reset(new ImageRGB32F(_gt->clone()));
		if (_row >= 0) {
			for (uint x = 0; x < _gtCopy->w(); ++x) {
				_gtCopy(x, _row) = sibr::Vector3f(0.0f, 0.0f, 0.0f);
			}
		}
		_gtTex.reset(new Texture2DRGB32F(_gtCopy));
	}

	void RoughnessTestView::updateValues() {
		_valuesGT.resize(_gt->w());

		_valuesProbe.resize(_gt->w());
		_valuesProbeProc.resize(_gt->w());
#pragma omp parallel for
		for (int i = 0; i < int(_gt->w()); ++i) {
			{
				const sibr::Vector3f& c = _gt(i, _row);
				_valuesGT.r[i] = c[0];
				_valuesGT.g[i] = c[1];
				_valuesGT.b[i] = c[2];
			}
			if (_probe > 0) {
				const sibr::Vector3f& c = _probes[_probe - 1].img(i, _row);
				_valuesProbe.r[i] = c[0];
				_valuesProbe.g[i] = c[1];
				_valuesProbe.b[i] = c[2];

			}
			else {
				_valuesProbe.r[i] = 0.0f;
				_valuesProbe.g[i] = 0.0f;
				_valuesProbe.b[i] = 0.0f;
			}
			if (_probe > 0 && _probes[_probe - 1].proc) {
				const sibr::Vector3f& c = _probes[_probe - 1].proc(i, _row);
				_valuesProbeProc.r[i] = c[0];
				_valuesProbeProc.g[i] = c[1];
				_valuesProbeProc.b[i] = c[2];
			}
			else {
				_valuesProbeProc.r[i] = 0.0f;
				_valuesProbeProc.g[i] = 0.0f;
				_valuesProbeProc.b[i] = 0.0f;
			}
		}

		_valuesGT.computeLuminance();
		_valuesProbe.computeLuminance();
		_valuesProbeProc.computeLuminance();
	}

	void RoughnessTestView::onGUI() {
		if (ImGui::Begin("Float viewer options")) {
			ImGui::RadioButton("GT", &_probe, 0);
			for (uint i = 0; i < _probes.size(); ++i) {
				if (ImGui::RadioButton(_probes[i].name.c_str(), &_probe, i + 1)) {
					updateValues();
				}
				if (_probes[i].proc) {
					ImGui::SameLine();
					ImGui::PushID(i);
					ImGui::Checkbox("Show blur", &_probes[i].showBlur);
					ImGui::PopID();
				}
			}
			ImGui::Separator();
			if (ImGui::SliderFloat("Scale radius", &_scale, 0.0f, 5.0f)) {
				if (_probe > 0) {
					auto& prob = _probes[_probe - 1];
					blur(prob.img, prob.proc, _scale, _intensity);
					prob.procTex.reset(new Texture2DRGB32F(prob.proc));
					updateValues();
				}
			}
			if (ImGui::SliderFloat("Intensity", &_intensity, 0.0f, 1.5f)) {
				if (_probe > 0) {
					auto& prob = _probes[_probe - 1];
					blur(prob.img, prob.proc, _scale, _intensity);
					prob.procTex.reset(new Texture2DRGB32F(prob.proc));
					updateValues();
				}
			}
			if (ImGui::Button("Blur current probe")) {
				if (_probe > 0) {
					auto& prob = _probes[_probe - 1];
					blur(prob.img, prob.proc, _scale, _intensity);
					prob.procTex.reset(new Texture2DRGB32F(prob.proc));
					updateValues();
				}
			}
			ImGui::Separator();
			if (ImGui::InputInt("Row", &_row)) {
				_row = sibr::clamp(_row, 0, int(_gt->h()));
				updateVisuGT();
			}
			if (ImGui::Button("Hide row")) {
				const int back = _row;
				_row = -1;
				updateVisuGT();
				_row = back;
			}
			if (ImGui::Button("Show line values")) {
				updateValues();
			}
		}
		ImGui::End();


		if (ImGui::Begin("Plot")) {
			static int mode = 0;
			ImGui::RadioButton("Lum##Plot", &mode, 0); ImGui::SameLine();
			ImGui::RadioButton("R##Plot", &mode, 1); ImGui::SameLine();
			ImGui::RadioButton("G##Plot", &mode, 2); ImGui::SameLine();
			ImGui::RadioButton("B##Plot", &mode, 3); ImGui::SameLine();
			ImGui::PushItemWidth(200);
			ImGui::DragFloat2("Range", &_minmax[0], 0.05f);
			ImGui::PopItemWidth();

			std::vector<float*> vals;
			if (mode == 0) {
				vals = { _valuesGT.l.data() , _valuesProbe.l.data(), _valuesProbeProc.l.data() };
			} else if (mode == 1) {
				vals = { _valuesGT.r.data() , _valuesProbe.r.data(), _valuesProbeProc.r.data() };
			} else if (mode == 2) {
				vals = { _valuesGT.g.data() , _valuesProbe.g.data(), _valuesProbeProc.g.data() };
			} else if (mode == 3) {
				vals = { _valuesGT.b.data() , _valuesProbe.b.data(), _valuesProbeProc.b.data() };
			}
			ImGui::PlotMultiLines("Plot", vals, int(_valuesGT.l.size()),
				{ ImVec4(0.0, 1.0, 0.0, 1.0), ImVec4(1.0, 0.0, 0.0, 1.0), ImVec4(1.0, 1.0, 0.0, 1.0) },
				_minmax[0], _minmax[1] , ImVec2(ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 60));
		}
		ImGui::End();
	}



	
	void RoughnessTestView::onRender(const Viewport & vpRender){
		if (!_gt) {
			return;
		}

		vpRender.bind();
		vpRender.clear();
		
		_blit.begin();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _gtTex->handle());
		
		if (_probe > 0) {
			auto& tex = (_probes[_probe - 1].showBlur ? _probes[_probe - 1].procTex  : _probes[_probe - 1].tex);
			glBindTexture(GL_TEXTURE_2D, tex->handle());
		}
		RenderUtility::renderScreenQuad();
		_blit.end();
		CHECK_GL_ERROR;
	}

}