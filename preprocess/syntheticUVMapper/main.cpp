/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/system/Config.hpp>
#include <core/graphics/Mesh.hpp>
#include <core/assets/UVUnwrapper.hpp>
#include "core/system/CommandLineArgs.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "projects/synthetic_ibr/renderer/MeshOBJExporter.hpp"


using namespace sibr;

struct SyntheticUVMapperArgs : public AppArgs {
	RequiredArg<std::string> path = { "path", "path to the OBJ mesh" };
	Arg<std::string> output = { "output", "", "path to the output obj mesh" };
	Arg<int> size = { "size", 4096, "target UV map width (approx.)" };
	Arg<bool> visu = { "visu", "save visualisation" };
};

/** Unwrap a synthetic mesh, keeping track of material and vertices remapping. */
int main(int ac, char ** av){

	CommandLineArgs::parseMainArgs(ac, av);
	SyntheticUVMapperArgs args;
	std::string outputFile = args.output;
	if(outputFile.empty()) {
		outputFile = sibr::removeExtension(args.path.get()) + "_output.obj";
	}
	const std::string addOutputFile = sibr::removeExtension(outputFile) + "_ids.bin";
	const std::string refOutputFile = sibr::removeExtension(outputFile) + "_ref.bin";
	sibr::makeDirectory(sibr::parentDirectory(outputFile));

	// Load object file.
	MaterialMesh mesh(false);
	if(sibr::getExtension(args.path) == "xml") {
		mesh.loadMtsXML(args.path);
	} else {
		mesh.load(args.path);
	}

	// Build material ID buffer.
	std::vector<uint> ids(mesh.vertices().size());
	const auto & matIds = mesh.matIds();
	const auto & meshTris = mesh.triangles();
	for (unsigned int i = 0; i < matIds.size(); i++){
		const uint matId = uint(matIds.at(i) + 1);
		ids.at(meshTris.at(i)[0]) = matId;
		ids.at(meshTris.at(i)[1]) = matId;
		ids.at(meshTris.at(i)[2]) = matId;
	}
	// Unwrap.
	UVUnwrapper unwrapper(mesh, uint32_t(args.size));
	auto finalMesh = unwrapper.unwrap();

	// Save mesh.
	const std::string header = "mtllib common_lightmap.mtl\nusemtl common_lightmap\n";
	MeshOBJHandler::saveOBJ(*finalMesh, outputFile, header);
	
	// Output a mtl file.
	std::ofstream mtlFile(sibr::parentDirectory(outputFile) + "/common_lightmap.mtl");
	if(!mtlFile.is_open()) {
		SIBR_WRG << "Unable to create stub mtl file." << std::endl;
	} else {
		mtlFile << "newmtl common_lightmap\nKa 0.0 0.0 0.0\nKd 1.0 1.0 1.0\nKs 0.0 0.0 0.0\nTr 0.0\nillum 0\nNs 0.000000\nmap_Kd scene_light_map_gamma.png\n" << std::endl;
		mtlFile.close();
	}

	// Write material IDs and mapping.
	const auto& mapping = unwrapper.mapping();
	const long idCount = long(mapping.size());
	std::vector<uint> materials(idCount);
	#pragma omp parallel for
	for (long mid = 0; mid < idCount; ++mid) {
		materials[mid] = ids[mapping[mid]];
	}
	// OBJ doesn't store colors/material ids.
	ByteStream addData;
	addData.push(materials.data(), uint(materials.size()*sizeof(uint)));
	addData.saveToFile(addOutputFile);
	ByteStream refData;
	refData.push(mapping.data(), uint(mapping.size() * sizeof(uint)));
	refData.saveToFile(refOutputFile);
	
	// Output debug vis.
	if (args.visu) {
		const std::string baseName = sibr::removeExtension(outputFile);
		const auto visuImgs = unwrapper.atlasVisualization();
		for (uint32_t i = 0; i < visuImgs.size(); i++) {
			const std::string fileName = baseName + "_charts_atlas_" + std::to_string(i) + ".png";
			visuImgs[i]->save(fileName);
		}
	}
	return EXIT_SUCCESS;
}
