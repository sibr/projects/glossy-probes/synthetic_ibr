/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "BilateralFilterRenderer.hpp"

using namespace sibr;

BilateralFilterRenderer::BilateralFilterRenderer(uint w, uint h, float sceneScale, float roughnessScale, const std::string & lutPath) {
	resize(w, h);
	loadShaders();

	_sceneScale = sceneScale;
	_roughnessScale = roughnessScale;
	prepareLookupTable(lutPath);
}

void BilateralFilterRenderer::prepareLookupTable(const std::string & path) {
	ByteStream ifile;
	ifile.load(path);
	int res;
	ifile >> res;
	ifile >> _mins.get()[0] >> _maxs.get()[0];
	ifile >> _mins.get()[1] >> _maxs.get()[1];
	ifile >> _mins.get()[2] >> _maxs.get()[2];
	std::vector<float> values(res * res * res);
	for (int dx = 0; dx < res * res * res; ++dx) {
		ifile >> values[dx];
	}


	SIBR_LOG << "[BRDF Blur]: " << "Loaded LUT of res " << res << ", mins: " << _mins.get() << ", maxs: " << _maxs.get() << "." << std::endl;

	glGenTextures(1, &_tex3D);
	glBindTexture(GL_TEXTURE_3D, _tex3D);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	CHECK_GL_ERROR;
	glTexImage3D(GL_TEXTURE_3D,
		0,
		GL_R32F,
		res, res, res, 0,
		GL_RED,
		GL_FLOAT,
		values.data()
	);
	CHECK_GL_ERROR;
	glBindTexture(GL_TEXTURE_3D, 0);
}

void BilateralFilterRenderer::process(const InputCamera& eye, const IRenderTarget& src, const IRenderTarget& gbuffer, const RenderTargetRGBA32F::Ptr& reflectedPos, IRenderTarget& dst) {
	if (dst.w() != _ping->w() || dst.h() != _ping->h()) {
		resize(dst.w(), dst.h());
	}
	_sizePixel = 2.f * std::tan(eye.fovy() / 2.f) / float(eye.h());

	glViewport(0, 0, dst.w(), dst.h());

	// First compute per pixel stddevs and principal directions, and extract bilateral weights.
	_params->clear();
	_params->bind();
	_prepass.begin();

	_camPos.set(eye.position());
	_camDir.set(eye.dir());
	_camUp.set(eye.up());
	_camRight.set(eye.right());
	_camMVP.set(eye.viewproj().transpose().inverse());
	_mins.send();
	_maxs.send();
	_sizePixel.send();
	_roughnessScale.send();
	_sceneScale.send();

	// Gbuffer.
	// layout(location = 0) out vec4 outDirMat;
	// layout(location = 1) out vec4 outPosConf;
	// layout(location = 2) out vec4 normalSmoothMat;
	// curvatures...
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gbuffer.handle(1));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gbuffer.handle(2));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, reflectedPos->handle(0));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_3D, _tex3D);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, src.handle(2));

	RenderUtility::renderScreenQuad();
	_prepass.end();
	_params->unbind();

	_filter.begin();

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _params->handle(0));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _params->handle(1));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _params->handle(2));

	// Then two two-directional passes.
	const int mStep = 2 * 2;
	for (int it = 0; it < mStep; ++it) {
		_filterPrimaryDir.set(it % 2 == 0);
		_filterUseConfidence.set(it == 0);
		// Render directly to the dst at the last step.
		if (it == (mStep - 1)) {
			dst.bind();
		} else {
			_pong->bind();
		}
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, it == 0 ? src.handle() : _ping->handle());
		RenderUtility::renderScreenQuad();
		if (it == (mStep - 1)) {
			dst.unbind();
		} else {
			_pong->unbind();
			std::swap(_ping, _pong);
		}
	}
	_filter.end();
}

void BilateralFilterRenderer::loadShaders() {
	// When reloading, relying on another renderer to copy all the shaders from src to bin.
	const std::string shaderDstPath = sibr::getShadersDirectory("synthetic_ibr") + "/";
	
	_prepass.init("Bilateral prepass",
		 sibr::loadFile(shaderDstPath + "synthetic_texture.vert"),
		 sibr::loadFile(shaderDstPath + "bilateral_filter_prepass.frag"));
	_camMVP.init(_prepass, "camNorMVP");
	_camPos.init(_prepass, "camPos");
	_camDir.init(_prepass, "camDir");
	_camUp.init(_prepass, "camUp");
	_camRight.init(_prepass, "camRight");
	_sceneScale.init(_prepass, "sceneScale");
	_mins.init(_prepass, "minsLUT");
	_maxs.init(_prepass, "maxsLUT");
	_sizePixel.init(_prepass, "sizePixelWorldNovel");
	_roughnessScale.init(_prepass, "roughnessScale");


	_filter.init("Bilateral pass",
		sibr::loadFile(shaderDstPath + "synthetic_texture.vert"),
		sibr::loadFile(shaderDstPath + "bilateral_filter_separable.frag"));
	_filterPrimaryDir.init(_filter, "primaryDirection");
	_filterUseConfidence.init(_filter, "useConfidence");

}

void BilateralFilterRenderer::resize(uint w, uint h) {
	_ping.reset(new RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_pong.reset(new RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_params.reset(new RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 3));
}