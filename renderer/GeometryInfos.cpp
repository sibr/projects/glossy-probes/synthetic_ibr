/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "GeometryInfos.hpp"
#include "core/raycaster/CameraRaycaster.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "MeshOBJExporter.hpp"


namespace sibr {
	
	GeometryInfos::GeometryInfos(const std::vector<sibr::Vector3f>& positions, const std::vector<sibr::Vector3f>& normals, const std::vector<sibr::Vector2f>& texcoords, 
		const std::vector<sibr::Vector4f>& mincs, const std::vector<sibr::Vector4f>& maxcs, const std::vector<uint>& mats, const std::vector<sibr::Vector3u>& tris) {
		std::vector<float> nmats(mats.size());
#pragma omp parallel for
		for(long tid = 0; tid < mats.size(); ++tid) {
			nmats[tid] = float(mats[tid]);
		}

		// Mesh setup.
		{
			glGenVertexArrays(1, &_vaoId);
			glGenBuffers(2, &_bufferIds[0]);
			glBindVertexArray(_vaoId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bufferIds[0]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(uint) * tris.size(), tris.data(), GL_STATIC_DRAW);
			_indexCount = (uint)(3*tris.size());

			const size_t vertexSize = sizeof(float) * positions.size() * 3;
			const size_t uvSize = sizeof(float) * texcoords.size() * 2;
			const size_t normalSize = sizeof(float) * normals.size() * 3;
			const size_t mincSize = sizeof(float) * mincs.size() * 4;
			const size_t maxcSize = sizeof(float) * maxcs.size() * 4;
			const size_t matSize = sizeof(float) * nmats.size();
			const size_t totalSize = vertexSize + normalSize + matSize + mincSize + maxcSize + uvSize;

			const size_t vertexOff = 0;
			const size_t uvOff = vertexSize;
			const size_t normalOff = vertexSize + uvSize;
			const size_t mincOff = vertexSize + uvSize + normalSize;
			const size_t maxcOff = vertexSize + uvSize + normalSize + mincSize;
			const size_t matOff = vertexSize + uvSize + normalSize + mincSize + maxcSize;
			
			glBindBuffer(GL_ARRAY_BUFFER, _bufferIds[1]);
			glBufferData(GL_ARRAY_BUFFER, totalSize, nullptr, GL_STATIC_DRAW);
			glBufferSubData(GL_ARRAY_BUFFER, vertexOff, vertexSize, positions.data());
			glBufferSubData(GL_ARRAY_BUFFER, uvOff, uvSize, texcoords.data());
			glBufferSubData(GL_ARRAY_BUFFER, normalOff, normalSize, normals.data());
			glBufferSubData(GL_ARRAY_BUFFER, mincOff, mincSize, mincs.data());
			glBufferSubData(GL_ARRAY_BUFFER, maxcOff, maxcSize, maxcs.data());
			glBufferSubData(GL_ARRAY_BUFFER, matOff, matSize, nmats.data());
			CHECK_GL_ERROR;
			// The ordering is weird.
			glVertexAttribPointer(VertexAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + vertexOff);
			glEnableVertexAttribArray(VertexAttribLocation);

			glVertexAttribPointer(TexCoordAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + uvOff);
			glEnableVertexAttribArray(TexCoordAttribLocation);
			
			glVertexAttribPointer(NormalAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + normalOff);
			glEnableVertexAttribArray(NormalAttribLocation);
			
			glVertexAttribPointer(MinCurvAttribLocation, 4, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + mincOff);
			glEnableVertexAttribArray(MinCurvAttribLocation);

			glVertexAttribPointer(MaxCurvAttribLocation, 4, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + maxcOff);
			glEnableVertexAttribArray(MaxCurvAttribLocation);

			glVertexAttribPointer(MaterialAttribLocation, 1, GL_FLOAT, GL_FALSE, 0, (uint8_t*)(0) + matOff);
			glEnableVertexAttribArray(MaterialAttribLocation);
			
			glBindVertexArray(0);
		}
		
		// SSBO setup
		{
			// We pad everything, just in case.
			const size_t pCount = positions.size();
			const size_t tCount = tris.size();

			std::vector<sibr::Vector4f> padPositions(pCount);
			std::vector<sibr::Vector4f> padNorms(pCount);
			std::vector<sibr::Vector4u> padTris(tCount);

#pragma omp parallel for
			for (long i = 0; i < long(pCount); ++i) {
				padPositions[i] = positions[i].homogeneous();
				padNorms[i] = normals[i].homogeneous();
			}
#pragma omp parallel for
			for (long i = 0; i < long(tCount); ++i) {
				padTris[i] = tris[i].homogeneous();
			}

			glGenBuffers(1, &posBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, posBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, positions.size() * sizeof(sibr::Vector4f), padPositions.data(), GL_STATIC_READ);

			glGenBuffers(1, &mincBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, mincBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, mincs.size() * sizeof(sibr::Vector4f), mincs.data(), GL_STATIC_READ);

			glGenBuffers(1, &maxcBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, maxcBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, maxcs.size() * sizeof(sibr::Vector4f), maxcs.data(), GL_STATIC_READ);

			glGenBuffers(1, &matBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, matBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, mats.size() * sizeof(uint), mats.data(), GL_STATIC_READ);

			glGenBuffers(1, &triBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, triBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, tris.size() * sizeof(sibr::Vector4u), padTris.data(), GL_STATIC_READ);

			glGenBuffers(1, &norBuff);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, norBuff);
			glBufferData(GL_SHADER_STORAGE_BUFFER, normals.size() * sizeof(sibr::Vector4f), padNorms.data(), GL_STATIC_READ);

			glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		}
	}

	void GeometryInfos::bind(size_t startLoc) {
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, posBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc), posBuff);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, mincBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 1), mincBuff);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, maxcBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 2), maxcBuff);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, matBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 3), matBuff);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, triBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 4), triBuff);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, norBuff);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 5), norBuff);
	}

	void GeometryInfos::unbind(size_t startLoc) {
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc), 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 1), 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 2), 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 3), 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 4), 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, GLuint(startLoc + 5), 0);
	}


	void  GeometryInfos::draw() const
	{
		glBindVertexArray(_vaoId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bufferIds[0]);
		glDrawElements(GL_TRIANGLES, _indexCount, GL_UNSIGNED_INT, (void*)0);
		glBindVertexArray(0);
	}

}
