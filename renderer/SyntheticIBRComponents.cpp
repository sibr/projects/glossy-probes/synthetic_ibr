/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SyntheticIBRComponents.hpp"
#include "core/system/String.hpp"


namespace sibr {

	namespace syntheticIBR {

		void tonemap(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg, float exposure, bool gamma) {
			const cv::Mat tonemaped = hdrImg.toOpenCV();
			cv::Mat tonemaped2;
			const cv::Mat exposed = -exposure * tonemaped;
			cv::exp(exposed, tonemaped2);
			tonemaped2 = cv::Scalar(1.0f, 1.0f, 1.0f) - tonemaped2;
			if (gamma) {
				cv::pow(tonemaped2, 1.0 / 2.2, tonemaped2);
			}
			cv::Mat tonemapedRGB;
			tonemaped2.convertTo(tonemapedRGB, CV_8UC3, 255.0f);
			ldrImg.fromOpenCV(tonemapedRGB);
		}

		void convert(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg) {
			ldrImg = ImageRGB(hdrImg.w(), hdrImg.h());
#pragma omp parallel for
			for(int y = 0; y < int(hdrImg.h()); ++y) {
				for (int x = 0; x < int(hdrImg.w()); ++x) {
					const sibr::Vector3f & col = hdrImg(x,y);
					ldrImg(x, y)[0] = uchar(sibr::clamp(255.0f * col[0], 0.0f, 255.0f));
					ldrImg(x, y)[1] = uchar(sibr::clamp(255.0f * col[1], 0.0f, 255.0f));
					ldrImg(x, y)[2] = uchar(sibr::clamp(255.0f * col[2], 0.0f, 255.0f));
				}
			}
		}

		void gamma(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg, float gamma) {
			ldrImg = ImageRGB(hdrImg.w(), hdrImg.h());
#pragma omp parallel for
			for (int y = 0; y < int(hdrImg.h()); ++y) {
				for (int x = 0; x < int(hdrImg.w()); ++x) {
					const sibr::Vector3f & col = hdrImg(x, y);
					ldrImg(x, y)[0] = uchar(sibr::clamp(255.0f * std::pow(col[0], gamma), 0.0f, 255.0f));
					ldrImg(x, y)[1] = uchar(sibr::clamp(255.0f * std::pow(col[1], gamma), 0.0f, 255.0f));
					ldrImg(x, y)[2] = uchar(sibr::clamp(255.0f * std::pow(col[2], gamma), 0.0f, 255.0f));
				}
			}
		}

		sibr::Vector3f pixelToDirWarped(const sibr::Vector2f & pix, const sibr::Vector2i & res, const ImageFloat2 & warpMap) {
			// Normalized coords.
			const sibr::Vector2f sampleCoord = pix.cwiseQuotient(res.cast<float>());
			const sibr::Vector2f lookup = sampleCoord.cwiseProduct(warpMap.size().cast<float>() - sibr::Vector2f(1.0f, 1.0f));
			const sibr::Vector2f pixelSampleNew = warpMap.bilinear(lookup).cwiseProduct(res.cast<float>());

			const double phi = pixelSampleNew.x() / float(res.x()) * 2.0 * M_PI;
			const double theta = pixelSampleNew.y() / float(res.y()) * M_PI;
			const double cPhi = std::cos(phi);
			const double sPhi = std::sin(phi);
			const double cTheta = std::cos(theta);
			const double sTheta = std::sin(theta);
			return { float(sPhi*sTheta), float(cTheta), -float(cPhi * sTheta) };
		}

		float loadScale(const std::string & basePath) {
			const std::string scalePath = basePath + "/params.txt";
			std::ifstream file(scalePath);;
			if (!file.is_open()) {
				SIBR_ERR << "Unable to load scale from params.txt file. Specify how many scene units are in 1m." << std::endl;
				return 1.0f;
			}
			float scale;
			file >> scale;
			file.close();
			return scale;
		}

	}

	

	void ProbeLocations::setupFromPath(const std::string & probesPath)
	{
		const std::string fileStr = sibr::loadFile(probesPath);
		const std::vector<std::string> posList = sibr::split(fileStr);
		_locations.reserve(posList.size());
		for(const auto & pos : posList) {
			if(pos.empty()) {
				continue;
			}
			sibr::Vector3f p;
			std::stringstream sst(pos);
			sst >> p[0] >> p[1] >> p[2];
			_locations.emplace_back(p);
		}
	}
	

}
