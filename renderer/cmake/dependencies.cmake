# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


###################
## Find OpenMesh
###################
sibr_addlibrary(
    NAME OpenMeshCore
    MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/OpenMesh-8.0.7z"
    MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/OpenMesh-8.0.7z"     # TODO SV: provide a valid version if required
    SET CHECK_CACHED_VAR OpenMesh_DIR PATH "openmesh 8.0"
)