/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "ProbesView.hpp"
#include <core/graphics/GUI.hpp>

extern "C" char embedded_ptx_code_refl[];

sibr::ProbesView::ProbesView(const sibr::ProbesGatherScene::Ptr& scene, uint w, uint h, const std::string& lutPath, bool supersampleDiffuse) :
	sibr::ViewBase(w,h)
{
	_ssdiff = supersampleDiffuse;
	_scene = scene;

	// Setup all renderers.
	_specRenderer = std::make_shared<GatherProbesRenderer>(scene->probes());
	_bilateralFilter = std::make_shared<BilateralFilterRenderer>(w, h, _scene->scale(), 0.5f, lutPath);

	// Basic shaders.
	_diffuseMesh.init("Textured diffuse", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_deferred_prepass.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_deferred_prepass.frag"));
	_diffMVP.init(_diffuseMesh, "mvp");

	_compositingShader.init("Compositing", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_blendcommon.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/basic_composite.frag"));
	_compositingMode.init(_compositingShader, "mode");
	_exposure.init(_compositingShader, "exposure");
	_clampToLDR.init(_compositingShader, "clampToLDR");
	_clampToLDR = true;
	_exposure = 1.0f;

	_fxaaShader.init("FXAA", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_texture.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_fxaa.frag"));
	_invSize.init(_fxaaShader, "inverseScreenSize");

	resizeForNeighbors(_numNeighs);

	// Adjust for proper scene scale.
	_specRenderer->debug().thresholds = { 20.0f*0.2f * _scene->scale(), 100000.0f * _scene->scale(), 1.0f, 1.0f };
	_specRenderer->sceneScale() = _scene->scale();

	// Setup the Optix raycaster.
	_raycaster.reset(new OptixRaycaster());
	OptixRaycaster::Options rayOpts;
	rayOpts.entryName = "__raygen__renderReflectedFrame";
	rayOpts.anyName = "__anyhit__reflpos";
	rayOpts.closestName = "__closesthit__reflpos";
	rayOpts.missName = "__miss__reflpos";
	rayOpts.ptxCode = std::string(embedded_ptx_code_refl);
	rayOpts.debug = false;
	_raycaster->createPrograms(rayOpts);
	_raycaster->createScene(_scene->proxies()->proxy());
	_raycaster->bindProgramsAndScene();

	resize(w,h);

	const auto& locations = _scene->probes()->locations();
	for (int i = 0; i < locations.size(); i++)
	{
		_probesBBox.extend(locations[i]);
	}
}

void sibr::ProbesView::resize(uint w, uint h) {
	SIBR_LOG << "[ProbesView] Triggering a resize." << std::endl;
	// We first trigger a spec renderer resize.
	// This will invalidate the textures registered by the optix raycaster.
	_specRenderer->resize(w,h);
	_raycaster->registerSource(*_specRenderer->prepassRT(), 1);
	_raycaster->registerDirection(*_specRenderer->prepassRT(), 0);
	_reflectedPos.reset(new sibr::RenderTargetRGBA32F(w,h,0,1));
	_raycaster->registerDestination(_reflectedPos);
	_specularRT.reset(new sibr::RenderTargetRGBA32F(w,h, SIBR_GPU_LINEAR_SAMPLING, 3));
	_diffuseRT.reset(new sibr::RenderTargetRGB((_ssdiff ? 2 : 1)*w, (_ssdiff ? 2 : 1) *h, SIBR_GPU_LINEAR_SAMPLING));
	_fxaaRT.reset( new RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_invSize = sibr::Vector2f( 1.0f/float(w), 1.0f/float(h));
}

void sibr::ProbesView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
{
	if(dst.w() != _specularRT->w() || dst.h() != _specularRT->h()) {
		resize(dst.w(), dst.h());
	}

	// Fill G-buffer.
	_gbufferTime.begin();
	if ((_layerMode & (SyntheticLayer::DIFFUSE | SyntheticLayer::SPECULAR))) {
		
		// Render the (supersampled) diffuse separately.
		_diffuseRT->clear();
		_diffuseRT->bind();
		glViewport(0, 0, _diffuseRT->w(), _diffuseRT->h());

		_diffuseMesh.begin();
		_diffMVP.set(eye.viewproj());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _scene->lightMap()->handle());
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		_scene->geometry()->draw();
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDepthFunc(GL_LESS);
		_diffuseMesh.end();
		_diffuseRT->unbind();
		CHECK_GL_ERROR;

		// Then generate the regular g-buffer with geoemtric and material data.
		_specRenderer->processGbuffer(eye, _scene->geometry(), _scene->lightMap());
	}
	_gbufferTime.end();

	
	if (_layerMode & SyntheticLayer::SPECULAR) {
		// Select probes.
		_selecTime.begin();
		selectProbes(eye);
		_selecTime.end();
		
		// Run the raycaster first.
		// It takes the reflector position adn reflected position and find the reflected positions and infos in the novel view.
		_optixTime.begin();
		_raycaster->onRender();
		_optixTime.end();

		// Gather specular effects.
		glViewport(0, 0, _specularRT->w(), _specularRT->h());
		_gatherTime.begin();
		_specRenderer->process(eye, _indices, _coords, *_specularRT,
			_scene->colorMaps(), _scene->reflectedMaps(), _scene->warpMaps(),_scene->diffWarpMaps(),  
			_scene->probes(), _scene->geometry(), _scene->geomMaps(), _reflectedPos);
		_gatherTime.end();

		// Glossiness reconstruction filter.
		_filterTime.begin();
		if (_blurMode) {
			const InputCamera cam(eye, dst.w(), dst.h());
			_bilateralFilter->process(cam, *_specularRT, *_specRenderer->prepassRT(), _reflectedPos, *_specularRT);
		}
		_filterTime.end();

		// Apply FXAA on the specular layer.
		if (_fxaa) {
			_fxaaRT->bind();
			glViewport(0, 0, _fxaaRT->w(), _fxaaRT->h());
			_fxaaShader.begin();
			_invSize.send();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _specularRT->handle(0));
			RenderUtility::renderScreenQuad();
			_fxaaShader.end();
			_fxaaRT->unbind();
			sibr::blit(*_fxaaRT, *_specularRT);
		}
	}

	// Display result:
	_compositeTime.begin();
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	glDisable(GL_DEPTH_TEST);
	_compositingShader.begin();
	_clampToLDR.send();
	_exposure.send();
	// Composite  diffuse and specular layers.
	_compositingMode.set(int(_layerMode));
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _diffuseRT->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _specularRT->handle());
		
	RenderUtility::renderScreenQuad();
	_compositingShader.end();

	dst.unbind();
	_compositeTime.end();

	glDisable(GL_BLEND);
	CHECK_GL_ERROR;

	if (_monitoring) {
		_frameTimes.push_back(ImGui::GetIO().DeltaTime * 1000.0f);
		_gbufferTimes.push_back(float(_gbufferTime.value()) / 1000000.0f);
		_optixTimes.push_back(float(_optixTime.value()) / 1000000.0f);
		_gatherTimes.push_back(float(_gatherTime.value()) / 1000000.0f);
		_filterTimes.push_back(float(_filterTime.value()) / 1000000.0f);
		_compositeTimes.push_back(float(_compositeTime.value()) / 1000000.0f);
	}

	// Visualisation in the debug view.
	if (_debugView) {
		renderVisualisation();
	}
}

void sibr::ProbesView::resizeForNeighbors(size_t value) {
	if(_coords.size() == value) {
		return;
	}
	_coords.resize(value);
	_indices.resize(value);
}

void sibr::ProbesView::renderVisualisation() {
	const std::vector<sibr::Vector3f> & probes = _scene->probes()->locations();
	std::vector<sibr::Vector3f> probesSelectedPos;
	std::vector<sibr::Vector3f> probesHighlightPos;
	for (int i = 0; i < _indices.size(); ++i) {

		if (_specRenderer->debug().probeLocalId != -1 && _specRenderer->debug().probeLocalId != i) {
			continue;
		}

		if(_indices[i] < 0) {
			continue;
		}
		probesSelectedPos.emplace_back(probes[_indices[i]]);
		
		probesHighlightPos.emplace_back(probes[_indices[i]]);
	}

	_debugView->addMeshAsLines("Scene cam", generateCamFrustum(_handler->getCamera(), 0.0f, _cameraScale)).setColor({ 1,0,0 });
	_debugView->addPoints("Probes (selected)", probesHighlightPos, { 1.0f, 0.5f, 0.0f }).setDepthTest(true);
	_debugView->addPoints("Probes (all)", probes, { 0.0f, 0.5f, 1.0f }).setDepthTest(true);

	{
		std::vector<sibr::Vector3f> lines;
		for (size_t pid = 0; pid < probes.size(); ++pid) {
			for (size_t opid = pid+1; opid < probes.size(); ++opid) {
				const sibr::Vector3f& p0 = probes[pid];
				const sibr::Vector3f& p1 = probes[opid];
				const int r0 = int(p0[0] == p1[0]);
				const int r1 = int(p0[1] == p1[1]);
				const int r2 = int(p0[2] == p1[2]);
				if (r0 + r1 + r2 > 1) {
					lines.push_back(p0);
					lines.push_back(p1);
				}
			}
		}
		_debugView->addLines("Probes (meshes)", lines, { 0.0f, 0.5f, 1.0f }).setDepthTest(true);

	}
}

void sibr::ProbesView::onGUI()
{

	// Panel to record and export key cameras that can then be inerpolated in the camera editor.
	if (!_pathCams.empty()) {
		if (ImGui::Begin("Synthetic path")) {
			ImGui::Text("Cameras: %d", _pathCams.size());
			if (ImGui::Button("Remove camera")) {
				_pathCams.pop_back();
			}
			if (ImGui::Button("Save path...")) {
				std::string outpath;
				if (sibr::showFilePicker(outpath, Save, "", "lookat") && !outpath.empty()) {
					InputCamera::saveAsLookat(_pathCams, outpath);
				}
			}
		}
		ImGui::End();
	}

	// Show debug parameters.
	if (_showParams) {
		if (ImGui::Begin("Synthetic extra params")) {
			ImGui::SliderFloat("surfTh", &_specRenderer->debug().surfTh.get(), 0.0f, 2.0f);
			ImGui::SliderFloat("maxPosError", &_specRenderer->debug().maxPosError.get(), 0.0f, 5.0f);
			ImGui::SliderFloat("surfAdj", &_specRenderer->debug().surfAdj.get(), 0.0f, 5.0f);
			ImGui::SliderFloat("posAdj", &_specRenderer->debug().posAdj.get(), 0.0f, 10.0f);
			ImGui::SliderFloat("lobeAdj", &_specRenderer->debug().lobeAdj.get(), 0.0f, 5.0f);
			ImGui::SliderFloat("errorScale", &_specRenderer->debug().errorScale.get(), 10.0f, 50.0f);
			ImGui::SliderFloat("refmatPenal", &_specRenderer->debug().refmatPenal.get(), 5.0f, 25.0f);
		}
		ImGui::End();
	}
	
	if (ImGui::Begin("Synthetic")) {
		ImGui::RadioButton("Diffuse", (int*)&_layerMode, 1); ImGui::SameLine();
		ImGui::RadioButton("Specular", (int*)&_layerMode, 2); ImGui::SameLine();
		ImGui::RadioButton("Both", (int*)&_layerMode, 3); ImGui::SameLine();
		
		ImGui::Checkbox("Roughness reconstruction", &_blurMode);
		ImGui::Checkbox("FXAA", &_fxaa);
		ImGui::Checkbox("Show params", &_showParams);
		ImGui::Checkbox("Show RTs", &_showDebugRTs);
		
		ImGui::Checkbox("Clamp to LDR", &_clampToLDR.get());
		if(!_clampToLDR) {
			ImGui::SameLine();
			ImGui::SliderFloat("Exposure", &_exposure.get(), -5.0f, 5.0f);
		}

		if (ImGui::SliderFloat3("Step scaling", &_specRenderer->debug().scaleSteps.get()[0], 0.1f, 10.0f)) {
			_specRenderer->debug().scaleSteps = _specRenderer->debug().scaleSteps.get().cwiseMax(sibr::Vector3f{ 0.1f, 0.1f, 0.1f });
		}
		if (ImGui::SliderInt3("Radii", &_specRenderer->radii()[0], 0, 16)) {
			_specRenderer->loadShaders(true);
		}
	
		ImGui::Separator();

		// Probes selection method.
		if(ImGui::Combo("Selection", (int*)&_selectionMode, "Trilinear\0Force one\0Angular\0Distance\0\0")) {
			if (_selectionMode == FORCE_ONE) {
				resizeForNeighbors(1);
			} else if (_selectionMode == TRILINEAR) {
				_numNeighs = 8;
				resizeForNeighbors(8);
			} else {
				resizeForNeighbors(_numNeighs);
				_specRenderer->debug().probeLocalId = -1;
			}
		}
		if(_selectionMode != FORCE_ONE) {
			if(ImGui::InputInt("Num. neighbors", &_numNeighs, 1)) {
				_numNeighs = sibr::clamp(_numNeighs, 1, int(_scene->probes()->locations().size()));
				resizeForNeighbors(_numNeighs);
			}
		}
		ImGui::InputInt("Every N", &_probeStep, 1);
		// Display a specific probe.
		if(_selectionMode == FORCE_ONE) {
			if(ImGui::InputInt("Show probe", &_forcedId)) {
				_forcedId = sibr::clamp(_forcedId, 0, int(_scene->probes()->locations().size()));
			}
		} else {
			if (ImGui::InputInt("Show local probe", &_specRenderer->debug().probeLocalId)) {
					_specRenderer->debug().probeLocalId = sibr::clamp(_specRenderer->debug().probeLocalId, -1, _numNeighs - 1);
			}
		}
		
		if (ImGui::Button("Snap to closest")) {
			snapTo(-1);
		}
		ImGui::SameLine();
		if (ImGui::InputInt("Snap to", &_snapId, 1, 10)) {
			snapTo(_snapId);
		}

		// Save HDR image of the specular layer.
		if (ImGui::Button("Save specular")) {
			std::string outputPath;
			if (sibr::showFilePicker(outputPath, FilePickerMode::Save, "", "exr") && !outputPath.empty()) {
				ImageRGB32F img(_specularRT->w(), _specularRT->h());
				_specularRT->readBack(img);
				if (outputPath.substr(outputPath.size() - 4, 4) != ".exr") {
					outputPath.append(".exr");
				}
				img.saveHDR(outputPath);
			}
			
		}

		if (ImGui::Button("Reset camera")) {
			InputCamera newCam(_handler->getCamera());
			newCam.setLookAt({ 0.0f,0.0f,0.0f }, {0.0f,0.0f,1.0f}, {0.0f,1.0f,0.0f});
			newCam.fovy(45.0f/180.0f*float(M_PI));
			_handler->fromCamera(newCam);
		}
		
		ImGui::Separator();

		if(ImGui::InputFloat("Camera scale", &_cameraScale, 0.1f, 10.0f)) {
			_cameraScale = std::max(0.001f, _cameraScale);
		}
		ImGui::PopItemWidth();

		if (ImGui::Button("Print Mitsuba rendering command")) {
			const InputCamera & cam = _handler->getCamera();
			const sibr::Vector3f o = cam.position();
			const sibr::Vector3f t = cam.position() + cam.dir();
			const sibr::Vector3f u = cam.up();
			std::stringstream command;
			command << "./mitsuba.exe -o " << _scene->basePath() << "/save/" << _scene->name() << "/mitsuba_" << sibr::timestamp() << ".exr ";
			command << "-D camOrigin=" << o[0] << "," << o[1] << "," << o[2] << " ";
			command << "-D camTarget=" << t[0] << "," << t[1] << "," << t[2] << " ";
			command << "-D camUp=" << u[0] << "," << u[1] << "," << u[2] << " ";
			command << "-D camFov=" << cam.fovy()*180.0f / float(M_PI) << " ";
			command << _scene->basePath() << "/scene/scene.xml";
			std::cout << command.str() << std::endl;
		}

		ImGui::Separator();
		if (_inRegion) {
			ImGui::TextColored(ImVec4(0.0f, 0.9f, 0.0f, 1.0f), "Inside a region.");
		}
		else {
			ImGui::TextColored(ImVec4(0.9f, 0.0f, 0.0f, 1.0f), "Outside.");
		}
		for (int i = 0; i < _indices.size(); ++i) {
			if(_indices[i] < 0) {
				continue;
			}
			ImGui::Text("Probe %d (%1.3f)", _indices[i], _coords[i]);
		}

	}
	ImGui::End();

	// Display intermediate RTs.
	if (_showDebugRTs) {
		Viewport vp(0, 0, float(_reflectedPos->w()), float(_reflectedPos->h()));
		sibr::showImGuiWindow("OptiX", *_reflectedPos, 0, vp, false, false);

		static int handle = 0;
		sibr::showImGuiWindow("Gbuffer", *(_specRenderer->prepassRT()), 0, vp, false, false, handle);
		if (ImGui::Begin("Gbuffer")) {
			ImGui::SliderInt("Handle", &handle, 0, 4);
		}
		ImGui::End();
	}
	
	// Display profiling information if enabled.
	if(ImGui::Begin("Profiling")) {

		ImGui::Text("Prepass: %5.1f ms", _gbufferTime.value() / 1000000.0f);
		ImGui::Text("Select.: %5.1f ms", _selecTime.value() / 1000000.0f);
		ImGui::Text("Raycast: %5.1f ms", _optixTime.value() / 1000000.0f);
		ImGui::Text("Gather.: %5.1f ms", _gatherTime.value() / 1000000.0f);
		ImGui::Text("Filter.: %5.1f ms", _filterTime.value() / 1000000.0f);
		ImGui::Text("Compos.: %5.1f ms", _compositeTime.value() / 1000000.0f);
		
		ImGui::Separator();

		static std::string perfStr;

		if (!_monitoring) {
			if (ImGui::Button("Start monitoring")) {
				_monitoring = true;
				_frameTimes.clear();
				_gbufferTimes.clear();
				_optixTimes.clear();
				_gatherTimes.clear();
				_filterTimes.clear();
				_compositeTimes.clear();
				perfStr = "";
			}
		}
		else {
			if (ImGui::Button("Stop monitoring")) {
				_monitoring = false;
				// Compute statistic on the timings for each step and display them.
				const std::vector<std::string> names = {"Frame", "Gbuffer", "Optix", "Gather", "Filter", "Compo"};
				const  std::vector<std::vector<float>> counts = {
					_frameTimes, _gbufferTimes, _optixTimes, _gatherTimes, _filterTimes, _compositeTimes
				};
				perfStr = "";
				for (int i = 0; i < names.size(); ++i) {
					// Compute metrics: min, max, avg, variance.
					double miniF = std::numeric_limits<double>::max();
					double maxiF = 0.0;
					double avgF = 0.0;
					for (size_t tid = 0; tid < counts[i].size(); ++tid) {
						const double ft = double(counts[i][tid]);
						avgF += ft;
						miniF = std::min(miniF, ft);
						maxiF = std::max(maxiF, ft);
					}
					avgF /= double(counts[i].size());
					double varF = 0.0;
					for (size_t tid = 0; tid < counts[i].size(); ++tid) {
						const double residualF = double(counts[i][tid]) - avgF;
						varF += residualF * residualF;
					}
					varF /= double(int(counts[i].size()) - 1);

					perfStr += names[i] + " min/max: " + std::to_string(miniF) + "/" + std::to_string(maxiF) + "\n";
					perfStr += names[i] + " avg/stddev: " + std::to_string(avgF) + "/" + std::to_string(std::sqrt(varF)) + "\n";
				}
			}
		}

		ImGui::Text(perfStr.c_str());

		if (ImGui::Button("Copy perf string")) {
			ImGui::SetClipboardText(perfStr.c_str());
		}
	}
	ImGui::End();

}

void sibr::ProbesView::snapTo(int i) {
	const InputCamera & cam = _handler->getCamera();
	const sibr::Vector3f & camPos = cam.position();
	const auto & locs = _scene->probes()->locations();
	sibr::Vector3f dstPos = camPos;
	// Look for the closest probe position.
	if(i < 0) {
		float bestDist = std::numeric_limits<float>::max();
		for (size_t lid = 0; lid < locs.size(); ++lid) {
			const float newDist = (locs[lid] - camPos).norm();
			if(newDist < bestDist) {
				bestDist = newDist;
				dstPos = locs[lid];
				_snapId = int(lid);
			}
		}
	} else if(i < int(locs.size())) {
		dstPos = locs[i];
	}
	InputCamera newCam(cam);
	newCam.position(dstPos);
	_handler->fromCamera(newCam);
}

struct Score {
	int id;
	float dist;

	friend bool operator <(const Score & a, const Score & b) {
		return a.dist < b.dist;
	}
	
	friend bool operator >(const Score & a, const Score & b) {
		return a.dist > b.dist;
	}
};


float sibr::ProbesView::aaVolume(const sibr::Vector3f& p0, const sibr::Vector3f& p1)
{
	const Vector3f diff = (p1 - p0).cwiseAbs();
	return diff.prod();
}


void sibr::ProbesView::selectProbes(const Camera& eye) {
	
	// Select the probes.
	if (_selectionMode == FORCE_ONE) {
		// Pick only one and don't mix positions.
		_inRegion = true;
		_indices[0] = _forcedId;
		_coords[0] = 1.0f;
		for (size_t lid = 1; lid < _indices.size(); ++lid) {
			_indices[lid] = -1;
			_coords[lid] = 0.0f;
		}
		_specRenderer->debug().probeLocalId = 0;
		
		return;
	}

	// Closest probes based on distance and angle
	if (_selectionMode == ANGULAR) {
		const sibr::Vector3f pos = eye.position();
		// Build the line defined by the camera direction.
		Eigen::ParametrizedLine<float, 3> line(pos, eye.dir());

		const auto & locations = _scene->probes()->locations();
		std::vector<Score> scores(locations.size());
		for (int i = 0; i < locations.size(); ++i) {
			scores[i].id = i;
			const sibr::Vector3f ldir = locations[i] - pos;
			const float dist = ldir.norm();
			const RayHit hit = _scene->raycaster()->intersect(Ray(pos, ldir));
			if (hit.hitSomething() && hit.dist() < dist) {
				scores[i].dist = 100000.0f;
			}
			else {
				scores[i].dist = static_cast<float>(line.distance(locations[i]));
			}

		}
		std::sort(scores.begin(), scores.end());
		_inRegion = true;
		float totalInvDist = 0.0f;
		const size_t bound = std::min(scores.size(), _indices.size());
		for (size_t i = 0; i < bound; ++i) {
			_indices[i] = scores[i].id;
			_coords[i] = 1.0f / std::max(0.001f, scores[i].dist);
			totalInvDist += _coords[i];
		}
		for (size_t i = 0; i < bound; ++i) {
			_coords[i] /= totalInvDist;
		}
		for (size_t i = bound; i < _indices.size(); ++i) {
			_indices[i] = -1;
			_coords[i] = 0.0f;
		}
		return;
	}
	
	// Closest probes based on euclidean distance.
	if(_selectionMode == DISTANCE) {
		const auto & locations = _scene->probes()->locations();
		std::vector<Score> scores(locations.size());
		for (int i = 0; i < locations.size(); ++i) {
			scores[i].id = i;
			if (i % _probeStep == 0) {
				const sibr::Vector3f ldir = locations[i] - eye.position();
				scores[i].dist = ldir.norm();
			}
			else {
				scores[i].dist = std::numeric_limits<float>::max();
			}
			
		}
		std::sort(scores.begin(), scores.end());
		_inRegion = true;
		float totalInvDist = 0.0f;
		const size_t bound = std::min(scores.size(), _indices.size());
		for (size_t i = 0; i < bound; ++i) {
			_indices[i] = scores[i].id;
			_coords[i] = 1.0f / std::max(0.0001f, scores[i].dist);
			totalInvDist += _coords[i];
		}
		for (size_t i = 0; i < bound; ++i) {
			_coords[i] /= totalInvDist;
		}
		for (size_t i = bound; i < _indices.size(); ++i) {
			_indices[i] = -1;
			_coords[i] = 0.0f;
		}
		return;
	}

	// Select the 8 closest probes with trilinear interpolation weights.
	if (_selectionMode == TRILINEAR) {
			
		const auto& locations = _scene->probes()->locations();
		
		sibr::Vector3f e = eye.position();
		const sibr::Vector3f eps = sibr::Vector3f::Constant(0.0001f);
		e = e.cwiseMax(_probesBBox.min() + eps).cwiseMin(_probesBBox.max() - eps);

		for (int i = 0; i < _coords.size(); i++) _coords[i] = FLT_MAX;
			
		// Select 8 closest probes on regular grid
		// Could be replaced by constant-time lookup, when probes bounding box and resolution is known
		for (int i = 0; i < locations.size(); ++i)
		{
			const sibr::Vector3f& p = locations[i];
			const float norm = (p - e).lpNorm<1>();

			for (int j = 0; j < 8; j++)
			{
				const bool x = ((j >> 2) & 1) != (p.x() <= e.x());
				const bool y = ((j >> 1) & 1) != (p.y() <= e.y());
				const bool z = ((j >> 0) & 1) != (p.z() <= e.z());

				if (x && y && z && norm < _coords[j])
				{
					_indices[j] = i;
					_coords[j] = norm;
				}
			}
		}

		// determine trilinear interpolation weights,
		// proportional to partial volume diagonally opposite the corner
		float weightSum = 0;
		for (int i = 0; i < 8; i++)
		{
			_coords[i] = aaVolume(e, locations[_indices[8-i-1]]);
			weightSum += _coords[i];
		}
		if (weightSum > 0)
		{
			for (int i = 0; i < 8; i++) _coords[i] /= weightSum;
		}
		return;
	}
}

void sibr::ProbesView::onUpdate(Input& input)
{
	// Live shader reloading for debug.
	if (input.key().isReleased(Key::F8)) {
		_specRenderer->loadShaders(true);
		_bilateralFilter->loadShaders();
		_fxaaShader.init("FXAA", sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_texture.vert"), sibr::loadFile(sibr::getShadersDirectory("synthetic_ibr") + "/synthetic_fxaa.frag"));
		_invSize.init(_fxaaShader, "inverseScreenSize");
	}
	if (input.key().isActivated(Key::LeftControl) && input.mouseScroll() != 0.0) {
		_cameraScale = std::max(0.001f, _cameraScale + (float)input.mouseScroll()*0.1f);
	}
	if (input.key().isReleased(Key::Enter)) {
		_pathCams.emplace_back(new InputCamera(_handler->getCamera()));
	}
}

void sibr::ProbesView::registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler)
{
	_debugView = debugView;
	if(_debugView){
		_debugView->addMesh("Proxy", _scene->proxies()->proxyPtr()).setColorMode(MeshData::ColorMode::USER_DEFINED).setColor({ 0.8f, 0.8f, 0.8f });
	}
	_handler = handler;
	snapTo(-1);
}

