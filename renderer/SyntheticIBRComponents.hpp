/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include "Config.hpp"
# include <core/system/CommandLineArgs.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Image.hpp>

namespace sibr {

	/** Glossy probe helpers. */
	namespace syntheticIBR {

		 void SIBR_SYNTHETIC_IBR_EXPORT tonemap(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg, float exposure, bool gamma = false);

		 void SIBR_SYNTHETIC_IBR_EXPORT convert(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg);

		 void SIBR_SYNTHETIC_IBR_EXPORT gamma(const sibr::ImageRGB32F & hdrImg, sibr::ImageRGB & ldrImg, float gamma);

		/** Apply reverse adaptive parameterization to an pixel location, obtaining a normalized direction. */
		 sibr::Vector3f SIBR_SYNTHETIC_IBR_EXPORT pixelToDirWarped(const sibr::Vector2f & pix, const sibr::Vector2i & res, const ImageFloat2 & warpMap);

		/** Save an image as a binary blob. Wrapper for intermediate testing. */
		 template<typename T_Type, unsigned int T_NumComp>
		 void saveImageBinary(const Image<T_Type, T_NumComp> & img, const std::string & outputPath) {
			 img.saveByteStream(outputPath, false);
		 }

		 /**
		  *	Load the scale of a scene.
		  *	\param basePath the base scene path (root dir)
		  *	\return how many scene units make 1 meter (for instance 100.0 if 1 unit = 1cm).
		  */
		float SIBR_SYNTHETIC_IBR_EXPORT loadScale(const std::string & basePath);


	}

	/** General arguments for the project. */
	struct SIBR_SYNTHETIC_IBR_EXPORT SyntheticAppArgs :
		virtual BasicIBRAppArgs {
		Arg<std::string> sceneSuffix = { "name", "", "the scene name" };
		Arg<bool> skipTopView = { "no-topview", "do not setup the top view" };
		Arg<std::string> lutPath = { "lut", "", "path to the variance LUT bin file" };
		Arg<bool> ssdiff = { "ssdif", "enable supersampling of the diffuse rendering" };
	};

	/** Probe locations. basilcally a list of 3D positions laoded from a text file. */
	class SIBR_SYNTHETIC_IBR_EXPORT ProbeLocations {
	public:
		SIBR_CLASS_PTR(ProbeLocations);

		/** Load from txt file with X Y Z on each line. */
		void setupFromPath(const std::string& probesPath);

		const std::vector<sibr::Vector3f>& locations() const { return _locations; }

	private:

		std::vector<sibr::Vector3f> _locations;
	};

	/** Implement a half float texture array.
	 *It was not possible to use the existing textures
	 *directly as the GPU type is strongly tied to the CPU
	 *OpenCV matrix type, which doesn't support half floats by default. */
	template<unsigned int T_NumComp>
	class Texture2DArray16F {
	public:

		typedef		Image<float, T_NumComp>			PixelImage;
		typedef	std::shared_ptr<Texture2DArray16F<T_NumComp>>	Ptr;

		Texture2DArray16F(const std::vector<std::vector<typename PixelImage::Ptr>>& images, uint flags);

		GLuint handle() const {
			return _handle;
		}

	private:
		GLuint _handle;
	};

	template<unsigned int T_NumComp>
	Texture2DArray16F<T_NumComp>::Texture2DArray16F(const std::vector<std::vector<typename PixelImage::Ptr > > & images, uint flags) {
		CHECK_GL_ERROR;
		glGenTextures(1, &_handle);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _handle);

		const int rmm = int(images.size());
		const int rml = int(images[0].size());
		const int rmw = images[0][0]->w();
		const int rmh = images[0][0]->h();

		if (flags & SIBR_GPU_LINEAR_SAMPLING) {
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else {
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
	
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


		static const GLenum preciseFormats[5] = { 0, GL_R16F, GL_RG16F, GL_RGB16F, GL_RGBA16F };
		static const GLenum formats[5] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };

		glTexStorage3D(GL_TEXTURE_2D_ARRAY, rmm, preciseFormats[T_NumComp], rmw, rmh, rml);

		CHECK_GL_ERROR;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glPixelStorei(GL_PACK_ALIGNMENT, 1);

		for (int lid = 0; lid < rmm; ++lid) {

			// Make sure all images have the same size.
			const uint dW = rmw / (1 << lid);
			const uint dH = rmh / (1 << lid);

			for (int im = 0; im < (int)rml; ++im) {
				PixelImage::Ptr clImg = images[lid][im]->clonePtr();
				if (flags & SIBR_FLIP_TEXTURE) {
					clImg->flipH();
				}
				glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					lid,
					0, 0, im,
					dW,
					dH,
					1, // one slice at a time
					formats[T_NumComp],
					GL_FLOAT,
					clImg->data()
				);
			}
		}
		CHECK_GL_ERROR;
	}
	
	/** Implement a half float 2D texture.
	 *It was not possible to use the existing textures
	 *directly as the GPU type is strongly tied to the CPU
	 *OpenCV matrix type, which doesn't support half floats by default. */
	template<unsigned int T_NumComp>
	class Texture2D16F {
	public:

		typedef		Image<float, T_NumComp>			PixelImage;
		typedef	std::shared_ptr<Texture2D16F<T_NumComp>>	Ptr;

		Texture2D16F(const std::vector<typename PixelImage::Ptr>& images, uint flags);

		GLuint handle() const {
			return _handle;
		}

	private:
		GLuint _handle;
	};

	template<unsigned int T_NumComp>
	Texture2D16F<T_NumComp>::Texture2D16F(const std::vector<typename PixelImage::Ptr >& images, uint flags) {
		CHECK_GL_ERROR;
		glGenTextures(1, &_handle);
		glBindTexture(GL_TEXTURE_2D, _handle);

		const int rmm = images.size();
		const int rmw = images[0]->w();
		const int rmh = images[0]->h();

		if (flags & SIBR_GPU_LINEAR_SAMPLING) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


		static const GLenum preciseFormats[5] = { 0, GL_R16F, GL_RG16F, GL_RGB16F, GL_RGBA16F };
		static const GLenum formats[5] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };

		glTexStorage2D(GL_TEXTURE_2D, rmm, preciseFormats[T_NumComp], rmw, rmh);

		CHECK_GL_ERROR;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glPixelStorei(GL_PACK_ALIGNMENT, 1);

		for (int lid = 0; lid < rmm; ++lid) {

			// Make sure all images have the same size.
			const uint dW = rmw / (1 << lid);
			const uint dH = rmh / (1 << lid);

			PixelImage::Ptr clImg = images[lid]->clonePtr();
			if (flags & SIBR_FLIP_TEXTURE) {
				clImg->flipH();
			}
			glTexSubImage2D(GL_TEXTURE_2D,
					lid,
					0, 0,
					dW,
					dH,
					formats[T_NumComp],
					GL_FLOAT,
					clImg->data()
			);
		}
		CHECK_GL_ERROR;
	}

	typedef Texture2DArray16F<3> Texture2DArrayRGB16F;
	typedef Texture2DArray16F<4> Texture2DArrayRGBA16F;
	typedef Texture2DArray16F<2> Texture2DArrayUV16F; 
	typedef Texture2D16F<1> Texture2DLum16F;


}