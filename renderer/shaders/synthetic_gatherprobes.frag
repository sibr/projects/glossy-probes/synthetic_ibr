/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 430

#define PROBES_COUNT (12)

in vec2 texcoord;

// Input probe.
struct ProbesInfos
{
  vec4 pos;
  int selected;
  float d0,d1,d2;
};

// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=10) uniform InputProbes
{
  ProbesInfos probes[PROBES_COUNT];
};

uniform vec3 camPos;
uniform float sceneScale;
uniform vec4 thresholds;

uniform vec3 stepsScale = vec3(4.0, 2.0, 1.0);
#define RADIUS_L (3)
#define RADIUS_S (2)
const int lodL = 0;
const int lodS = 0;

uniform float surfTh;
uniform float maxPosError;
uniform float surfAdj;
uniform float posAdj;
uniform float lobeAdj;
uniform float errorScale;
uniform float refmatPenal;

// Inputs from the gbuffer.
layout(binding = 0) uniform sampler2D dirMat;
layout(binding = 1) uniform sampler2D posConf;
layout(binding = 2) uniform sampler2D colorDist;
layout(binding = 3) uniform sampler2D minc;
layout(binding = 4) uniform sampler2D maxc;
// Optix.
layout(binding = 5) uniform sampler2D refRefl;
// Probes data.
layout(binding = 6) uniform sampler2DArray inputRGBs;
layout(binding = 7) uniform sampler2DArray reflectedMaps;
layout(binding = 8) uniform sampler2DArray warpMaps;
layout(binding = 9) uniform isampler2DArray geomMaps;
layout(binding = 10) uniform sampler2DArray diffWarpMaps;

// Temporal reprojection inputs.
layout(binding = 11) uniform sampler2D prevFrame;
uniform mat4 prevMVP;

// Geometry buffers.
restrict readonly layout(std430, binding = 0) buffer Data0 {
    vec4 meshPositions[];
};
restrict readonly layout(std430, binding = 1) buffer Data1 {
    vec4 meshMincs[];
};
restrict readonly layout(std430, binding = 2) buffer Data2 {
    vec4 meshMaxcs[];
};
restrict readonly layout(std430, binding = 3) buffer Data3 {
    uint meshMatIds[];
};
restrict readonly layout(std430, binding = 4) buffer Data4 {
    uvec4 meshTris[];
};
restrict readonly layout(std430, binding = 5) buffer Data5 {
    vec4 meshNormals[];
};

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outBackup;

#define M_PI 3.14159265359
#define NOT_FOUND 10000000.0

// Specular path perturbation.
vec3 estimatePosition(vec3 probePos, vec3 currPos, vec3 reflectorPos, vec3 reflectedPos, vec3 minC, vec3 maxC, float minR, float maxR, vec3 refN);

// Reprojection utilities.

vec2 worldToSpherical(vec3 world_coord);

// Error measurement.

bool nearestGeometricParams(vec3 uv, vec2 texsize, vec2 invsize, uint matId, out vec3 reflectorPos, out vec3 reflectorNor, int lod, out int refMatId);

float evalError(vec3 pos, vec3 reflectorPos, vec3 reflectedPos, vec3 reflectorNor, vec3 probePos, vec3 probeReflectorPos, vec3 probeReflectedPos, vec3 probeReflectorNor, float surfDisagreement);

bool evaluateSample(vec3 pbuv, uint matId, int refMatId, vec3 reflectorPos, vec3 reflectedPos, vec3 refN, vec3 probePos, float depthTh, float newDepth,  inout vec4 bestUVError, vec2 texSize, vec2 invSize, int lod, float conf);

// Color fetch.

vec4 readBilinearColor(vec3 pbuv, vec2 texsize, vec2 invsize, uint matId);

// Main.
void main(void) {

	// Read information at the corresponding point.
	ivec2 icoords = ivec2(gl_FragCoord.xy);
	vec4 posAndConf = texelFetch(posConf, icoords, 0);
	float confidence = posAndConf.a;
	// Skip diffuse regions (confidence == -1).
	if(confidence < -0.5){
		outColor = vec4(0.0,0.0,0.0,-1.0);
		return;
	}

	// Fetch curvature info.
	vec4 dirAndMat = texelFetch(dirMat, icoords, 0);
	vec4 minCR = texelFetch(minc, icoords, 0);
	vec4 maxCR = texelFetch(maxc, icoords, 0);
	vec3 minDir = normalize(minCR.xyz);
	vec3 maxDir = normalize(maxCR.xyz);
	// Ensure orthogonality.
	vec3 refN = normalize(cross(minDir, maxDir));
	// Reflection info.
	vec4 reflPosAndMat = texelFetch(refRefl, icoords, 0);
	vec3 reflectedPos = reflPosAndMat.xyz;
	vec3 reflectorPos = posAndConf.xyz;
	uint matId = uint(dirAndMat.w);
	int refMatId = (int(round(reflPosAndMat.a)));

	float depthTh = thresholds.x;
	float reflectTh = thresholds.y;

	// We use LOD 0 everywhere now, we could skip this.
	vec2 texsizeL = textureSize(geomMaps, lodL).xy;
	vec2 invsizeL = 1.0/texsizeL;
	vec2 texsizeS = textureSize(geomMaps, lodS).xy;
	vec2 invsizeS = 1.0/texsizeS;
	vec2 texsize = textureSize(geomMaps, 0).xy;
	vec2 invsize = 1.0/texsize;

	float minError = 100000000.0;
	int validCount = 0;
	vec4 accumColor = vec4(0.0);

	// Iterate over the selected neighbors.
	for(int pid = 0; pid < PROBES_COUNT; ++pid){
			
		if(probes[pid].selected == 0){
			continue;
		}
		vec3 probePos = probes[pid].pos.xyz;
		// Estimate warped reflector position from probe viewpoint.
		// Implement Chen & Arvo 2001 for a paraboloid surface.
		vec3 newReflectorPos = estimatePosition(camPos, probePos, reflectorPos, reflectedPos, minDir, maxDir, minCR.w, maxCR.w, refN);
		// Reproject into the probe.
		vec3 udir = (newReflectorPos - probePos);
		float newDepth = length(udir);
		vec2 warpUV = worldToSpherical(udir);
		vec3 probeUV = vec3(textureLod(warpMaps, vec3(warpUV, pid), 0.0).xy, pid);
		// We clamp to avoid division by zero and avoid stepping onto the same texel many times.
		vec2 uvDiffs = clamp(textureLod(diffWarpMaps, probeUV, 0.0).xy, 0.01, 10.0);
		vec2 commonShift = invsize / uvDiffs;
		// Compute step sizes for both search windows.
		vec2 largeShifts = stepsScale[0] * commonShift;
		vec2 smallShifts = (stepsScale[1] * commonShift);

		// Look around to find a better location.
		vec4 bestUVError = vec4(0.0,0.0,0.0,NOT_FOUND);
		// First search.
		for(int dy = -RADIUS_L; dy <= RADIUS_L; ++dy){
			for(int dx = -RADIUS_L; dx <= RADIUS_L; ++dx){
				vec3 shiftp = vec3(vec2(dx,dy)*largeShifts, 0.0);
				vec3 pbuv = probeUV + shiftp;
				bool found = evaluateSample(pbuv, matId, refMatId, reflectorPos, reflectedPos, refN, probePos, depthTh, newDepth, bestUVError, texsizeL, invsizeL, lodL, confidence);
				
			}
		}
		// if we were not able to find anything, skip the probe.
		if(bestUVError.a == NOT_FOUND){
			continue;
		}
		// Second search window
		for(int dy = -RADIUS_S; dy <= RADIUS_S; ++dy){
			
			for(int dx = -RADIUS_S; dx <= RADIUS_S; ++dx){
				vec3 shiftp = vec3(vec2(dx,dy)*smallShifts, 0.0);
				vec3 pbuv = bestUVError.xyz + shiftp;
				bool found = evaluateSample(pbuv, matId, refMatId, reflectorPos, reflectedPos, refN, probePos, depthTh, newDepth, bestUVError, texsizeS, invsizeS, lodS, confidence);
				
			}
		}
		
		// Do our own ID aware filtering to avoid artifacts at object edges.
		vec4 bcola = readBilinearColor(bestUVError.xyz, texsize, invsize, matId);
		
		if(bcola.a != 0.0){
			bcola /= bcola.a;
			// If black on a perfect mirror, just skip.
			// \todo: check if still needed
			if(confidence > 0.98 && all(lessThan(bcola.xyz, vec3(1.0/255.0)))){
				continue;
			}
			// Accumulate, taking into account relative error score.
			float pw = probes[pid].pos.w;
			float weight = exp(-bestUVError.a );
			minError = min(bestUVError.a, minError);
			bcola.a = 1.0;
			float fweight = pw * weight;
			accumColor += fweight * bcola;
		} 
	}
	// Validity score of the current best point based on its error.
	float validity = clamp(1.0 / max(minError, 0.001), 0.01,  1.0);

	if(accumColor.a != 0.0){
		accumColor /= accumColor.a;
	} else {
		// If we haven't found any color info,
		// reproject from previous frame.
		vec4 newPos = prevMVP * vec4(reflectorPos, 1.0);
		newPos.xyz /= newPos.w;
		vec2 prevUV = newPos.xy * 0.5 + 0.5;
		ivec2 prevFetch = ivec2(prevUV * textureSize(prevFrame, 0).xy);
		vec4 prevColor = texelFetch(prevFrame, prevFetch, 0);
		uint prevMatId = uint(fract(prevColor.a/1000.0) * 1000.0);
		float prevVal = floor(prevColor.a/1000.0)/1000.0;
		// If IDs are similar, reuse.
		if(prevMatId == matId){
			accumColor.rgb = prevColor.rgb;
			validity = prevVal;
		} else {
			discard;
		}
	}
	// Save info for future reprojection at next frame.
	outBackup.rgb = accumColor.rgb;
	outBackup.a = float(matId) + 1000.0*(float(int(clamp(validity*1000.0, 0.0, 100000000.0))));
	// Output result.
	outColor.rgb = accumColor.rgb;
	outColor.a = validity;
}



/** Implementation of 'Theory and Application of Specular Path Perturbation',
Min Chen, James Arvo, (2001)
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.88.2973&rep=rep1&type=pdf
for paraboloids, as described in section 5.1. */

vec3 computeCommonDiff(int i, int j, vec3 pmx, float dpx3, float dpx5){
	float dij = (j == i) ? 1.0 : 0.0;
	vec3 dik = vec3(0.0); dik[i] = 1.0;
	vec3 djk = vec3(0.0); djk[j] = 1.0;
	return (dij * pmx + dik * pmx[j] + djk * pmx[i])/dpx3 - 3.0 * pmx[i] * pmx[j] * pmx / dpx5;
}

vec3 estimatePosition(vec3 probePos, vec3 currPos, vec3 reflectorPos, vec3 reflectedPos, vec3 minC, vec3 maxC, float minR, float maxR, vec3 refN){
	vec3 dx = vec3(0.0);
	vec3 ddx = vec3(0.0);
	vec3 p = probePos;
	vec3 dp = currPos - p;
	vec3 q = reflectedPos;
	vec3 x = reflectorPos;

	vec3 h;
	mat3 hh;
	mat3 hhh[3];
	{
		
		mat3 localToWorld = mat3(minC, maxC, refN);
		mat3 worldToLocal = transpose(localToWorld);
		mat3 R = worldToLocal;
		vec3 C = x;

		mat3 A = mat3(0.0);
		A[0][0] = 1.0/(2.0*minR);
		A[1][1] = 1.0/(2.0*maxR);
		A[2][2] = 0.0;
		vec3 B = vec3(0.0, 0.0, 1.0);
		mat3 D = transpose(R)*A*R;
		mat3 tD = transpose(D);

		vec3 E =  B * R;
		h = (D + tD) * (x - C) + E;
		hh = D + tD;
		hhh[0] = hhh[1] = hhh[2] = mat3(0.0);

	}
	
	vec3 absh = abs(h);
	int sj = 0;
	
	if(absh.x > 0.1 && absh.y > 0.1 && absh.z > 0.1){
		sj = 2;
	} else if(absh.x > 0.1){
		sj = 0;
	} else if(absh.y > 0.1){
		sj = 1;
	}  else {
		sj = 2;
	}

	vec3 px    = p - x;
	vec3 qx    = q - x;
	float lpx  = max(0.001, length(px)  );
	float lqx  = max(0.001, length(qx)  );
	float lpx3 = max(0.001, lpx*lpx*lpx );
	float lqx3 = max(0.001, lqx*lqx*lqx );
	float lpx5 = max(0.001, lpx3*lpx*lpx);
	float lqx5 = max(0.001, lqx3*lqx*lqx);
	float denomCoeff = absh[sj] < 0.001 ? 0.001 : h[sj];

	float lambda = (1.0/denomCoeff) * (px[sj]/lpx + qx[sj]/lqx);
	mat3 dFwrtX = (1.0/lpx + 1.0/lqx) * mat3(1.0) - outerProduct(px,px)/(lpx3) - outerProduct(qx,qx)/(lqx3) + lambda * hh;
	mat3 dFwrtP = (-1.0/lpx) * mat3(1.0) + outerProduct(px,px)/(lpx3);
	// Building A.
	mat4 A = mat4(dFwrtX);
	A[0][3] = A[3][0] = h[0];
	A[1][3] = A[3][1] = h[1];
	A[2][3] = A[3][2] = h[2];
	A[3][3] = 0.0;
	// Building T.
	mat3x4 T = mat3x4(dFwrtP);
	mat4x4 G = inverse(A);
	mat3x4 D = -G * T;
	mat3 J = mat3(D);
	dx = J * dp;

	// Building grad(A) and grad(T).
	mat4 AA[3];
	mat3x4 TT[3];
	// Just in case.
	for(int i = 0; i < 3; ++i){
		AA[i] = mat4(0.0);
		TT[i] = mat3x4(0.0);
	}
	for(int k = 0; k < 3; ++k){
		for(int j = 0; j < 3; ++j){
			vec3 dTwrtP = computeCommonDiff(j, k, px, lpx3, lpx5); 
			vec3 dT = dTwrtP + vec4(-dTwrtP, 0.0) * D; 
			TT[0][k][j] = dT[0]; TT[1][k][j] = dT[1]; TT[2][k][j] = dT[2];
			vec3 dAwrtP = -dTwrtP;
			vec3 dAwrtX = dTwrtP + computeCommonDiff(j, k, qx, lqx3, lqx5) + lambda * vec3(hhh[0][k][j], hhh[1][k][j], hhh[2][k][j]);
			float dAwrtL = hh[k][j];
			vec3 dA = dAwrtP + vec4(dAwrtX, dAwrtL) * D;
			AA[0][k][j] = dA[0]; AA[1][k][j] = dA[1]; AA[2][k][j] = dA[2];
		}
		TT[0][k][3] = TT[1][k][3] = TT[2][k][3] = 0.0; 
		vec3 dA = vec4(hh[k], 0.0) * D;
		AA[0][3][k] = AA[0][k][3] = dA[0];
		AA[1][3][k] = AA[1][k][3] = dA[1];
		AA[2][3][k] = AA[2][k][3] = dA[2];
	}
	AA[0][3][3] = AA[1][3][3] = AA[2][3][3] = 0.0;
	mat3x4 DD[3];
	for(int i = 0; i < 3; ++i){
		DD[i] = -G * (TT[i] + AA[i] * D);
	}
	// From those we can build the jacobian and the triple hessian.
	for(int j = 0; j < 3; ++j){
		mat3 HJ;
		for(int k = 0; k < 3; ++k){
			for(int m = 0; m < 3; ++m){
				HJ[k][m] = DD[m][j][k];
			}
		}
		ddx[j] = dot(dp, HJ * dp); 
	}
	
	return x + 1.0 * dx + 0.5 * ddx;
}


/// Reprojection utilities.

vec2 worldToSpherical(vec3 world_coord){
	vec3 n_world_coord = normalize(world_coord.xyz);
	float longitude = atan(-n_world_coord.x, n_world_coord.z);
	float latitude = acos(n_world_coord.y);
	vec2 norm_coord = vec2(((longitude/M_PI)) * 0.5 + 0.5, (latitude/M_PI));
	return norm_coord;
}

/// Search and evaluation.

/// Unpack our barycentric coordinates.
vec3 convertToBary(int x){
	vec2 barys = unpackSnorm2x16(x).yx;
	return vec3(clamp(1.0 - barys.y - barys.x, 0.0, 1.0), barys.x, barys.y);
}

/** Fetch info from the geometric maps for a given probe and texel. */
bool nearestGeometricParams(vec3 uv, vec2 texsize, vec2 invsize, uint matId, out vec3 reflectorPos, out vec3 reflectorNor, int lod, out int refMatId) {
	
	vec2 pixCoords = uv.xy*texsize;
	ivec3 pixCoordsFloor = ivec3((pixCoords),uv.z);

	ivec4 triInfos = texelFetch(geomMaps, pixCoordsFloor, lod);
	// We need to check multiple things:
	//	* Is there anything here?
	if (triInfos[0] == -1) {
		return false;
	}

	uvec3 vids = meshTris[triInfos[0]].xyz;
	//  * is the data at that point from the same object (reflector material test)
	const uint provokMatId = meshMatIds[vids[0]];
	if (matId != provokMatId) {
		return false;
	}

	// We use barycentric coordinates to get the position and normal.
	vec3 barys = convertToBary(triInfos[1]);
	
	reflectorPos = barys.x * meshPositions[vids.x].xyz + barys.y * meshPositions[vids.y].xyz + barys.z * meshPositions[vids.z].xyz;
	reflectorNor = barys.x * meshNormals[vids.x].xyz + barys.y * meshNormals[vids.y].xyz + barys.z * meshNormals[vids.z].xyz;
	reflectorNor = normalize(reflectorNor);

	refMatId = triInfos[2];
	return true;
}

/** Evalute the error described in section 5.2 of the paper, Eq.1 */
float evalError(vec3 pos, vec3 reflectorPos, vec3 reflectedPos, vec3 reflectorNor, vec3 probePos, vec3 probeReflectorPos, vec3 probeReflectedPos, vec3 probeReflectorNor, float surfDisagreement){
	
	vec3 deltaPos = probeReflectedPos - reflectedPos;
	float reflectedError = length(deltaPos);
	reflectedError = min(reflectedError/sceneScale, maxPosError)/maxPosError; 

	float surfError = min(surfDisagreement, 1.0);

	float coneErrorRef = 1.0-(dot(normalize(reflectedPos - reflectorPos), normalize(probeReflectedPos - probeReflectorPos)));
	coneErrorRef = min(1.0, coneErrorRef);

	float error = posAdj*reflectedError + surfAdj*surfError + lobeAdj * coneErrorRef;
	error *= errorScale;
	return error;
}


bool evaluateSample(vec3 pbuv, uint matId, int refMatId, vec3 reflectorPos, vec3 reflectedPos, vec3 refN, vec3 probePos, float depthTh, float newDepth,  inout vec4 bestUVError, vec2 texSize, vec2 invSize, int lod, float conf){
	vec3 probeReflectorPos = vec3(0.0);
	vec3 probeReflectorNor = vec3(0.0);
	int probeRefMatId = 0;
	// Fetch geoemtric info from the probe.
	bool success = nearestGeometricParams(pbuv, texSize, invSize, matId, probeReflectorPos, probeReflectorNor, lod, probeRefMatId);
	if (!success) {
		return false;
	}

	// Error in orientation.
	float surfDisagreement = 1.0-dot(refN, probeReflectorNor);
	if(surfDisagreement > surfTh){
		return false;
	}
	
	// Compute score for the candidate.
	vec3 probeReflectedPos = textureLod(reflectedMaps, pbuv, lod).rgb;
	float error = evalError(camPos, reflectorPos, reflectedPos, refN, probePos, probeReflectorPos, probeReflectedPos, probeReflectorNor, surfDisagreement);
	error *= (probeRefMatId != refMatId ? refmatPenal : 1.0);

	// Update if better.
	if(error < bestUVError.a){
		bestUVError.xyz = pbuv;
		bestUVError.a = error;
		return true;
	} 
	return false;
}

/** Bilinear color fetch that is material aware to avoid spilling artifacts. */
vec4 readBilinearColor(vec3 pbuv, vec2 texsize, vec2 invsize, uint matId){
			
	vec2 pixCoords = pbuv.xy*texsize-0.5;
	vec2 pixCoordsFloor = floor(pixCoords);
	vec3 gatherUV = vec3((pixCoordsFloor + 1.0) * invsize, pbuv.z);

	ivec4 tritris = textureGather(geomMaps,  gatherUV, 0);
	vec4 reds = textureGather(inputRGBs, gatherUV, 0);
	vec4 greens = textureGather(inputRGBs, gatherUV, 1);
	vec4 blues = textureGather(inputRGBs, gatherUV, 2);
		
	vec2 bw = pixCoords - pixCoordsFloor;
	vec4 weights = vec4(bw.y*(1.0-bw.x), bw.y*bw.x, bw.x*(1.0-bw.y), (1.0-bw.x)*(1.0-bw.y));
		
	vec4 bcola = vec4(0.0);
	for (int bid = 0; bid < 4; ++bid) {

		// We need to check multiple things:
		//	* Is there anything here?
		if (tritris[bid] == -1) {
			weights[bid] = 0.0;
			continue;
		}
		//  * is the data at that point from the same object (reflector material test)
		uvec3 vids = meshTris[tritris[bid]].xyz;
		if (matId != meshMatIds[vids[0]]) {
			weights[bid] = 0.0;
			continue;
		}
		// If yes accumulate with the proper weight.
		bcola += weights[bid] * vec4(reds[bid], greens[bid], blues[bid], 1.0);
			
	}
	return bcola;
}