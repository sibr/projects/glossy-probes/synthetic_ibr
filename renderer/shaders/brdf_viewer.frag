/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in vec2 tex_coord;

uniform float roughness;
uniform vec3 planeNormal;
uniform vec3 camPos;
uniform vec3 lightPos;
uniform vec3 imgCorner;
uniform vec3 imgDx;
uniform vec3 imgDy;

const int res = 1024;

layout(location = 0) out vec4 fragColor;

#define INV_M_PI 0.318309886183790671538
#define M_PI 3.14159265358979323846

vec3 F(vec3 F0, float VdotH){
	return F0 + pow(1.0 - VdotH, 5.0) * (1.0 - F0);
}

float D(float NdotH, float alpha){
	float halfDenum = NdotH * NdotH * (alpha * alpha - 1.0) + 1.0;
	float halfTerm = alpha / max(0.00001, halfDenum);
	return halfTerm * halfTerm * INV_M_PI;
}

float G1(float NdotX, float alpha){
	float cosX2 = NdotX*NdotX;
	float tanX2 = (1.0 - cosX2) / cosX2;
	float alpha2 = alpha * alpha;
	return 2.0 / (1.0 + sqrt(1.0 + alpha2 * tanX2));
}

float G(float NdotL, float NdotV, float alpha){
	return G1(NdotL, alpha) * G1(NdotV, alpha);
}

vec3 ggx(vec3 n, vec3 v, vec3 l, vec3 F0, float alpha){
	// Compute half-vector.
	vec3 h = normalize(v+l);
	// Compute all needed dot products.
	float NdotL = clamp(dot(n,l), 0.0, 1.0);
	float NdotV = clamp(dot(n,v), 0.0, 1.0);
	float NdotH = clamp(dot(n,h), 0.0, 1.0);
	float VdotH = clamp(dot(v,h), 0.0, 1.0);
	float alphaClamp = max(0.0001, alpha);

	return D(NdotH, alphaClamp) * G(NdotL, NdotV, alphaClamp) * F(F0, VdotH) / (4.0f * NdotV * NdotL);
}

/** Intersect frontal plane and evaluate Cook-Torrance here. */
void main(){
	// Build ray.
	vec2 pixCoord = floor(res * tex_coord);
	vec3 worldPos = imgCorner + (pixCoord.x + 0.5) * imgDx +  (pixCoord.y + 0.5) * imgDy;
	vec3 dir = normalize(worldPos - camPos);
	// Intersect plane.
	float t = - dot(camPos, planeNormal) / dot(dir, planeNormal);
	vec3 hit = camPos + t * dir;

	vec3 l = normalize(lightPos - hit);
	vec3 v = normalize(camPos - hit);
	vec3 F0 = vec3(1.0);
	vec3 brdf = ggx(planeNormal, v, l, F0, roughness);
	fragColor.rgb = vec3(brdf.x);
	fragColor.a = 1.0;
}