/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420


layout(location = 0) in vec3 in_vertex;

uniform int axis;
uniform int slice;
uniform vec2 scales;
uniform mat4 mvp;
uniform bool mode2D;

layout(binding = 0) uniform sampler3D vars;

layout(location = 0) out vec4 fragColor;

out vec2 tex_coord;

/** Variance lookup table viewer, applied on a wireframe grid. */
void main(){

	tex_coord = in_vertex.xz * 0.5 + 0.5;
	if(mode2D){
		gl_Position = vec4(in_vertex.xz, 0.0, 1.0);
		return;
	}
	
	// Build tex coordinates.
	vec3 size = textureSize(vars,0).xyz;

	ivec3 coords;

	if(axis == 0){
		coords[0] = min(slice, int(size[0]));
		coords[1] = int(floor(tex_coord.x * size[1]));
		coords[2] = int(floor(tex_coord.y * size[2]));
	} else if(axis == 1){
		coords[0] = int(tex_coord.x * size[0]);
		coords[1] = min(slice, int(size[1]));
		coords[2] = int(tex_coord.y * size[2]);
	} else {
		coords[0] = int(float(tex_coord.x * size[0]));
		coords[1] = int(float(tex_coord.y * size[1]));
		coords[2] = min(slice, int(size[2]));
	}

	float val = texelFetch(vars, min(coords, ivec3(size)-1), 0).r;
	
	float final = (val - scales.x)/(scales.y - scales.x);
	gl_Position = mvp * vec4(in_vertex.z, final, in_vertex.x, 1.0);

}