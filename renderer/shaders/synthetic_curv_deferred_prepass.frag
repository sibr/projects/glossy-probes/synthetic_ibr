/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 450

#define DIFFUSE_CONFIDENCE -1

in vec3 worldPos;
in vec3 worldNormal;
in vec2 uvs;
in vec4 minc;
in vec4 maxc;
in flat float mid;

uniform vec3 camPos;
layout(binding = 0) uniform sampler2D lightmap;

layout(location = 0) out vec4 outDirMat;
layout(location = 1) out vec4 outPosConf;
layout(location = 2) out vec4 outNormalRoughMat;
layout(location = 3) out vec4 outMinc;
layout(location = 4) out vec4 outMaxc;

void main(void) {
	vec4 rgbsmooth = texture(lightmap, vec2(uvs.x, 1.0-uvs.y));
	// Instead of storing the normal we store the reflected direction directly.
	vec3 wo = normalize(worldPos - camPos);
	vec3 n = normalize(worldNormal);
	vec3 refl = reflect(wo, n);
	float curv = length(fwidth(n))/length(fwidth(worldPos));
	// Adjust the threshold based on the curvature intensity.
	// From experimental measurements, curvature value is in 0.0, 0.5
	float confidence = 1.0-clamp(curv/0.5, 0.0, 1.0);
	// Retrieve smoothness and adjust confidence.
	float smoothnessWeight = rgbsmooth.a*rgbsmooth.a;
	smoothnessWeight *= smoothnessWeight;
	confidence = clamp(confidence * smoothnessWeight, 0.0, 1.0);

	// binary flag: if input is diffuse, confidence gets special value
	if (rgbsmooth.a == 0) confidence = DIFFUSE_CONFIDENCE;

	outDirMat = vec4(refl, mid);
	outPosConf = vec4(worldPos, confidence);
	// Retrieve the real roughness from our packed smoothness
	float smoothness = pow(rgbsmooth.a, 2.2);
	float roughness = smoothness == 0.0 ? 0.99 : (1.0-((smoothness * 255.0)-1.0)/254.0);
	// Pack roughness and material ID in a float.
	 // mid id an int, smooth is a 0.0-1.0.
	outNormalRoughMat = vec4(n, clamp(roughness, 0.0, 0.99) + mid*10.0);
	
	// Output renormalized curvature info: min and max dir.
	vec3 newMax = normalize(maxc.xyz);
	vec3 tempN  = normalize(minc.xyz);
	vec3 fixedMin = normalize(cross(newMax, tempN));
	vec3 fixedMax = normalize(cross(tempN, fixedMin));

	outMinc = vec4(fixedMin, minc.a);
	outMaxc = vec4(fixedMax, maxc.a);

}