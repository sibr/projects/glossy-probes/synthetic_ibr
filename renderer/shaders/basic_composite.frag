/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define DIFFUSE 1
#define SPECULAR 2

in vec2 texcoord;

layout(binding = 0) uniform sampler2D diffuse;
layout(binding = 1) uniform sampler2D specular;
layout(binding = 2) uniform sampler2D groundtruth;

uniform int mode;
uniform float exposure = 1.0f;
uniform bool clampToLDR = true;


layout(location= 0) out vec4 out_color;

/** Composite specific layers. */
void main(void) {
	vec3 baseColor = vec3(0.0);
    if(bool(mode & DIFFUSE)){
		baseColor += textureLod(diffuse, texcoord, 0.0).rgb;
	}
	if(bool(mode & SPECULAR)){
		baseColor += textureLod(specular, texcoord, 0.0).rgb;
	}
	
	// Exposure
	if(!clampToLDR){
		baseColor = 1.0 - exp(-exposure * baseColor);
	}
	// Gamma correction.
	out_color.rgb = pow(baseColor, vec3(1.0/2.2));
	out_color.a = 1.0;
}
