/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(binding = 0) uniform sampler2D inputTexture;			// linear filtering
layout(binding = 1) uniform sampler2D stdDevAndDirTexture;	// linear filtering, 2D std dev along principal axes, 2D principal axis direction 
layout(binding = 2) uniform sampler2D jointInputTexture0;	// linear filtering
layout(binding = 3) uniform sampler2D jointInputTexture1;	// linear filtering

const int filterRadius = 20;				// filter radius, large enough to cover areas of low confidence, decoupled from domain std devs!
const float rangeStdDev = 0.001;				// range std dev
uniform bool primaryDirection;			// has to be true for every other pass
uniform bool useConfidence;				// enable confidence processing
const int numIterations = 2;			// total number of iterations, set to 2


layout(location = 0) out vec4 outTexture2D;

//==========================================================

#define EPS 10e-30
#define M_PI 3.14159265358979323846

//==========================================================

float gaussian(const float x, const float mu, const float sigma)
{
	return 1.0 / (sigma * sqrt(2.0 * M_PI)) * exp((-0.5 *((x - mu) * (x - mu))) / (sigma * sigma));
}

float gaussian(const float x, const float sigma)
{
	return gaussian(x, 0.0, sigma);
}

float gaussianCentered2(const float x2, const float sigma)
{
	return 1.0 / (sigma * sqrt(2.0 * M_PI)) * exp((-0.5 *(x2)) / (sigma * sigma));
}
//==========================================================

// variances should add to 1
float stdDevFactor(const int numIterations)
{
	return 1. / sqrt(numIterations);
}

void main()
{

	ivec2 coord2D = ivec2(gl_FragCoord.xy);
	// Fetch features and stddev/directions.
	vec4 jointReferenceTexel0 = texelFetch(jointInputTexture0, coord2D, 0);
	vec3 jointReferenceTexel1 = normalize(texelFetch(jointInputTexture1, coord2D, 0).xyz);
	ivec2 resolution = textureSize(inputTexture, 0);
	
	vec4 domainStdDevsAndDir =  texelFetch(stdDevAndDirTexture, coord2D, 0);
	
	vec2 domainStdDevs = domainStdDevsAndDir.xy;
	float domainStdDev = (primaryDirection) ? domainStdDevs.x : domainStdDevs.y;
	domainStdDev *= stdDevFactor(numIterations);
	domainStdDev = max(domainStdDev, EPS);

	vec2 direction = (domainStdDevsAndDir.zw);
	if (!primaryDirection){
		direction = vec2(direction.y, -direction.x);
	}

	outTexture2D = vec4(0.0);
	float weightSum = 0.0;
	// Blur along one axis.
	for(int d = -filterRadius; d <= filterRadius; d++)
	{
		vec2 texCoord = (coord2D + vec2(0.5) + float(d) * direction) / resolution;
		if (any(lessThan(texCoord, vec2(0.0))) || any(greaterThanEqual(texCoord, vec2(1.0)))) continue;
			
		vec4 texel = textureLod(inputTexture, texCoord, 0.0);
		vec4 jointTexel0 = textureLod(jointInputTexture0, texCoord, 0.0);
		vec3 jointTexel1 = normalize(textureLod(jointInputTexture1, texCoord, 0.0).xyz);
		float confidence = (useConfidence) ? abs(texel.a) : 1.0;

		vec3 dPos = jointReferenceTexel0.xyz - jointTexel0.xyz;
		float dRangeSq = min(dot(dPos, dPos), 1.0) 
				+ max(1.0-dot(jointReferenceTexel1, jointTexel1.xyz), 0.0);
		float binStep = float(round(jointReferenceTexel0.a) == round(jointTexel0.a));
		float rangeWeight = binStep * (1.0-step(rangeStdDev, dRangeSq));

		float domainWeight = gaussian(abs(d), domainStdDev);
			
		float weight = confidence * rangeWeight * domainWeight;
		weight = max(weight, EPS);
			
		outTexture2D += weight * texel;
		weightSum += weight;
	}

	outTexture2D /= weightSum;
}
