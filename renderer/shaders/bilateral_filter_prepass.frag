/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in vec2 texcoord;

layout(binding = 0) uniform sampler2D posConfMap;
layout(binding = 1) uniform sampler2D normalRoughMatMap;
layout(binding = 2) uniform sampler2D reflectedPos;
layout(binding = 3) uniform sampler3D lutVariance;
layout(binding = 4) uniform sampler2D distanceMap;

uniform vec3 minsLUT;
uniform vec3 maxsLUT;
uniform float sceneScale;
uniform float sizePixelWorldNovel;
uniform vec3 camPos;
uniform vec3 camDir;
uniform vec3 camRight;
uniform vec3 camUp;
uniform mat4 camNorMVP;
uniform float roughnessScale;

layout(location = 0) out vec4 stdDevDirs;
layout(location = 1) out vec4 jointVector0;
layout(location = 2) out vec4 jointVector1;

void main(){
	
	// We assume src and dst have the same res.
	ivec2 coords = ivec2(gl_FragCoord.xy);

	// Read geometric info.
	vec4 posAndConf = texelFetch(posConfMap, coords, 0);
	vec3 worldPos = posAndConf.xyz;
	float conf = posAndConf.w;
	vec4 t = texelFetch(normalRoughMatMap, coords, 0);
	vec3 normal = normalize(t.xyz);
	float rough = max(fract(t.a), 0.001); 
	float material = floor(t.a/10.0);
	vec3 reflPos = texelFetch(reflectedPos, coords, 0).xyz;
	// ouotput features for the guide.
	jointVector0 = vec4(worldPos/sceneScale, material);
	jointVector1 = vec4(normal, 0.0);

	float mainDepth = distance(camPos, worldPos)/sceneScale;
	float reflDepth = distance(reflPos, worldPos)/sceneScale;

	const float minScale = -0.0;
	const float thDec = 1.0;
	float valueDec = reflDepth;
	// Bias roughness at low distance for contact sharpening.
	float decFac = clamp(valueDec < thDec ? (minScale + (1.0 - minScale)/thDec * valueDec) : 1.0, 0.0, 1.0);
	
	// Determine the stddev required as sqrt(var(rough) - var(rough*0.5))
	// this requires the variance to be independent from the resolution and field of view.
	// For now, we assume it was generated for a 1024 image at 70* fovy.
	const float sizePixelWorldRef = 2.0*tan(70.0*3.14159/180.0/2.0)/1024.0;

	// Compute the UV in the LUT for the full and half roughness.
	vec3 lutUVFull = vec3(rough, reflDepth, mainDepth);
	lutUVFull = clamp((lutUVFull - minsLUT)/(maxsLUT - minsLUT), 0.0, 1.0);
	vec3 lutUVHalf = vec3(rough*roughnessScale, reflDepth, mainDepth);
	lutUVHalf = clamp((lutUVHalf - minsLUT)/(maxsLUT - minsLUT), 0.0, 1.0);
	// Inverse power sampling.
	lutUVFull = (sqrt(lutUVFull));
	lutUVHalf = (sqrt(lutUVHalf));
	
	// Read both variances.
	float varFull = decFac*textureLod(lutVariance, lutUVFull, 0.0).r;
	float varHalf = decFac*textureLod(lutVariance, lutUVHalf, 0.0).r;
	// Derive blur radius.
	float deltaVar = max(varFull - varHalf, 0.0);
	float deltaSigma = sqrt(deltaVar);
	deltaSigma *= sizePixelWorldRef;
	deltaSigma /= sizePixelWorldNovel;
	// Clamp to half a pixel.
	deltaSigma = max(deltaSigma, 0.5);
	// Take into account grazing angle forshortening.
	vec3 viewDir = normalize(camPos - worldPos);
	float shorten = clamp(dot(normal, viewDir), 0.1, 1.0);
	
	// We also need the principal directions of the ellipse on the image plane.
	// the small axis is the projection of the normal onto the image plane.
	// the big axis is orthogonal, still in the image plane
	vec4 projN = camNorMVP * vec4(normal, 0.0);
	// Shorten the minor axis.
	// We don't have anything for the intrinsic curvature of the surface though.
	// Reclamp to half a pixel.
	stdDevDirs.xy = max(deltaSigma * vec2(1.0, shorten), 0.5);
	
	// Technically there is a pinch if the normal is orthogonal.
	stdDevDirs.zw = normalize(vec2(projN.y, -projN.x));
	
}