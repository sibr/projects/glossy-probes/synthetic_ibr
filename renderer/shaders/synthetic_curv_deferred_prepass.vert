/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(location = 0) in vec3 in_vertex;
//layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_uvs;
layout(location = 3) in vec3 in_normal;
layout(location = 4) in vec4 in_minc;
layout(location = 5) in vec4 in_maxc;
layout(location = 6) in float in_mat;

uniform mat4 mvp;

out vec3 worldPos;
out vec3 worldNormal;
out vec2 uvs;
out vec4 minc;
out vec4 maxc;
out flat float mid;

void main(void) {
	gl_Position = mvp * vec4(in_vertex,1.0);
	// World space info.
	worldPos = in_vertex.xyz;
	worldNormal = in_normal;
	uvs = in_uvs;
	minc = in_minc;
	maxc = in_maxc;
	mid = in_mat;
}
