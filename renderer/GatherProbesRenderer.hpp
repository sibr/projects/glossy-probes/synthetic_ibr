/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"

# include <core/system/Config.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/renderer/RenderMaskHolder.hpp>
# include <core/graphics/GPUQuery.hpp>

#include "GeometryInfos.hpp"
#include "SyntheticIBRComponents.hpp"

namespace sibr { 

	/** Main renderer for Glossy Probe Reprojection for Interactive Global Illumination.
	 */
	class  GatherProbesRenderer
	{
		SIBR_CLASS_PTR(GatherProbesRenderer);
	public:

		/**
		 * Constructor.
		 */
		GatherProbesRenderer(ProbeLocations::Ptr probes);

		/** Render the gbuffer. */
		void processGbuffer(const Camera& eye, const GeometryInfos::Ptr& proxy, const Texture2DRGBA::Ptr & lightmap);

		/**
		 * Performs rendering to a given destination rendertarget.
		 */
		void process(
			const sibr::Camera& eye,
			const std::vector<int> & indices,
			const std::vector<float> & coords,
			IRenderTarget& dst,
			const Texture2DArrayRGB16F::Ptr & inputRGBs,
			const  sibr::Texture2DArrayRGB16F::Ptr & reflectedMaps,
			const  sibr::Texture2DArrayUV16F::Ptr& warpMaps,
			const  sibr::Texture2DArrayUV16F::Ptr& diffWarpMaps,
			const ProbeLocations::Ptr & probes, 
			const GeometryInfos::Ptr & geometry,
			const Texture2DArrayInt3::Ptr & geomMaps,
			const RenderTargetRGBA32F::Ptr& reflectedPos
			);

		void loadShaders(bool restore);

		const RenderTargetRGBA32F::Ptr & prepassRT() const {
			return _prepass;
		}

		void resize(uint w, uint h);

		struct DebugParameters {
			GLuniform<Vector4f> thresholds;
			GLuniform<Vector3f> scaleSteps;
			int probeLocalId = -1;

			GLuniform<float> surfTh;
			GLuniform<float> maxPosError;
			GLuniform<float> surfAdj;
			GLuniform<float> posAdj;
			GLuniform<float> lobeAdj;
			GLuniform<float> errorScale; 
			GLuniform<float> refmatPenal; 
		};

		float & sceneScale() {
			return _gatherScale.get();
		}

		sibr::Vector3i & radii() {
			return _rads;
		}

		DebugParameters & debug() {
			return _debug;
		}
		
	protected:

		void updateProbesSelection(const std::vector<int>& probeIds, const std::vector<float>& coords);

		// G-buffer prepass.
		RenderTargetRGBA32F::Ptr _prepass;
		GLShader			_prepassShader;
		GLuniform<Matrix4f> _prepassMvp;
		GLuniform<Vector3f> _prepassEye;


		GLShader			_gatherShader;
		GLuniform<Vector3f> _gatherEye;
		GLuniform<float> _gatherScale;
		DebugParameters _debug;
		sibr::Vector3i _rads = { 3, 1, 0 };

		/// Probes infos data structure shared between the CPU and GPU.
		/// We have to be careful about alignment if we want to send those struct directly into the UBO.
		struct ProbesUBOInfos {
			Vector3f pos;
			float weight = 0.0f;
			int selected = 0;
			float d0, d1, d3;
		};
		std::vector<ProbesUBOInfos> _probesInfos;
		GLuint _uboIndex;

		RenderTargetRGBA32F::Ptr _prevFrame;
		GLuniform<Matrix4f> _prevMVP;
		
	};


} /*namespace sibr*/ 

