/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "GatherProbesRenderer.hpp"


sibr::GatherProbesRenderer::GatherProbesRenderer(ProbeLocations::Ptr probes)
{
	// Copy probes data.
	const auto& locs = probes->locations();
	_probesInfos.clear();
	_probesInfos.resize(locs.size());
	for (size_t i = 0; i < locs.size(); ++i) {
		_probesInfos[i].pos = locs[i];
		_probesInfos[i].selected = 0;
	}
	// Create UBO.
	_uboIndex = 0;
	glGenBuffers(1, &_uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(ProbesUBOInfos) * _probesInfos.size(), &_probesInfos[0], GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// Setup sahders.
	loadShaders(false);

	// RTs will be resized at the same dimensions as the destination RT on the fly.
	resize(1920,1080);

}


void sibr::GatherProbesRenderer::loadShaders(bool restore) {
	// Copy shaders.
	const std::string shaderSrcPath = sibr::getBinDirectory() + "/../../src/projects/synthetic_ibr/renderer/shaders/";
	const std::string shaderDstPath = sibr::getShadersDirectory("synthetic_ibr") + "/";
	const auto files = sibr::listFiles(shaderSrcPath, false, false, {"vert", "frag", "geom"});
	for (const auto & file : files) {
		sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
	}


	const sibr::Vector4f ths = restore ? _debug.thresholds : sibr::Vector4f( 0.1f, 0.1f, 1.0f, 1.0f );
	const sibr::Vector3f stpscls = restore ? _debug.scaleSteps : sibr::Vector3f(4.0f, 2.0f, 1.0f);
	
	_prepassShader.init("Prepass",
		sibr::loadFile(shaderDstPath + "synthetic_curv_deferred_prepass.vert"),
		sibr::loadFile(shaderDstPath + "synthetic_curv_deferred_prepass.frag"));
	_prepassMvp.init(_prepassShader, "mvp");
	_prepassEye.init(_prepassShader, "camPos");

	const GLShader::Define def("PROBES_COUNT", _probesInfos.size());
	const GLShader::Define radLDef("RADIUS_L", _rads[0]);
	const GLShader::Define radSDef("RADIUS_S", _rads[1]);
	const GLShader::Define radFDef("FINAL_RADIUS", _rads[2]);
	_gatherShader.init("Gather",
		sibr::loadFile(shaderDstPath + "synthetic_blendcommon.vert"),
		sibr::loadFile(shaderDstPath + "synthetic_gatherprobes.frag", { def, radLDef, radSDef, radFDef }));
	_gatherEye.init(_gatherShader, "camPos");
	_gatherScale.init(_gatherShader, "sceneScale");
	_debug.thresholds.init(_gatherShader, "thresholds");
	_debug.thresholds = ths;
	
	_debug.scaleSteps.init(_gatherShader, "stepsScale");
	_debug.scaleSteps = stpscls;

	_debug.surfTh.init(_gatherShader, "surfTh");
	
	_debug.maxPosError.init(_gatherShader, "maxPosError");
	_debug.surfAdj.init(_gatherShader, "surfAdj");
	_debug.posAdj.init(_gatherShader, "posAdj");
	_debug.lobeAdj.init(_gatherShader, "lobeAdj");
	_debug.errorScale.init(_gatherShader, "errorScale");
	_debug.refmatPenal.init(_gatherShader, "refmatPenal");
	
	if (!restore) {
		_debug.surfTh = 0.8f;
		_debug.maxPosError = 1.0f;
		_debug.surfAdj = 2.5f;
		_debug.posAdj = 2.5f;
		_debug.lobeAdj = 2.5f;
		_debug.errorScale = 8.0f;
		_debug.refmatPenal = 10.0f;
	}


	_prevMVP.init(_gatherShader, "prevMVP");
	_prevMVP = sibr::Matrix4f::Identity();
}


void sibr::GatherProbesRenderer::processGbuffer(const Camera& eye, const GeometryInfos::Ptr& proxy,
	const Texture2DRGBA::Ptr & lightmap) {
	

	// Render a fat gbuffer, see synthetic_curv_deferred_prepass.frag for the detail of the output.
	glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Deferred prepass");
	
	_prepass->clear();
	_prepass->bind();
	glViewport(0, 0, _prepass->w(), _prepass->h());
	
	_prepassShader.begin();
	_prepassMvp.set(eye.viewproj());
	_prepassEye.set(eye.position());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, lightmap->handle());
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	proxy->draw();

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDepthFunc(GL_LESS);

	_prepassShader.end();
	_prepass->unbind();
	
	glPopDebugGroup();
}

void sibr::GatherProbesRenderer::resize(uint w, uint h) {
	SIBR_LOG << "[SplatProbesRenderer] Resizing at " << w << "x" << h << "." << std::endl;
	_prepass.reset(new RenderTargetRGBA32F(w, h, 0, 5));
	_prevFrame.reset(new RenderTargetRGBA32F(w, h, 0, 1));
	_prevFrame->clear();
}

void sibr::GatherProbesRenderer::updateProbesSelection(const std::vector<int>& probeIds, const std::vector<float>& coords) {
	// Reset all cameras.
	for (auto& probesInfos : _probesInfos) {
		probesInfos.selected = 0;
		probesInfos.weight = 0.0f;
	}
	// Enabled the ones passed as indices.
	for (size_t lid = 0; lid < probeIds.size(); ++lid) {
		if (_debug.probeLocalId != -1 && lid != _debug.probeLocalId) {
			continue;
		}
		_probesInfos[probeIds[lid]].selected = 1;
		_probesInfos[probeIds[lid]].weight = coords[lid];
	}

	// Update the content of the UBO.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ProbesUBOInfos) * _probesInfos.size(), &_probesInfos[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}


void sibr::GatherProbesRenderer::process(
	const sibr::Camera & eye,
	const std::vector<int> & indices,
	const std::vector<float> & coords,
	IRenderTarget & dst,
	const sibr::Texture2DArrayRGB16F::Ptr & inputRGBs,
	const  sibr::Texture2DArrayRGB16F::Ptr & reflectedMaps,
	const  sibr::Texture2DArrayUV16F::Ptr& warpMaps,
	const  sibr::Texture2DArrayUV16F::Ptr& diffWarpMaps,
	const ProbeLocations::Ptr & probes, const GeometryInfos::Ptr & geometry,
	const Texture2DArrayInt3::Ptr & geomMaps, const RenderTargetRGBA32F::Ptr & reflectedPos) {

	// Check size of intermediate targets.
	if(dst.w() != _prepass->w() || dst.h() != _prepass->h()) {
		resize(dst.w(), dst.h());
	}

	// Update probe flags.
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	updateProbesSelection(indices, coords);

	// Gathering in final view.
	glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, "Gathering");
	dst.clear();
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	_gatherShader.begin();

	// Uniforms.
	_gatherEye.set(eye.position());
	_gatherScale.send();
	_debug.thresholds.send();
	_debug.scaleSteps.send();

	_debug.surfTh.send();
	_debug.maxPosError.send();
	_debug.surfAdj.send();
	_debug.posAdj.send();
	_debug.lobeAdj.send();
	_debug.errorScale.send();
	_debug.refmatPenal.send();

	_prevMVP.send();
	// Textures.
	// Prepass output.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _prepass->handle(0));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _prepass->handle(1));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _prepass->handle(2));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _prepass->handle(3));
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, _prepass->handle(4));
	// Optix output.
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, reflectedPos->handle(0));
	// Probe texture arrays.
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputRGBs->handle());
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D_ARRAY, reflectedMaps->handle());
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_2D_ARRAY, warpMaps->handle());
	glActiveTexture(GL_TEXTURE9);
	glBindTexture(GL_TEXTURE_2D_ARRAY, geomMaps->handle());
	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D_ARRAY, diffWarpMaps->handle());
	// Previous frame.
	glActiveTexture(GL_TEXTURE11);
	glBindTexture(GL_TEXTURE_2D, _prevFrame->handle());
	// Bind UBO to shader, after all possible textures.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBindBufferBase(GL_UNIFORM_BUFFER, 10, _uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	// Additional geometry info.
	geometry->bind(0);

	// Gathering...
	RenderUtility::renderScreenQuad();

	geometry->unbind(0);
	_gatherShader.end();
	dst.unbind();

	glPopDebugGroup();

	// Copy the previous frame result.
	glNamedFramebufferReadBuffer(dst.fbo(), GL_COLOR_ATTACHMENT1);
	glBlitNamedFramebuffer(
		dst.fbo(), _prevFrame->fbo(),
		0, 0, dst.w(), dst.h(),
		0, 0, _prevFrame->w(), _prevFrame->h(),
		GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glNamedFramebufferReadBuffer(dst.fbo(), GL_COLOR_ATTACHMENT0);

	_prevMVP = eye.viewproj();
}
