/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include "Config.hpp"

#include "SyntheticIBRComponents.hpp"
#include <core/renderer/PoissonRenderer.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/scene/BasicIBRScene.hpp>

#include <core/scene/ProxyMesh.hpp>
#include "GeometryInfos.hpp"

namespace sibr {

	/** Scene with a set of adaptive probe for Glossy Probe Reprojection for Interactive Global Illumination. */
	class SIBR_SYNTHETIC_IBR_EXPORT ProbesGatherScene {

		SIBR_DISALLOW_COPY(ProbesGatherScene);
		SIBR_CLASS_PTR(ProbesGatherScene);

	public:
		
		ProbesGatherScene(SyntheticAppArgs & myArgs);

		ProxyMesh::Ptr proxies() const  { return _proxies; }

		ProbeLocations::Ptr probes() const { return _probes; }

		Texture2DRGBA::Ptr lightMap() const { return _lightMap;  }

		const Raycaster::Ptr& raycaster();
		
		const std::string & basePath() const { return _basePath;  }

		const std::string & name() const { return _name; }

		const Texture2DArrayRGB16F::Ptr & reflectedMaps() const {
			return _reflectedMaps;
		}

		const Texture2DArrayRGB16F::Ptr& colorMaps() const {
			return _colorMaps;
		}

		const Texture2DArrayUV16F::Ptr& warpMaps() const {
			return _warpMaps;
		}

		const Texture2DArrayUV16F::Ptr& diffWarpMaps() const {
			return _diffWarpMaps;
		}

		float scale() const {
			return _scale;
		}

		const GeometryInfos::Ptr  & geometry() const {
			return _geomInfos;
		}

		const Texture2DArrayInt3::Ptr & geomMaps() const {
			return _geomMaps;
		}

		const sibr::Vector2u & size() const {
			return _size;
		}

		/** Load the geoemtry into a custom CPU/GPU representation, including material ID and curvature info. */
		static Mesh::Ptr loadGeometry(const std::string& basePath, GeometryInfos::Ptr& geomInfos);

		/** Load a lightmap, combining diffuse and material information in a packed texture and a color image. */
		static Texture2DRGBA::Ptr loadLightmap(const std::string& basePath, ImageRGBA& lightImg);
		
	private:

		void setupFromPath(const std::string& basePath, const std::string& name);

		sibr::Vector2u _size;
		ProxyMesh::Ptr _proxies;
		ProbeLocations::Ptr _probes;
		Texture2DRGBA::Ptr _lightMap;
		Raycaster::Ptr _raycaster;

		GeometryInfos::Ptr _geomInfos;

		Texture2DArrayRGB16F::Ptr _reflectedMaps;
		Texture2DArrayInt3::Ptr   _geomMaps;
		Texture2DArrayRGB16F::Ptr   _colorMaps;
		Texture2DArrayUV16F::Ptr   _warpMaps;
		Texture2DArrayUV16F::Ptr   _diffWarpMaps;
		
		std::string _basePath;
		std::string _name;
		float _scale;
	};

}
