/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include "Config.hpp"

# include <core/system/Config.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
#include <core\assets\InputCamera.hpp>

namespace sibr { 

	/** Fast adaptive bilateral filter that takes into account the surface roughness
	 * to replicate the BRDF effect in screenspace.
	 */
	class SIBR_SYNTHETIC_IBR_EXPORT BilateralFilterRenderer
	{
		SIBR_CLASS_PTR(BilateralFilterRenderer);

	public:

		/**
		 *\param w render width
		 *\param h render height
		 *\param sceneScale the scene scale in meters
		 * \param roughnessScale the scaling factor of the roughness used during probes precomputation
		 * \param lutPath path to the variance look-up table on disk
		 */
		BilateralFilterRenderer(uint w, uint h, float sceneScale, float roughnessScale, const std::string& lutPath);

		void process(const InputCamera& eye, const IRenderTarget& src, const IRenderTarget& gbuffer, const RenderTargetRGBA32F::Ptr& reflectedPos, IRenderTarget& dst);

		void loadShaders();

	protected:

		/** Load the lookup table. */
		void prepareLookupTable(const std::string& path);

		void resize(uint w, uint h);

		GLShader _prepass;
		GLuniform<sibr::Matrix4f> _camMVP;
		GLuniform<sibr::Vector3f> _camPos;
		GLuniform<sibr::Vector3f> _camDir;
		GLuniform<sibr::Vector3f> _camUp;
		GLuniform<sibr::Vector3f> _camRight;
		GLuniform<sibr::Vector3f> _mins;
		GLuniform<sibr::Vector3f> _maxs;
		GLuniform<float> _sceneScale;
		GLuniform<float> _roughnessScale;
		GLuniform<float> _sizePixel;
		GLuint _tex3D;

		RenderTargetRGBA32F::Ptr _params;

		GLShader _filter;
		GLuniform<bool> _filterPrimaryDir;
		GLuniform<bool> _filterUseConfidence;

		RenderTargetRGBA32F::Ptr _ping, _pong;
		
	};


} /*namespace sibr*/ 

