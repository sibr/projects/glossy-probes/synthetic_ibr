/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

# include "Config.hpp"

#include <core/scene/BasicIBRScene.hpp>

namespace sibr {


	/** Store geometric information as a GPU mesh and a set of buffers directly accessible from a shader.
	 * Includes application specific data such as curvatures and materials. 
	 */
	class SIBR_SYNTHETIC_IBR_EXPORT GeometryInfos {
		SIBR_CLASS_PTR(GeometryInfos);
		
	public:

		GeometryInfos(const std::vector<sibr::Vector3f>& positions, const std::vector<sibr::Vector3f>& normals, const std::vector<sibr::Vector2f>& texcoords,
			const std::vector<sibr::Vector4f>& mincs, const std::vector<sibr::Vector4f>& maxcs, const std::vector<uint>& mats, const std::vector<sibr::Vector3u>& tris);

		/** Bind SSBOs.
		 * \param startLoc the location of the first slot to use
		 */
		void bind(size_t startLoc);
		
		/** Unbind SSBOs.
		 * \param startLoc the location of the first slot to unbind
		 */
		void unbind(size_t startLoc);

		/** Draw the mesh. */
		void draw() const;
		
	private:

		enum AttribLocation
		{
			VertexAttribLocation = 0,
			TexCoordAttribLocation = 2,
			NormalAttribLocation = 3,
			MinCurvAttribLocation = 4,
			MaxCurvAttribLocation = 5,
			MaterialAttribLocation = 6,
			AttribLocationCount
		};
		
		// SSBO infos.
		GLuint posBuff;
		GLuint mincBuff;
		GLuint maxcBuff;
		GLuint matBuff;
		GLuint triBuff;
		GLuint norBuff;
		// VAO infos.
		GLuint _vaoId; ///< Vertex array object ID.
		std::array<GLuint, 2> _bufferIds; ///< Buffers IDs.
		uint _indexCount; ///< Number of elements in the index buffer.
		uint _vertexCount; ///< Number of elements in the vertex buffer.
		
	};
}
