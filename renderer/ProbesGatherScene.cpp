/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ProbesGatherScene.hpp"
#include "core/raycaster/CameraRaycaster.hpp"
#include "core/graphics/MaterialMesh.hpp"
#include "MeshOBJExporter.hpp"
#include <array>

namespace sibr {

	

	ProbesGatherScene::ProbesGatherScene(SyntheticAppArgs & myArgs)
	{
		const std::string basePath = myArgs.dataset_path.get();
		_name = myArgs.sceneSuffix.get();
		_basePath = basePath;
		setupFromPath(basePath, _name);
	}

	void ProbesGatherScene::setupFromPath(const std::string & basePath, const std::string & name) {

		_proxies.reset(new ProxyMesh());
		_probes.reset(new ProbeLocations());

		// Load scene scale.
		_scale = syntheticIBR::loadScale(basePath);
		// Load geometry.
		SIBR_LOG << "[SyntheticScene] Loading geometry..." << std::flush;
		_proxies->replaceProxyPtr(loadGeometry(basePath, _geomInfos));
		std::cout << "Done." << std::endl;
		
		// Load lightmap.
		ImageRGBA::Ptr lightImg(new ImageRGBA());
		_lightMap = loadLightmap(basePath, *lightImg);

		// Load probe locations.
		_probes->setupFromPath(basePath + "/probes/" + name + ".positions");

		// Load panoramas.
		SIBR_LOG << "[SyntheticScene] Loading specular probes..." << std::endl;
		std::vector<sibr::ImageRGB32F::Ptr> images;
		{
			const std::string imagesDir = basePath + "/images/" + name + "/";
			auto imgFiles = sibr::listFiles(imagesDir, false, false, { "bin" });
			images.resize(imgFiles.size());
	#pragma omp parallel for
			for (int fid = 0; fid < int(imgFiles.size()); ++fid) {
				const auto& imgFile = imgFiles[fid];
				images[fid].reset(new ImageRGB32F());
				images[fid]->loadByteStream(imagesDir + "/" + imgFile, false);
			}
		}
		_size = images[0]->size();
		_colorMaps.reset(new Texture2DArrayRGB16F({ images }, SIBR_FLIP_TEXTURE));
		std::cout << " Done." << std::endl;

		// Load geometric probe info.
		const std::string flowPath = basePath + "/flows/" + name + "/";
		const size_t pCount = _probes->locations().size();
		std::vector<ImageUV8::Ptr> idImgs(pCount);
		std::vector<ImageRGB32F::Ptr> reflectedImgs(pCount);
		std::vector<ImageInt3::Ptr> geomImgs(pCount);
		
		{
			SIBR_LOG << "[SyntheticScene] Loading maps..." << std::endl;
			// Get position map filenames.
			const std::vector<std::string> mapFiles = sibr::listFiles(flowPath + "/intersection/", false, false, { "bin" });
#pragma omp parallel for
			for (int i = 0; i < mapFiles.size(); ++i) {
				const auto & file = mapFiles[i];
				// Get the camera name.
				std::string srcName = file.substr(3);
				srcName = srcName.substr(0, srcName.find_last_of('.'));
				const size_t srcId = std::stol(srcName);
				reflectedImgs[srcId].reset(new ImageRGB32F());
				reflectedImgs[srcId]->loadByteStream(flowPath + "/reflected/" + file, false);
				idImgs[srcId].reset(new ImageUV8());
				idImgs[srcId]->loadByteStream(flowPath + "/matid/" + file, false);
				// Pack reflected material ID info with the geometry map.
				ImageInt3 geomImg;
				geomImg.loadByteStream(flowPath + "/intersection/" + file, false);
				ImageUV8 refIdImg;
				refIdImg.loadByteStream(flowPath + "/reflectedmatid/" + file, false);
				geomImgs[srcId].reset(new ImageInt3(geomImg.w(), geomImg.h()));
#pragma omp parallel for
				for (int y = 0; y < int(geomImg.h()); ++y) {
					for (int x = 0; x < int(geomImg.w()); ++x) {
						const sibr::Vector2ub& id = refIdImg(x, y);
						const int iid = int(id[0]) + (int(id[1]) << 8);
						const sibr::Vector3i& gg = geomImg(x, y);
						geomImgs[srcId](x, y)[0] = gg[0];
						// Pack the two float uvs together.
						float b0, b1;
						std::memcpy(&b0, &gg[1], sizeof(float));
						std::memcpy(&b1, &gg[2], sizeof(float));
						// Replace the third one by the reflected id.
						const int maxUV = 1 << 15;
						const int uv0 = sibr::clamp(int(std::round(b0 * float(maxUV))), 0, maxUV);
						const int uv1 = sibr::clamp(int(std::round(b1 * float(maxUV))), 0, maxUV);
						const int packed = ((uv0 & 0xffff) << 16) | (uv1 & 0xffff);
						geomImgs[srcId](x, y)[1] = packed;
						geomImgs[srcId](x, y)[2] = iid;
					}
				}
			}
			std::cout << " Done." << std::endl;
		}

		{
			SIBR_LOG << "[SyntheticScene] Updating visibility..." << std::flush;
			// Mark samples to skip in geometry map.
	#pragma omp parallel for
			for (int fid = 0; fid < pCount; ++fid) {
				const auto & img = *images[fid];
	#pragma omp parallel for
				for (int y = 0; y < int(img.h()); ++y) {
					for (int x = 0; x < int(img.w()); ++x) {
						const sibr::Vector2ub & id = idImgs[fid](x, y);
						if (id[0] == 0 && id[1] == 0) {
							geomImgs[fid](x, y)[0] = -1;
						}
					}
				}
			}
			std::cout << " Done." << std::endl;

			SIBR_LOG << "[SyntheticScene] Uploading textures..." << std::flush;
			// We don't use extra mipmaps anymore.
			const size_t mipCount = 1;
			std::vector<std::vector<ImageRGB32F::Ptr>> reflectedMips(mipCount);
			std::vector<std::vector<ImageInt3::Ptr>> geomMips(mipCount);
			std::vector<std::vector<ImageUV8::Ptr>> idMips(mipCount);
			reflectedMips[0] = reflectedImgs;
			geomMips[0] = geomImgs;
			idMips[0] = idImgs;
			_reflectedMaps = std::make_shared<Texture2DArrayRGB16F>(reflectedMips, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING);
			_geomMaps = std::make_shared<Texture2DArrayInt3>(geomMips, SIBR_FLIP_TEXTURE | SIBR_GPU_INTEGER);
			std::cout << "Done." << std::endl;
		}

		// Load the forward warping infos.
		{
			SIBR_LOG << "[SyntheticScene] Loading forward maps..." << std::endl;
			const std::string paramPath = basePath + "/warps/" + name + "/forward_flow/";
			std::vector<sibr::ImageFloat2::Ptr> warpMaps;
			warpMaps.resize(pCount);
			const auto warpFiles = sibr::listFiles(paramPath, false, false, { "exr" });
#pragma omp parallel for
			for (int i = 0; i < warpFiles.size(); ++i) {
				const auto& warpFile = warpFiles[i];
				// Extract the cam number.
				const std::string::size_type beg = warpFile.find_first_of("0123456789");
				const std::string::size_type end = warpFile.find_last_of("0123456789");
				const std::string idStr = warpFile.substr(beg, end - beg + 1);
				const int id = std::stoi(idStr);
				assert(id < warpMaps.size());
				warpMaps[id].reset(new ImageFloat2());
				warpMaps[id]->load(paramPath + "/" + warpFile, false);
			}
			_warpMaps.reset(new Texture2DArrayUV16F({ warpMaps }, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING));
			std::cout << " Done." << std::endl;
		}

		// Load backward warping for derivatives
		{
			const sibr::Vector2f probeSize = images[0]->size().cast<float>();
			SIBR_LOG << "[SyntheticScene] Loading backward maps ..." << std::endl;
			const std::string paramPath = basePath + "/warps/" + name + "/inverse_flow/";
			std::vector<sibr::ImageFloat2::Ptr> diffWarpMaps;
			diffWarpMaps.resize(pCount);
			const auto warpFiles = sibr::listFiles(paramPath, false, false, { "exr" });
#pragma omp parallel for
			for (int i = 0; i < warpFiles.size(); ++i) {
				const auto& warpFile = warpFiles[i];
				// Extract the cam number.
				const std::string::size_type beg = warpFile.find_first_of("0123456789");
				const std::string::size_type end = warpFile.find_last_of("0123456789");
				const std::string idStr = warpFile.substr(beg, end - beg + 1);
				const int id = std::stoi(idStr);
				assert(id < diffWarpMaps.size());
				ImageFloat2 wmap;
				wmap.load(paramPath + "/" + warpFile, false);
				const uint ww = wmap.w();
				const uint wh = wmap.h();
				ImageFloat2 fullRes(ww, wh, { 0.0f,0.0f });
				for (uint y = 0; y < wh; ++y) {
					for (uint x = 0; x < ww; ++x) {
						// Read values in a cross.
						// Wrap around X axis, clamp around Y axis.
						const int xm = x == 0 ? 0 : (x - 1);
						const int xp = x == (ww - 1) ? (ww - 1) : (x + 1);
						const int ym = y == 0 ? 0 : (y - 1);
						const int yp = y == (wh - 1) ? (wh - 1) : (y + 1);

						const sibr::Vector2f vxm = wmap(xm, y).cwiseProduct(probeSize);
						const sibr::Vector2f vxp = wmap(xp, y).cwiseProduct(probeSize);
						const sibr::Vector2f vym = wmap(x, ym).cwiseProduct(probeSize);
						const sibr::Vector2f vyp = wmap(x, yp).cwiseProduct(probeSize);

						const sibr::Vector2f vdx = (vxp - vxm) / (2.0f);
						const sibr::Vector2f vdy = (vyp - vym) / (2.0f);

						fullRes(x, y)[0] = vdx.norm();
						fullRes(x, y)[1] = vdy.norm();
					}
				}
				diffWarpMaps[i].reset(new ImageFloat2(fullRes.resized(fullRes.w() / 4, fullRes.h() / 4, cv::INTER_LINEAR)));

			}
			_diffWarpMaps.reset(new Texture2DArrayUV16F({ diffWarpMaps }, SIBR_FLIP_TEXTURE | SIBR_GPU_LINEAR_SAMPLING));
			std::cout << " Done." << std::endl;
		}

	}

	const Raycaster::Ptr & ProbesGatherScene::raycaster() {
		if(!_raycaster) {
			_raycaster.reset(new Raycaster());
			_raycaster->addMesh(_proxies->proxy());
		}
		return _raycaster;
	}

	Mesh::Ptr ProbesGatherScene::loadGeometry(const std::string& basePath, GeometryInfos::Ptr& geomInfos) {
		Mesh::Ptr mesh(new Mesh(true));
		MeshOBJHandler::loadOBJ(basePath + "/lightmap/scene_light_map.obj", *mesh);


		std::vector<uint> matIds;
		// Load material infos.
		{
			ByteStream materialData;
			materialData.load(basePath + "/lightmap/scene_light_map_ids.bin");
			size_t vsize = materialData.bufferSize() / sizeof(uint);
			matIds.resize(vsize);
			std::memcpy(matIds.data(), materialData.buffer(), materialData.bufferSize());

			std::vector<sibr::Vector3f> colors(vsize);
#pragma omp parallel for
			for (long vid = 0; vid < vsize; ++vid) {
				const uint matId = matIds[vid];
				if (matId <= 0 || matId > 255) {
					SIBR_LOG << "Unsupported material ID encountered: " << matId << "." << std::endl;
				}
				colors[vid][0] = float(matId);
				colors[vid][2] = colors[vid][1] = 0.0f;
			}
			mesh->colors(colors);
		}

		// Load other geometry info if it exists.
		if (sibr::fileExists(basePath + "/curvatures/scene_light_map_minc.bin")) {
			std::vector<sibr::Vector4f> mincs(mesh->vertices().size());
			std::vector<sibr::Vector4f> maxcs(mesh->vertices().size());
			ByteStream minData;
			minData.load(basePath + "/curvatures/scene_light_map_minc.bin");
			std::memcpy(mincs.data(), minData.buffer(), minData.bufferSize());
			ByteStream maxData;
			maxData.load(basePath + "/curvatures/scene_light_map_maxc.bin");
			std::memcpy(maxcs.data(), maxData.buffer(), maxData.bufferSize());

			geomInfos.reset(new GeometryInfos(mesh->vertices(), mesh->normals(), mesh->texCoords(), mincs, maxcs, matIds, mesh->triangles()));
		}
		return mesh;
	}


	Texture2DRGBA::Ptr ProbesGatherScene::loadLightmap(const std::string& basePath, ImageRGBA& lightImg) {

		lightImg.load(basePath + "/lightmap/scene_light_map.png", false, true);
		// Transfer roughness info.
		ImageRGBA lightmapRough;
		lightmapRough.load(basePath + "/lightmap/scene_light_map_mat.png", false, true);
#pragma omp parallel for
		for (int y = 0; y < int(lightmapRough.h()); ++y) {
			for (int x = 0; x < int(lightmapRough.w()); ++x) {
				lightImg(x, y)[3] = lightmapRough(x, y)[3];
			}
		}
		return std::make_shared<Texture2DRGBA>(lightImg, SIBR_GPU_LINEAR_SAMPLING);
	}

}
