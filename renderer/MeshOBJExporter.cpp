/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "MeshOBJExporter.hpp"


using namespace sibr;

bool MeshOBJHandler::saveOBJ(const Mesh & mesh, const std::string & filePath, const std::string & header) {
	std::ofstream file(filePath);
	if(!file.is_open()) {
		SIBR_ERR << "Unable to save mesh to " << filePath << std::endl;
		return false;
	}

	const auto & verts = mesh.vertices();
	const size_t vCount = verts.size();

	const auto & norms = mesh.normals();
	const size_t nCount = norms.size();

	const auto & uvs = mesh.texCoords();
	const size_t uCount = uvs.size();

	const auto & tris = mesh.triangles();
	const size_t tCount = tris.size();

	const bool hasN = mesh.hasNormals();
	const bool hasU = mesh.hasTexCoords();

	file << "# OBJ exported using SIBR custom handler." << "\n";
	file << "# " << vCount << " vertices, " << nCount << " normals, " << uCount << " texcoords, " << tCount << " faces." << "\n";
	file << header << "\n";

	for(const sibr::Vector3f & v : verts) {
		file << "v " << v[0] << " " << v[1] << " " << v[2] << "\n";
	}
	for (const sibr::Vector3f & n : norms) {
		file << "vn " << n[0] << " " << n[1] << " " << n[2] << "\n";
	}
	for (const sibr::Vector2f & uv : uvs) {
		file << "vt " << uv[0] << " " << uv[1] << "\n";
	}
	for (const sibr::Vector3u & t : tris) {
		file << "f ";
		for(int i = 0; i < 3; ++i) {
			const std::string ind = std::to_string(t[i]+1);
			if(hasN && hasU) {
				file << ind << "/" << ind << "/" << ind;
			} else if(hasN) {
				file << ind << "//" << ind;
			} else if(hasU) {
				file << ind << "/" << ind;
			} else {
				file << ind;
			}
			file << (i < 2 ? " " : "\n");
		}
		
	}
	
	file.close();
	return true;
}

bool MeshOBJHandler::saveMtlOBJ(const MaterialMesh& mesh, const std::string& filePath) {
	std::ofstream file(filePath);
	if (!file.is_open()) {
		SIBR_ERR << "Unable to save mesh to " << filePath << std::endl;
		return false;
	}

	const auto& verts = mesh.vertices();
	const size_t vCount = verts.size();

	const auto& norms = mesh.normals();
	const size_t nCount = norms.size();

	const auto& uvs = mesh.texCoords();
	const size_t uCount = uvs.size();

	const auto& tris = mesh.triangles();
	const size_t tCount = tris.size();

	const bool hasN = mesh.hasNormals();
	const bool hasU = mesh.hasTexCoords();

	file << "# OBJ exported using SIBR custom handler." << "\n";
	file << "# " << vCount << " vertices, " << nCount << " normals, " << uCount << " texcoords, " << tCount << " faces." << "\n";
	

	for (const sibr::Vector3f& v : verts) {
		file << "v " << v[0] << " " << v[1] << " " << v[2] << "\n";
	}
	for (const sibr::Vector3f& n : norms) {
		file << "vn " << n[0] << " " << n[1] << " " << n[2] << "\n";
	}
	for (const sibr::Vector2f& uv : uvs) {
		file << "vt " << uv[0] << " " << uv[1] << "\n";
	}
	// Regroup faces per material IDs.
	size_t maxId = 0;
	for(size_t id = 0; id < verts.size(); ++id) {
		maxId = std::max(maxId, size_t(mesh.meshIds()[id]));
	}
	SIBR_LOG << "Max ID found: " << maxId << std::endl;
	std::vector<std::vector<size_t>> trids(maxId+1);
	// Dispatch vertices.
	for (size_t id = 0; id < tris.size(); ++id) {
		const auto refId = mesh.meshIds()[tris[id][0]];
		trids[refId].emplace_back(id);
	}

	int oid = 0;
	for (const std::vector<size_t>& obj : trids) {
		file << "o Object" << oid << "\n" << "usemtl Material" << oid << std::endl;
		for (const size_t& tid : obj) {
			const sibr::Vector3u& t = tris[tid];
			file << "f ";
			for (int i = 0; i < 3; ++i) {
				const std::string ind = std::to_string(t[i] + 1);
				if (hasN && hasU) {
					file << ind << "/" << ind << "/" << ind;
				} else if (hasN) {
					file << ind << "//" << ind;
				} else if (hasU) {
					file << ind << "/" << ind;
				} else {
					file << ind;
				}
				file << (i < 2 ? " " : "\n");
			}
		}
		++oid;

	}

	file.close();
	return true;
}


bool MeshOBJHandler::loadOBJ(const std::string & filePath, Mesh & mesh) {
	std::ifstream file(filePath);
	if(!file.is_open()) {
		SIBR_ERR << "Unable to load mesh at path " << filePath << "." << std::endl;
		return false;
	}

	std::vector<sibr::Vector3f> verts;
	std::vector<sibr::Vector3f> norms;
	std::vector<sibr::Vector2f> uvs;
	std::vector<sibr::Vector3u> tris;
	
	std::string line;
	while(std::getline(file, line)) {
		if(line.empty() || line[0] == '#' || line.size() < 3) {
			continue;
		}
		const std::string::size_type pos = line.find(' ');
		if(pos == std::string::npos) {
			continue;
		}
		const std::string key = line.substr(0, pos);
		const std::string token = line.substr(pos+1);
		std::stringstream sst(token);
		if(key == "v") {
			sibr::Vector3f v;
			sst >> v[0] >> v[1] >> v[2];
			verts.emplace_back(v);
		} else if (key == "vt") {
			sibr::Vector2f v;
			sst >> v[0] >> v[1];
			uvs.emplace_back(v);
		}
		else if (key == "vn") {
			sibr::Vector3f v;
			sst >> v[0] >> v[1] >> v[2];
			norms.emplace_back(v);
		} else if(key == "f") {
			std::string t0, t1, t2;
			sst >> t0 >> t1 >> t2;
			const unsigned long i0 = std::stoul(t0.substr(0, t0.find('/')));
			const unsigned long i1 = std::stoul(t1.substr(0, t1.find('/')));
			const unsigned long i2 = std::stoul(t2.substr(0, t2.find('/')));
			tris.emplace_back(uint(i0-1), uint(i1-1), uint(i2-1));
		}
	}
	file.close();

	mesh.vertices(verts);
	mesh.normals(norms);
	mesh.texCoords(uvs);
	mesh.triangles(tris);
	return true;
}
