/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once
#include "Config.hpp"

#include <core/system/Config.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
# include <core/graphics/GPUQuery.hpp>
#include <core/view/SceneDebugView.hpp>

#include <projects/optix/renderer/OptixRaycaster.hpp>
#include "GatherProbesRenderer.hpp"
#include "ProbesGatherScene.hpp"

#include "BilateralFilterRenderer.hpp"


namespace sibr { 

	/**
	 * \class ProbesView
	 * \brief 
	 */
	class  SIBR_SYNTHETIC_IBR_EXPORT ProbesView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(ProbesView);


	public:

		/**
		 * Constructor
		 */
		ProbesView(const ProbesGatherScene::Ptr& scene, uint w, uint h, const std::string& lutPath, bool supersampleDiffuse);

		
		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		void onUpdate(Input& input) override;

		void onGUI() override;

		void registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler);

		void snapTo(int i);

	protected:

		void resize(uint w, uint h);
		
		void selectProbes(const Camera& eye);

		void renderVisualisation();

		void resizeForNeighbors(size_t value);

		float aaVolume(const sibr::Vector3f& p0, const sibr::Vector3f& p1);

		enum SyntheticLayer : int {
			DIFFUSE = 1, 
			SPECULAR = 2,
			BOTH = DIFFUSE | SPECULAR
		};

		enum ProbesSelection : int {
			TRILINEAR = 0,
			FORCE_ONE = 1,
			ANGULAR = 2,
			DISTANCE = 3
		};

		RenderTargetRGBA32F::Ptr _specularRT;
		RenderTargetRGBA32F::Ptr _fxaaRT;
		RenderTargetRGBA32F::Ptr _reflectedPos;
		
		GatherProbesRenderer::Ptr _specRenderer;
		BilateralFilterRenderer::Ptr _bilateralFilter;
		
		GLShader _diffuseMesh;
		GLuniform<sibr::Matrix4f> _diffMVP;
		RenderTargetRGB::Ptr _diffuseRT;

		ProbesGatherScene::Ptr _scene;
		OptixRaycaster::Ptr _raycaster;
		
		MultiMeshManager::Ptr _debugView;
		InteractiveCameraHandler::Ptr _handler;

		GLShader _compositingShader;
		GLuniform<int> _compositingMode;
		GLuniform<float> _exposure;
		GLuniform<bool> _clampToLDR;

		GLShader _fxaaShader;
		GLuniform<sibr::Vector2f> _invSize;


		std::vector<int> _indices;
		std::vector<float> _coords;
		int _numNeighs = 8;

		SyntheticLayer _layerMode = BOTH;
		ProbesSelection _selectionMode = TRILINEAR;
		bool _blurMode = true;

		int _snapId = 0;
		float _cameraScale = 10.0f;
		bool _inRegion = true;
		int _forcedId = 0;
		bool _fxaa = true;

		GPUQuery _gbufferTime = GPUQuery(GL_TIME_ELAPSED);
		GPUQuery _selecTime = GPUQuery(GL_TIME_ELAPSED);
		GPUQuery _optixTime = GPUQuery(GL_TIME_ELAPSED);
		GPUQuery _gatherTime = GPUQuery(GL_TIME_ELAPSED);
		GPUQuery _filterTime = GPUQuery(GL_TIME_ELAPSED);
		GPUQuery _compositeTime = GPUQuery(GL_TIME_ELAPSED);
		bool _showDebugRTs = false;

		std::vector<float> _frameTimes;
		std::vector<float> _gbufferTimes;
		std::vector<float> _optixTimes;
		std::vector<float> _gatherTimes;
		std::vector<float> _filterTimes;
		std::vector<float> _compositeTimes;
		bool _monitoring = false;

		int _probeStep = 1;
		bool _ssdiff = true;
		bool _showParams = false;
		std::vector<InputCamera::Ptr> _pathCams;

		Eigen::AlignedBox3f _probesBBox;
	};

} /*namespace sibr*/ 
