/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once
#include "Config.hpp"
#include <core/system/Config.hpp>
#include <core/graphics/Mesh.hpp>
#include <core/graphics/MaterialMesh.hpp>

namespace sibr {
	class MaterialMesh;

	class  SIBR_SYNTHETIC_IBR_EXPORT MeshOBJHandler {
	public:

		/** Export an OBJ with a custom header and ensuring that no geometry is modified/reordered (thus no ASSIMP).
		 * \param mesh the mesh to save
		 * \param filePath destination path
		 * \param header optional header (to reference a .mtl)
		 */
		static bool saveOBJ(const Mesh & mesh, const std::string & filePath, const std::string & header = "");

		/** Export an OBJ with additional material info and ensuring that no geometry is modified/reordered (thus no ASSIMP).
		 * \param mesh the mesh to save
		 * \param filePath destination path
		 */
		static bool saveMtlOBJ(const MaterialMesh& mesh, const std::string& filePath);

		/** Load an OBJ making sure that no alteration is done to the vertex or primitive ordering.
		 *\param filePath source path
		 *\param mesh will be populated with the data read from disk
		 *\warning This loader assumes that the same indices are used for positions/normals/uvs.
		*/
		static bool loadOBJ(const std::string & filePath, Mesh & mesh);
	};

} 
