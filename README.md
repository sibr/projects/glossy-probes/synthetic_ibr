# Glossy Probe Reprojection for Interactive Global Illumination 					{#synthetic_ibrPage}
 
This *Project* is the reference implementation of [Rodriguez et al. 20] *Glossy Probe Reprojection for Interactive Global Illumination*, (http://www-sop.inria.fr/reves/Basilic/2020/RLPWSD20/).

If you use the code, we would greatly appreciate it if you could cite the corresponding paper:

```
@Article{RLPWSD20,
  author       = "Rodriguez, Simon and Leimk{\"u}hler, Thomas and Prakash, Siddhant and Wyman, Chris and Shirley, Peter and Drettakis, George",
  title        = "Glossy Probe Reprojection for Interactive Global Illumination",
  journal      = "ACM Transactions on Graphics (SIGGRAPH Asia Conference Proceedings)",
  number       = "6",
  volume       = "39",
  month        = "December",
  year         = "2020",
  keywords     = "light probes, parameterization, filtering, interactive global illumination",
  url          = "http://www-sop.inria.fr/reves/Basilic/2020/RLPWSD20"
}
```

and of course the actual *sibr* system:

```
@misc{sibr2020,
   author       = "Bonopera, Sebastien and Hedman, Peter and Esnault, Jerome and Prakash, Siddhant and Rodriguez, Simon and Thonat, Theo and Benadel, Mehdi and Chaurasia, Gaurav and Philip, Julien and Drettakis, George",
   title        = "sibr: A System for Image Based Rendering",
   year         = "2020",
   url          = "https://gitlab.inria.fr/sibr/sibr_core"
}
```

---

## Authors

Simon Rodriguez, Thomas Leimkühler, Siddhant Prakash, Chris Wyman, Peter Shirley, and George Drettakis.

---

## How to use

### Binary distribution

The easiest way to use *SIBR* is to download the binary distribution. All steps described below, including all preprocessing for your datasets will work using this code.

Download the precompiled distribution from the page: https://sibr.gitlabpages.inria.fr/download.html (Synthetic Ibr 159Mb); unzip the file and rename the directory "install". 

You will need Optix 7, CUDA 10.1 and the latest NVIDIA driver.

### Checkout and SIBR build
 
You will have to clone `sibr_core` (https://gitlab.inria.fr/sibr/sibr_core), `synthetic_ibr` (https://gitlab.inria.fr/sibr/projects/glossy-probes/synthetic_ibr) and `sibr_optix` (https://gitlab.inria.fr/sibr/projects/optix) for rendering, requiring CUDA 10.1.

You can do this using the following commands:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/sibr_core.git
## through SSH
git clone git@gitlab.inria.fr:sibr/sibr_core.git
```

Then go to *src/projects* and clone the other projects:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/projects/glossy-probes/synthetic_ibr.git
git clone https://gitlab.inria.fr/sibr/projects/optix.git
## through SSH
git clone git@gitlab.inria.fr:sibr/projects/glossy-probes/synthetic_ibr.git
git clone git@gitlab.inria.fr:sibr/projects/optix.git
```

Enable projects in Cmake (`BUILD_IBR_SYNTHETIC_IBR`, `BUILD_IBR_OPTIX`) before running the configuration (see [Configuring](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_configure_cmake)), then INSTALL everything (see [Compiling](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_compile)). You will also need our version of Mitsuba (see subsection below).
 
### Installing Mitsuba
 
There are two subrepos to clone (they should be in the same parent directory, called `mitsubaroot` hereafter): https://gitlab.inria.fr/syntheticgt/syntheticgt-mitsuba/-/tree/synthetic-modifs and https://gitlab.inria.fr/syntheticgt/syntheticgt-utilities
 
For the mitsuba subrepo, you will have to check the `synthetic-modifs` branch. You have to download the dependencies (from https://repo-sam.inria.fr/fungraph/synthetic-probes/datasets/windows-mitsuba-deps-1.0.zip) and put them in a `dependencies/` directory, so that the final hierarchy is:
 
    - mitsubaroot/
        - utilities/
        - mitsuba/
            - dependencies/
 
You will also have to install Python 2.7 and Scons v2.5.1. The path to Scons should be similar to `C:/Python27/scons-2.5.1/Scons/`. Make sure Python and Scons are in the system PATH or that you are using the proper environment.  
Open the VS2017 solution in the `mitsubaroot/mitsuba/build/` directory. Build in Release mode. The executables will be put in `mitsubaroot/mitsuba/dist/`.  
The build scripts have an issue with the Python bindings, go to the `mitsubaroot/mitsuba/dist/` directory and:
 
- if you want to use mitsuba with Python 2: make a copy of `boost_python27-vc141-mt-1_64.dll`, rename it `boost_python-vc141-mt-1_64.dll`
- if you want to use mitsuba with Python 3.5: make a copy of `boost_python35-vc141-mt-1_64.dll`, rename it `boost_python3-vc141-mt-1_64.dll`
- if you want to use mitsuba with Python 3.6: make a copy of `boost_python36-vc141-mt-1_64.dll`, rename it `boost_python3-vc141-mt-1_64.dll`
 
To run the python scripts, you will also have to set the path to the proper bindings python version and the binary directory in both `magnificationMapRender.py`, `warpFieldRender.py` and `lightmapRender.py`.
 
## How to preprocess the dataset
 
### Scene input layout
 
The input layout is as follows. We assume as input the Mitsuba `scene/` directory (all objects should be OBJ, no Mitsuba primitives), and the params.txt file that should contain a float value, the scale of the scene: how many units make 1 meter.
            
    - root/
    - scene/
        - scene.xml
        - XXX.obj,...
        - ZZZ.mtl,...
    - params.txt
 
  
### Scene data generation
 
The first time you process a new scene, you'll need to generate a parameterized mesh of the scene using an out-of-the-box atlas unwrapper, along with a diffuse lightmap and curvature information. This is automated using the `preprocess_synthetic_ibr.sh` script available in `synthetic_ibr/preprocess/`. Make sure to udpate the path to `mitsubaroot/mitsuba/` and `sibr/install/bin` in the script. 

> The script is split in three steps so that the Mitsuba lightmap rendering can be done on a cluster separately. Please refer to the script content or the [Preprocessing details](@ref glossyProbesPreprocDetails) page.

Run the following commands to perform unwrapping and compute curvatures, to render the lightmap (using Mitsuba, can take a few hours) and to merge the lightmap data:

    sh preprocess_synthetic_ibr.sh root/ uvmap 2048 8192
    sh preprocess_synthetic_ibr.sh root/ lightmap 2048 8192
    sh preprocess_synthetic_ibr.sh root/ merge 2048 8192
 
This will generate the `lightmap` and `curvatures` directories and their content.

### Probes placement
Probes have to be placed in the scene, using a simple interface.
Run the probes regular grid generator, from `sibr/install/bin` run:

    ./probesPlacement_rwdi.exe --mesh root/lightmap/scene_light_map.obj

Adjust the dimensions of the bounding box, the number of probes on each axis, and export the probes to `root/probes/NAME.positions`.
 
### Probe data generation
 
For each probe, specular and geometric data has to be precomputed. This is once again automated using the  `preprocess_synthetic_ibr.sh` script. 

> The script is split in three steps so that the Mitsuba renderings can be done on a cluster separately. Please refer to the script content or the [Preprocessing details](@subpage glossyProbesPreprocDetails) page.

Run the following steps in order:
 
    sh preprocess_synthetic_ibr.sh root/ preproc NAME WIDTH
    sh preprocess_synthetic_ibr.sh root/ render NAME WIDTH
    sh preprocess_synthetic_ibr.sh root/ pack NAME WIDTH
 
where `NAME` is the name of the probes file and `WIDTH` is the horizontal resolution to use (usually 1024, adjust depending on the number of probes).


### Final dataset layout

The layout obtained at the end of the preprocess is the following:

    - curvatures/   contains the precomputed surface curvatures
    - flows/        the precomputed geometric probe data for each set of probes
    - images/       the adaptive glossy renderings for each set of probes
    - lightmap/     the lightmap mesh and textures
    - probes/       the named lists of probes
    - scene/        the Mitsuba scene data
    - warps/        the guide and adaptive parameterization info for each set of probes
    - params.txt    the scene scale

Optionally for comparisons, you will also find the following directories:

    - falcor/           scene for the Falcor real-time path tracer comparison
    - g3d/              scene for the G3D light field probes comparison
        - probes/       the latitude-longitude G3D precomputed probes
        - probescube/   the cubemap faces G3D precomputed probes
        - scene/        the scene for G3D
        - SCENE.LightFieldModelSpecification.Any    the specification file for G3D
    - paths/            SIBR and lookat camera paths for comparisons

 
 
## Running

You will need a BRDF variance lookup table for runtime. You can either find one in the master dataset directory (`lut32pow.bin`), or generate it using (from `sibr/install/bin`):

    ./roughnessTest.exe --output path/to/lut.bin

> This is dataset independent and can be done only once and reused.

To visualize a scene, after building and installing, from `sibr/install/bin`, run:
 
    ./syntheticViewer_rwdi.exe --path root/ --name NAME --lut path/to/lut.bin (--rendering-size 1920 1080) (--no-topview) (--vsync 0)

For comparisons, see the [Comparisons](@subpage glossyProbesComp) page.  
The final datasets can be found here: https://repo-sam.inria.fr/fungraph/synthetic-probes/datasets/


